package khabarhub.com;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import khabarhub.com.English.View.EnglishMainActivity;
import khabarhub.com.Nepali.Views.MainActivity;
import khabarhub.com.Nepali.Views.SplashActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.onesignal.OneSignal;

public class SettingActivity extends Fragment {

    CardView changelang_card;
    TextView textView_default_language;
    Switch switch_notification;
    Boolean notification_state;
    LanguageSharepreference sharepreference;
    Sharepref_sessions sharepreferences_notification;
    AlertBoxs alertBoxs;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_setting, container, false);
        changelang_card = view.findViewById(R.id.language_card);
        textView_default_language = view.findViewById(R.id.txt_language_default);
        switch_notification = view.findViewById(R.id.notification_toggle);
        // alertBoxs=AlertBoxs.Instance(this);


        changelang_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertbox();
            }
        });
        notification_state = switch_notification.isChecked();
        OneSignal.setSubscription(notification_state);
        sharepreferences_notification = Sharepref_sessions.Instance(getContext());
        sharepreferences_notification.setnotificationstate(notification_state);
        sharepreference = LanguageSharepreference.Instance(getContext());
        String lang = sharepreference.getlanguage();
        if (lang != null && lang.equalsIgnoreCase("English")) {
            textView_default_language.setText("English");

        }
        return view;

    }

    public void alertbox() {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(getContext(), R.style.MyDialogTheme);
        } else {
            builder = new AlertDialog.Builder(getContext());
        }
        builder
                .setTitle(Html.fromHtml("<font color='#FFFFFF'>Set Default Language</font>"))
                .setPositiveButton(Html.fromHtml("<font color='#FFFFFF'>Nepali</font>"), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        sharepreference.clearprefe();
                        sharepreference.setlanguage("Nepali");
                        textView_default_language.setText("Nepali");
                        startActivity(new Intent(getActivity(), MainActivity.class));
                      getActivity().finish();

                    }
                })
                .setNegativeButton(Html.fromHtml("<font color='#FFFFFF'>English</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        sharepreference.clearprefe();
                        sharepreference.setlanguage("English");
                        textView_default_language.setText("English");
                        startActivity(new Intent(getActivity(), EnglishMainActivity.class));
                        getActivity().finish();


                    }
                }).show();

    }
}

