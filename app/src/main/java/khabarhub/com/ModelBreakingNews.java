package khabarhub.com;

public class ModelBreakingNews {
    String title,webcontent,description;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getWebcontent() {
        return webcontent;
    }

    public void setWebcontent(String webcontent) {
        this.webcontent = webcontent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
