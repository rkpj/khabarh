package khabarhub.com;

import android.content.Context;
import android.content.SharedPreferences;

public class LanguageSharepreference {


    public static LanguageSharepreference instance=null;
    Context context;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    public LanguageSharepreference(Context context) {
        this.context = context;
    }
    public static LanguageSharepreference Instance(Context context){
        if (instance==null){
            instance=new LanguageSharepreference(context);

        }
        return instance;
    }

    public void setlanguage(String language){
        sharedPreferences=context.getSharedPreferences("Khabarhub",Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        editor.putString("language",language);
        editor.commit();

    }
    public String getlanguage(){
        sharedPreferences=context.getSharedPreferences("Khabarhub",Context.MODE_PRIVATE);
        String language=sharedPreferences.getString("language",null);
        return language;
    }
    public void clearprefe(){
        sharedPreferences=context.getSharedPreferences("Khabarhub",Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        editor.clear();
        editor.commit();
    }




}
