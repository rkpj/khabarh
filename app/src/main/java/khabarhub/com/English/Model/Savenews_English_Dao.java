package khabarhub.com.English.Model;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;


@Dao
public interface Savenews_English_Dao  {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertall(Savenews_English_Dto savenews_english_dto);




    @Query("SELECT * FROM englisgsavenews")
    LiveData<List<Savenews_English_Dto>> getallenglishsavenews();

    @Query("SELECT * FROM englisgsavenews where id IN(:ids)")
    List<Savenews_English_Dto> getbodyofnews(int ids);

    @Query("DELETE FROM englisgsavenews")
    public void deleteoldvalue();

    @Query("DELETE FROM englisgsavenews where news_id IN(:ids)")
    public void deletenews(int ids);
}
