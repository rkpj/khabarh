package khabarhub.com.English.Model;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface EnglishBreakingNewsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertall(Dto_EnglishBreakingNews dto_englishBreakingNews);




    @Query("SELECT * FROM englishbreakingnews")
    LiveData<List<Dto_EnglishBreakingNews>> getallenglishbreakingnews();

    @Query("SELECT * FROM englishbreakingnews where id IN(:ids)")
    List<Dto_EnglishBreakingNews> getbodyofenglishbreakingnews(int ids);

    @Query("DELETE FROM englishbreakingnews")
    public void deleteoldvalue();
}
