package khabarhub.com.English.Model;

import android.content.Context;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import khabarhub.com.Nepali.Model.NewsPojoClass;

public class EnglishNewsNetworkOperation {
    public static EnglishNewsNetworkOperation instance=null;
    Context context;
        List<NewsPojoClass> listofNews=new ArrayList<>();
     EnglishNewsNetworkOperation(Context context) {
        this.context = context;
    }
    public static EnglishNewsNetworkOperation Instance(Context context){
        if (instance==null){
            instance=new EnglishNewsNetworkOperation(context);
        }
        return instance;
    }

    public void getNewsList(CategoriesNewsList_Interface categoriesNewsList_interface,int catid,int pagenumber){
        Log.d("Khabarhub_log","url:"+"https://english.khabarhub.com/wp-json/appharurest/v2/posts/"+catid+"/"+pagenumber);

        AndroidNetworking.get("https://english.khabarhub.com/wp-json/appharurest/v2/posts/"+catid+"/"+pagenumber)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        if (listofNews.size()>0){
                            listofNews.clear();
                        }
                        for (int i = 0; i < response.length(); i++) {
                            NewsPojoClass newsPojoClass = new NewsPojoClass();

                            try {
                                JSONObject jsonObject = response.getJSONObject(i);
                                 newsPojoClass.setNews_id(jsonObject.getInt("id"));
                                newsPojoClass.setTitle(jsonObject.optString("title"));

                                newsPojoClass.setDate(jsonObject.optString("date"));
                                String news_img=jsonObject.optString("image_link_small");
                                newsPojoClass.setImage(news_img);

                                listofNews.add(newsPojoClass);

                                //   newslist.add(newsPojoClass);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.d("Khabarhub_log", "getcategorieslist catch: " + e.getMessage());
                            }

                        }
//                        Log.d("Khabarhub_log", "getcategorieslist : " + listofNews.get(0).getImg());

                        categoriesNewsList_interface.categoriesNews(listofNews);


                    }


                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
}
