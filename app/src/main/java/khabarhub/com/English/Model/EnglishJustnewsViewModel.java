package khabarhub.com.English.Model;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

public class EnglishJustnewsViewModel extends AndroidViewModel {
    EnglishRepo englishRepo;

    public EnglishJustnewsViewModel(@NonNull Application application) {
        super(application);
        englishRepo=EnglishRepo.instance(application);
    }


    public LiveData<List<JustInNews_english>> getenglishjustnews(){

        return englishRepo.getEnglishJustNews();
    }
    public LiveData<List<Savenews_English_Dto>> getenglishsavednews(){

        return englishRepo.getEnglishsavedNews();
    }

    public LiveData<List<Dto_EnglishBreakingNews>> getenglishbreakingnews(){

        return englishRepo.getEnglishbreakingNews();
    }

}
