package khabarhub.com.English.Model;

import java.util.List;

import khabarhub.com.Nepali.Model.NewsPojoClass;

public interface CategoriesNewsList_Interface {
    void categoriesNews(List<NewsPojoClass> listofNewsPojos);
}
