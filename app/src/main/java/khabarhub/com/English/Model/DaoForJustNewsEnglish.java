package khabarhub.com.English.Model;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.JustInNewsDto;
@Dao
public interface DaoForJustNewsEnglish {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertall(JustInNews_english justInNews_english);




    @Query("SELECT * FROM justnewsforenglish")
    LiveData<List<JustInNews_english>> getallenglishnewsjust();

    @Query("SELECT * FROM justnewsforenglish where id IN(:ids)")
    List<JustInNews_english> getbodyofnews(int ids);

    @Query("DELETE FROM justnewsforenglish")
    public void deleteoldvalue();

}
