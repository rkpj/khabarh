package khabarhub.com.English.Model;

import java.io.Serializable;

public class ListofNewsPojo implements Serializable {
    int id;
    String title,date,img,news_link,desc;

    public ListofNewsPojo() {
    }

    public ListofNewsPojo(int id, String title, String date, String img, String news_link, String desc) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.img = img;
        this.news_link = news_link;
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getNews_link() {
        return news_link;
    }

    public void setNews_link(String news_link) {
        this.news_link = news_link;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }


}
