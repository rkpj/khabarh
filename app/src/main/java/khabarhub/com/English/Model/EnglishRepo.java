package khabarhub.com.English.Model;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

import androidx.lifecycle.LiveData;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.JustInDb;

public class EnglishRepo {


    private static EnglishRepo singleton_class=null;
    Application application;
    JustInDb justInDb;
    DaoForJustNewsEnglish daoForJustNewsEnglish;
    LiveData<List<JustInNews_english>> englishnewslist;
    EnglishBreakingNewsDao englishBreakingNewsDao;
    LiveData<List<Dto_EnglishBreakingNews>> englishbreakingnewslist;

    Savenews_English_Dao savenews_english_dao;
    LiveData<List<Savenews_English_Dto>> englishsavenewslist;
    public static EnglishRepo instance(Application application){
        if (singleton_class==null){
            singleton_class=new EnglishRepo(application);
        }
        return singleton_class;
    }

    public EnglishRepo(Application application) {
        this.application = application;
        justInDb= JustInDb.Instance(application);
        daoForJustNewsEnglish=justInDb.daoForJustNewsEnglish();
        englishnewslist=daoForJustNewsEnglish.getallenglishnewsjust();
        englishBreakingNewsDao=justInDb.englishBreakingNewsDao();
        englishbreakingnewslist=englishBreakingNewsDao.getallenglishbreakingnews();
        savenews_english_dao=justInDb.savenews_english_dao();
        englishsavenewslist=savenews_english_dao.getallenglishsavenews();



    }

    public LiveData<List<JustInNews_english>> getEnglishJustNews(){
        return englishnewslist;
    }

    public LiveData<List<Savenews_English_Dto>> getEnglishsavedNews(){
        return englishsavenewslist;
    }

    public LiveData<List<Dto_EnglishBreakingNews>> getEnglishbreakingNews(){
        return englishbreakingnewslist;
    }

    public void insertEnglishjustnews(JustInNews_english justInNews_english){
        new InsertEngleishJustnews(daoForJustNewsEnglish).execute(justInNews_english);
    }

    public void insertEnglishbreakingnews(Dto_EnglishBreakingNews dto_englishBreakingNews){
        new InsertEngleishBreakingnews(englishBreakingNewsDao).execute(dto_englishBreakingNews);
    }


    private class InsertEngleishJustnews extends AsyncTask<JustInNews_english,Void,Void> {
        DaoForJustNewsEnglish daoForJustNewsEnglishs;

        public InsertEngleishJustnews(DaoForJustNewsEnglish daoForJustNewsEnglish) {
            daoForJustNewsEnglishs=daoForJustNewsEnglish;

        }

        @Override
        protected Void doInBackground(JustInNews_english... justInNews_englishes) {
            daoForJustNewsEnglishs.insertall(justInNews_englishes[0]);
            return null;
        }
    }


    private class InsertEngleishBreakingnews extends AsyncTask<Dto_EnglishBreakingNews,Void,Void> {
        EnglishBreakingNewsDao englishBreakingNewsDaos;

        public InsertEngleishBreakingnews(EnglishBreakingNewsDao englishBreakingNewsDao) {
            englishBreakingNewsDaos=englishBreakingNewsDao;

        }

        @Override
        protected Void doInBackground(Dto_EnglishBreakingNews... dto_englishBreakingNews) {
            englishBreakingNewsDaos.insertall(dto_englishBreakingNews[0]);
            return null;
        }
    }
}
