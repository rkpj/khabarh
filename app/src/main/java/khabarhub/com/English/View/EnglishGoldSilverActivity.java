package khabarhub.com.English.View;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Map;

import khabarhub.com.Nepali.RepositoryPackage.GetGoldSilverPrice;
import khabarhub.com.Nepali.Views.DailyutilPage.Gold_Silver_Interface;
import khabarhub.com.R;

public class EnglishGoldSilverActivity extends AppCompatActivity implements Gold_Silver_Interface {

    private TextView mtitle,unit,finegoldname,tejabigoldname,silvername,finegoldprice,tejabigoldprice,silverprice,publishdate;
    private LinearLayout linearLayout;
    ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gold__silver);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        progressBar = findViewById(R.id.progressbar_gold_silver);
        linearLayout = findViewById(R.id.linear_gold);
        mtitle = findViewById(R.id.txt_gold_silvet_title);
        unit = findViewById(R.id.txt_gold_silver_unit);
        finegoldname = findViewById(R.id.txt_finegold_name);
        finegoldprice = findViewById(R.id.txt_finegold_price);
        tejabigoldname = findViewById(R.id.txt_tejabigold_name);
        tejabigoldprice = findViewById(R.id.txt_tejabigold_price);
        silvername = findViewById(R.id.txt_silver_name);
        silverprice = findViewById(R.id.txt_silver_price);
        publishdate = findViewById(R.id.txt_gold_silver_publishdate);
        mtitle.setText("Gold & Silver Rate");
        mtitle.setAllCaps(true);
        unit.setText("PER TOLA");

        LayoutInflater inflator = (LayoutInflater) this .getSystemService(this.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.toolbar_custom, null);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setCustomView(v);
        new GoldAsyncTask(this,this).execute();
    }

    @Override
    public void goldSilverPrice(Map<String, String> mapdata) {
        if (mapdata!=null){
            progressBar.setVisibility(View.GONE);
            linearLayout.setVisibility(View.VISIBLE);
            silvername.setText("SILVER");
            publishdate.setText("published_date: "+mapdata.get("published_date"));
            finegoldname.setText("FINE GOLD (999999))");
            tejabigoldname.setText("TEJABI GOLD");
            finegoldprice.setText(""+mapdata.get("finegold"));
            tejabigoldprice.setText(""+mapdata.get("tejabigold"));
            silverprice.setText(""+mapdata.get("silver"));

        }



    }

    private class GoldAsyncTask extends AsyncTask<Void,Void,Void> {
        Gold_Silver_Interface gold_silver_interface;
        Context context;

        public GoldAsyncTask(Gold_Silver_Interface gold_silver_interface, Context context) {
            this.gold_silver_interface = gold_silver_interface;
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            new GetGoldSilverPrice(gold_silver_interface,context).getGoldSilverPrice();
            return null;
        }
    }
}
