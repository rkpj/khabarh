package khabarhub.com.English.View;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import android.util.Log;

import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import khabarhub.com.Aboutus;
import khabarhub.com.AlertBoxs;
import khabarhub.com.English.View.CategoriesNes.Fragment_of_english_categories;
import khabarhub.com.English.View.SaveNews.EnglishSaveNewsFragment;
import khabarhub.com.English.View.SaveNews.Savenews_English_Adapter;
import khabarhub.com.Nepali.Apis;
import khabarhub.com.Nepali.Views.DailyutilPage.ForexActivity;
import khabarhub.com.Nepali.Views.DailyutilPage.ShareMarketActivity;
import khabarhub.com.Nepali.Views.MainActivity;
import khabarhub.com.R;
import khabarhub.com.SettingActivity;
import khabarhub.com.Sharepref_sessions;
import khabarhub.com.calendarnepali.CalendarActivity;
import khabarhub.com.radio.AudioManage;
import khabarhub.com.radio.MyService;
import khabarhub.com.radio.RadioActivity;

import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class EnglishMainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    DrawerLayout drawer;
AlertBoxs alertBoxs;
    TextView weather_temp;
    ImageView weather_icon;
    BottomNavigationView bottomNavigationView;

    String lat,longs;
    Sharepref_sessions sharepreference;
    CardView radio_linearlayout;
    FrameLayout container;
    ConstraintLayout.LayoutParams params;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_english_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


            alertBoxs=AlertBoxs.Instance(this);
         drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        weather_icon=findViewById(R.id.weather_icon);
        weather_temp=findViewById(R.id.weather_temp);
        sharepreference= Sharepref_sessions.Instance(this);
        lat=sharepreference.getlat();
        longs=sharepreference.getlong();
        getweather(lat,longs);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        replaceFragment(new EnglishTabFragment(),1);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
         bottomNavigationView=findViewById(R.id.bottom_navigation);
//        bottomNavigationView.getMenu().removeItem(R.id.navigation_rasifal);
        bottomNavigationView.getMenu().removeItem(R.id.navigation_new);
        bottomNavigationView.getMenu().removeItem(R.id.navigation_setting);
        bottomNavigationView.inflateMenu(R.menu.bottomnavmenu);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {

                    case R.id.navigation_engcalendar:
                        replaceFragment(new EnglishCalendar(),1);
                        break;
                    case R.id.navigation_settings:
                        replaceFragment(new SettingActivity(),1);
                        break;
                    case R.id.navigation_news:
                        replaceFragment(new EnglishTabFragment(),1);
                        break;

                    case R.id.navigation_radio:
                        startActivity(new Intent(EnglishMainActivity.this, RadioActivity.class));
                        break;

                }
                return true;

            }
        });

         radio_linearlayout = findViewById(R.id.layout_linear_buttomradio_mainpage);
        AppCompatImageView radiopausebtn = findViewById(R.id.imgbtn_play_bottomradio_mainpage);
        container = findViewById(R.id.contentview);
        params = (ConstraintLayout.LayoutParams) container.getLayoutParams();

        if (AudioManage.INSTANCE(this).getAudioFlag() == true){
            radio_linearlayout.setVisibility(View.VISIBLE);
            params.bottomMargin=165;
            container.setLayoutParams(params);
        }
        radiopausebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EnglishMainActivity.this, MyService.class);
                intent.setAction("com.android.music.playstatechanged");
                stopService(intent);
                radio_linearlayout.setVisibility(View.GONE);
                params.bottomMargin=0;
                container.setLayoutParams(params);

            }
        });

    }
    private void getweather(String lat, String longs) {
        AndroidNetworking.get(Apis.weather_base_url+"lat="+lat+"&lon="+longs+"&units=metric&appid="+Apis.apikey_weather)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String city=response.optString("name");
                        try {
                            JSONObject jsonObject=response.getJSONObject("main");
                            String temp=jsonObject.optString("temp");
                          //  weather_temp.setText(temp+"\u2103");
                            JSONArray jsonArray=response.getJSONArray("weather");
                            for (int i=0;i<jsonArray.length();i++) {
                                JSONObject objforicons = jsonArray.getJSONObject(i);
                                String icon=objforicons.optString("icon");
                                String discribtion=objforicons.optString("description");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                    }
                });
    }
    @Override
    protected void onStart() {
        super.onStart();
        lat=sharepreference.getlat();
        longs=sharepreference.getlong();
        getweather(lat,longs);
        String temp=sharepreference.getTemp();
        if (temp!=null){
            weather_temp.setText(sharepreference.getTemp()+"\u2103");
            Picasso.with(this).load("http://openweathermap.org/img/w/" + sharepreference.gettempIcon() + ".png").into(weather_icon);
        }
        replaceFragment(new EnglishTabFragment(),1);
        if (AudioManage.INSTANCE(this).getAudioFlag() == true){
            radio_linearlayout.setVisibility(View.VISIBLE);
            params.bottomMargin=165;
            container.setLayoutParams(params);
        }
        if (AudioManage.INSTANCE(this).getAudioFlag() == false){
            radio_linearlayout.setVisibility(View.GONE);
            params.bottomMargin=0;
            container.setLayoutParams(params);

        }

    }
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            int seletedItemId = bottomNavigationView.getSelectedItemId();
            if (R.id.navigation_news != seletedItemId) {
                bottomNavigationView.setSelectedItemId(R.id.navigation_news);
            } else {
              //  finish();
              finishAffinity();
            }
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            replaceFragment(new EnglishTabFragment(),1);
            bottomNavigationView.setSelectedItemId(R.id.navigation_news);
            // Handle the camera action
        } else if (id == R.id.nav_news) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_news);
            replaceFragment(new Fragment_of_english_categories(),13);

        }
        else if (id == R.id.nav_national) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_news);
            replaceFragment(new Fragment_of_english_categories(),2);

        }else if (id == R.id.nav_international) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_news);
            replaceFragment(new Fragment_of_english_categories(),2);

        } else if (id == R.id.nav_business) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_news);
            replaceFragment(new Fragment_of_english_categories(),4);

        }
        else if (id == R.id.nav_travel) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_news);
            replaceFragment(new Fragment_of_english_categories(),49);

        } else if (id == R.id.nav_lifestyle) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_news);
            replaceFragment(new Fragment_of_english_categories(),16);

        }
        else if (id == R.id.nav_health) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_news);
            replaceFragment(new Fragment_of_english_categories(),41);

        } else if (id == R.id.nav_entertainment) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_news);
            replaceFragment(new Fragment_of_english_categories(),10);

        }
        else if (id == R.id.nav_technology) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_news);
            replaceFragment(new Fragment_of_english_categories(),15);

        } else if (id == R.id.nav_sports) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_news);
            replaceFragment(new Fragment_of_english_categories(),5);


        }

        else if (id == R.id.saves) {
            startActivity(new Intent(this, EnglishSaveNewsFragment.class));
        }
        else if (id == R.id.navigation_engcalendar) {
            startActivity(new Intent(this, CalendarActivity.class));
        }
        else if (id == R.id.forex) {
            startActivity(new Intent(this, ForexActivity.class));
        }
        else if (id == R.id.gold_silver) {
            startActivity(new Intent(this, EnglishGoldSilverActivity.class));
        }
        else if (id == R.id.sharemarket) {
            startActivity(new Intent(this, ShareMarketActivity.class));
        }
        else if (id == R.id.dateConvert) {
            startActivity(new Intent(this, EnglishDateConverterActivity.class));
        }
        else if (id == R.id.settings) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_settings);
            replaceFragment(new SettingActivity(),1);
        }

        else if (id == R.id.nav_aboutuss) {
            startActivity(new Intent(this, Aboutus.class));
        }
        else if (id == R.id.nav_facebooklike_eng) {
            String facebookId = "fb://page/389410281854039";
            String urlPage = "https://www.facebook.com/KhabarhubEnglish";

            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookId )));
            } catch (Exception e) {
                //Open url web page.
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlPage)));
            }
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    void replaceFragment(Fragment fragment, int cat_id){
        if (fragment != null) {
            Bundle bundle=new Bundle();
            bundle.putInt("cat_id",cat_id);
            fragment.setArguments(bundle);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.contentview, fragment);
            ft.commit();
        }
        drawer.closeDrawer(GravityCompat.START);
    }

}

