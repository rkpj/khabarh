package khabarhub.com.English.View.BreakingNews;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import khabarhub.com.English.Model.Savenews_English_Dao;
import khabarhub.com.English.Model.Savenews_English_Dto;
import khabarhub.com.English.View.EnglishMainActivity;
import khabarhub.com.English.View.HotNews.RelativeNews_JustIn_english;
import khabarhub.com.LanguageSharepreference;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.JustInDb;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.NepaliNewsSaveDao;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.NepaliNewsSaveDto;
import khabarhub.com.Nepali.Views.MainActivity;
import khabarhub.com.R;
import khabarhub.com.radio.AudioManage;

public class EngNewsdetailspageFlipView extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";
    private static final String ARG_PARAM ="param";
    private static final String ARG_Author ="author";

    // TODO: Rename and change types of parameters
    private String mtitle;
    private String mdate;
    private String mAuthor;
    private String news,news_url,imgs;
    private WebSettings settings;
    RelativeNews_Breaking relativeNews_breaking;
    LanguageSharepreference languageSharepreference;
    String language;
    int news_id;
    MenuItem voice_menu,pause_menu;

    private OnFragmentInteractionListener mListener;

    public EngNewsdetailspageFlipView() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static EngNewsdetailspageFlipView newInstance(int news_id, String title, String date, String body, String news_link, String img,String author_name) {
        EngNewsdetailspageFlipView fragment = new EngNewsdetailspageFlipView();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, title);
        args.putString(ARG_PARAM2, date);
        args.putString(ARG_PARAM3,body);
        args.putString(ARG_PARAM4,news_link);
        args.putString(ARG_PARAM5,img);
        args.putInt(ARG_PARAM,news_id);
        args.putString(ARG_Author,author_name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        relativeNews_breaking= (RelativeNews_Breaking) getActivity();

        if (getArguments() != null) {

            mtitle = getArguments().getString(ARG_PARAM1);
            mdate = getArguments().getString(ARG_PARAM2);
            news=getArguments().getString(ARG_PARAM3);
            news_url=getArguments().getString(ARG_PARAM4);
            imgs=getArguments().getString(ARG_PARAM5);
            news_id=getArguments().getInt(ARG_PARAM);
            mAuthor = getArguments().getString(ARG_Author);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_newsdetailspage_with_related_news, container, false);
        FrameLayout frameLayout = v.findViewById(R.id.framelayout_newsdetails_page);
        AudioManage audioManage = AudioManage.INSTANCE(getContext());
        Log.d("mediaplayer","radioflag:"+audioManage.getAudioFlag());
        if (audioManage.getAudioFlag()==true){
            v.setPaddingRelative(0,0,0,45);
        }
        TextView title=v.findViewById(R.id.txt_news_detail_title);
        TextView date=v.findViewById(R.id.txt_news_detail_date);
        TextView authorname=v.findViewById(R.id.txt_news_detail_author_name);

        ImageView img=v.findViewById(R.id.img_relatednews);
        WebView webView=v.findViewById(R.id.related_news_detail_body);

        setHasOptionsMenu(true);
        languageSharepreference=LanguageSharepreference.Instance(getContext());
         language=languageSharepreference.getlanguage();

        settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setLoadsImagesAutomatically(true);

        webView.setWebViewClient(new WebViewClient());
        title.setText(mtitle);
        title.setTextSize(24);
        date.setText(mdate);
        authorname.setText(mAuthor+" | Kathmandu");

        webView.loadDataWithBaseURL(null, "<style>img{display: inline;height: auto;max-width: 100%;}p{text-align: justify;}</style>" +news,"text/html","utf-8",null);
        Picasso.with(getContext()).load(imgs).placeholder(R.drawable.opcity_logo).into(img);
        settings.setTextZoom(settings.getTextZoom()+20);

        return v;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main,menu);
        voice_menu = menu.findItem(R.id.menu_voice);
        pause_menu = menu.findItem(R.id.menu_pause);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_sharebutton) {
            Intent intent=new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            intent.putExtra(Intent.EXTRA_TITLE,"Khabarhub");
            intent.putExtra(Intent.EXTRA_TEXT,news_url);
            startActivity(Intent.createChooser(intent,"Share Using"));

            return true;
        }
        if (id==R.id.menu_fontdecreament){
            settings.setTextZoom(settings.getTextZoom() - 10);
        }
        if (id==R.id.menu_fontincreament){
            settings.setTextZoom(settings.getTextZoom() + 10);
        }
        if (id==R.id.menu_savebutton){
           if (language!=null && language.equalsIgnoreCase("English")){
                JustInDb justInDb = JustInDb.Instance(getContext());
                new SaveEnglishNewsAsyncTask(justInDb).execute();
            }
            else {
                JustInDb justInDb = JustInDb.Instance(getContext());
                new SaveNewsAsyncTask(justInDb).execute();
            }
        }
        if (id == R.id.menu_voice){
         //   item.setVisible(false);

            relativeNews_breaking.speakWords(mtitle+"."+Html.fromHtml(news));
            item.setVisible(false);
            pause_menu.setVisible(true);
        }
        if (id == R.id.menu_pause){
            pause_menu.setVisible(false);
            relativeNews_breaking.stopAudio();
            voice_menu.setVisible(true);
        }
        if (id==R.id.menu_home){

            if (language!=null && language.equalsIgnoreCase("English")){
                startActivity(new Intent(getActivity(), EnglishMainActivity.class));
            }
            else {
                startActivity(new Intent(getActivity(), MainActivity.class));
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class SaveNewsAsyncTask extends AsyncTask<Void,Void,Void> {
        JustInDb justInDbs;
        NepaliNewsSaveDao nepaliNewsSaveDao;
        public SaveNewsAsyncTask(JustInDb justInDb) {
            justInDbs=justInDb;
            nepaliNewsSaveDao=justInDb.nepaliNewsSaveDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Log.d("Khabarhub_log","save image doinback: "+imgs);
            justInDbs.nepaliNewsSaveDao().insertall(new NepaliNewsSaveDto(news_id,mtitle,mdate,imgs,news_url,news,mAuthor,""));
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(getActivity(),"Saved",Toast.LENGTH_SHORT).show();
            super.onPostExecute(aVoid);
        }
    }

    private class SaveEnglishNewsAsyncTask extends AsyncTask<Void,Void,Void>{
        JustInDb justInDbs;
        Savenews_English_Dao savenews_english_dao;
        public SaveEnglishNewsAsyncTask(JustInDb justInDb) {
            justInDbs=justInDb;
            savenews_english_dao=justInDb.savenews_english_dao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            justInDbs.savenews_english_dao().insertall(new Savenews_English_Dto(news_id,mtitle,mdate,imgs,news_url,news,mAuthor,""));

            return null;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("Khabarhub_log","save image: "+imgs);
            Toast.makeText(getActivity(),"Saved",Toast.LENGTH_SHORT).show();

            super.onPostExecute(aVoid);
        }
    }
}
