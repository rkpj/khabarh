package khabarhub.com.English.View.CategoriesNes;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import khabarhub.com.English.Model.CategoriesNewsList_Interface;
import khabarhub.com.English.Model.EnglishNewsNetworkOperation;
import khabarhub.com.Nepali.Model.NewsPojoClass;
import khabarhub.com.Nepali.Views.NpCategoriesNews.RelatednewsActivity_with_news_details_page;
import khabarhub.com.NetworkConnection;
import khabarhub.com.R;

public class Fragment_of_english_categories extends Fragment implements List_of_english_Categories_news.OnItemClickListner, CategoriesNewsList_Interface, SwipeRefreshLayout.OnRefreshListener {

    List<NewsPojoClass> newslist=new ArrayList<>();
    RecyclerView recyclerView_news;
    SwipeRefreshLayout mswiperefresh;
    List_of_english_Categories_news adapter;
    int categ_id;
    NetworkConnection networkConnection;
    boolean ntstatus;
    Dialog dialog;
    EnglishNewsNetworkOperation newsNetworkOperation;


    int total=0;
    private int mPreviousTotal = 0;
    static int current_page;
    static int last_page;
    private boolean mLoading = true;


    public Fragment_of_english_categories() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_recycle_view, container, false);
        mswiperefresh=view.findViewById(R.id.swiperefresh_fragmentrecy);
        Bundle b=getArguments();
        networkConnection=new NetworkConnection(getContext());
        prodressdialog();
        //dialog.show();

        newsNetworkOperation=EnglishNewsNetworkOperation.Instance(getContext());
        categ_id=b.getInt("cat_id");
        recyclerView_news=view.findViewById(R.id.frag_recylce_view_recycleview);
        AndroidNetworking.initialize(getContext());
      //  newsNetworkOperation.getNewsList(this,categ_id,1);
        newslist.clear();
        dialog.show();
        current_page=1;

        getNewsList(categ_id,current_page);
      //  getNewsList(categ_id);

        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getContext());
        recyclerView_news.setLayoutManager(layoutManager);
        recyclerView_news.setHasFixedSize(true);

        recyclerView_news.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState==0){

                    onScrollrecycleview();
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });
        adapter = new List_of_english_Categories_news(newslist, getContext(), Fragment_of_english_categories.this);

        recyclerView_news.setAdapter(adapter);


        mswiperefresh.setOnRefreshListener(this);
        return  view;
    }


    void prodressdialog()
    {
        ProgressBar progressBar=new ProgressBar(getContext());

        progressBar.setIndeterminate(true);
        progressBar.setScrollBarSize(2);
        dialog = new Dialog(getActivity());
        progressBar.setMinimumWidth(10);
        progressBar.setMinimumHeight(10);

        //  dialog.getWindow().setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL);
        dialog.getWindow().setBackgroundDrawable(new
                ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(progressBar);

        dialog.setCancelable(true);
        //  dialog.setContentView(R.layout.progressdialoglayout);


    }

    @Override
    public void onclick(String title, String date, String news,List<NewsPojoClass> newslists,int clickposition,String url_link) {
        ntstatus=networkConnection.isOnline();
        if (ntstatus==true) {
            Intent intent = new Intent(getContext(), RelatednewsActivity_with_news_details_page.class);
            intent.putExtra("news_title", title);
            //  intent.putExtra("news_title",newslist.get(getAdapterPosition()).getNews_title());
            intent.putExtra("news_date", date);
            intent.putExtra("news_details",news);
            intent.putExtra("newslist", (Serializable) newslists);
            intent.putExtra("clickposition",clickposition);
            intent.putExtra("link",url_link);

            startActivity(intent);

        }
        else {
            Snackbar.make(getView(),"Something is Wrong",Snackbar.LENGTH_LONG).show();
        }

    }

    @Override
    public void categoriesNews(List<NewsPojoClass> listofNewsPojos) {
//        Log.d("Khabarhub_log","loadinrecycle:"+listofNewsPojos.get(0).getTitle());
        if (listofNewsPojos.size()>0) {
            adapter = new List_of_english_Categories_news(listofNewsPojos, getContext(), Fragment_of_english_categories.this);
            dialog.dismiss();
            recyclerView_news.setAdapter(adapter);

            mswiperefresh.setRefreshing(false);

        }
        else {
            Snackbar.make(getView(),"Something is Wrong",Snackbar.LENGTH_LONG).show();

        }
    }



    @Override
    public void onRefresh() {
        newslist.clear();

        getNewsList(categ_id,1);

    }


    public void getNewsList(int catid,int pagenumber){
      //  dialog.show();

        AndroidNetworking.get("https://english.khabarhub.com/wp-json/appharurest/v2/posts/"+catid+"/"+pagenumber)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        mswiperefresh.setRefreshing(false);
                        for (int i = 0; i < response.length(); i++) {
                            NewsPojoClass newsPojoClass = new NewsPojoClass();

                            try {
                                JSONObject jsonObject = response.getJSONObject(i);
                                newsPojoClass.setNews_id(jsonObject.getInt("id"));
                                newsPojoClass.setTitle(jsonObject.optString("title"));
                                newsPojoClass.setImage(jsonObject.optString("image_link_medium"));
                                newsPojoClass.setDate(jsonObject.optString("date"));
                                newsPojoClass.setDesc(jsonObject.optString("body"));
                                newsPojoClass.setNews_link(jsonObject.optString("permalink"));
                                newsPojoClass.setAuthor_name(jsonObject.optString("author_name"));
                              newslist.add(newsPojoClass);
                                //   newslist.add(newsPojoClass);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        dialog.dismiss();

                        adapter.notifyDataSetChanged();
//                        Log.d("Khabarhub_log", "getcategorieslist : " + listofNews.get(0).getImg());

                    //    categoriesNewsList_interface.categoriesNews(listofNews);


                    }


                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();


                    }
                });
    }


    private void onScrollrecycleview() {

        int visibleItemCount = recyclerView_news.getChildCount();
        int totalItemCount = recyclerView_news.getLayoutManager().getItemCount();

        int firstVisibleItem = ((LinearLayoutManager) recyclerView_news.getLayoutManager()).findFirstVisibleItemPosition();

        if (mLoading) {

            if (total > totalItemCount) {
                mLoading = false;
                mPreviousTotal = totalItemCount;
            }

        }

        int visibleThreshold = 5;
        if ( (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + visibleThreshold)) {




            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    current_page += 1;
                 //   dialog.show();
                    getNewsList(categ_id,current_page);
                    mLoading = true;
                }


            }, 3000);


        }
        else {

        }
    }

}
