package khabarhub.com.English.View;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.applandeo.materialcalendarview.CalendarView;

import khabarhub.com.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link EnglishCalendar#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EnglishCalendar extends Fragment {
    public EnglishCalendar() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_english_calendar, container, false);
        CalendarView calendarView = v.findViewById(R.id.english_calendar);
        // Inflate the layout for this fragment
        return v;
    }
}
