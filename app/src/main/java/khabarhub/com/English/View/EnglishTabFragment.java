package khabarhub.com.English.View;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;

import java.util.List;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import khabarhub.com.Nepali.ApiOpseration;
import khabarhub.com.Nepali.Model.Categories;
import khabarhub.com.R;

public class EnglishTabFragment extends Fragment {
    private static final String TAG = EnglishTabFragment.class.getSimpleName();
    private TabLayout tabLayout;
    private ViewPager viewPager;
    List<Categories> categories;
    public EnglishTabFragment() {
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab, container, false);
        tabLayout = (TabLayout)view.findViewById(R.id.tabs);
        viewPager = (ViewPager)view.findViewById(R.id.view_pager);
        ApiOpseration apiOpseration=new ApiOpseration(getContext());
        viewPager.setAdapter(new CustomFragmentPageAdapterfor_english(getChildFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
        return view;
    }

}
