package khabarhub.com.English.View.BreakingNews;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidnetworking.AndroidNetworking;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import khabarhub.com.English.Model.Dto_EnglishBreakingNews;
import khabarhub.com.English.Model.EnglishJustnewsViewModel;
import khabarhub.com.English.Model.JustInNews_english;
import khabarhub.com.English.View.HotNews.RelativeNews_JustIn_english;
import khabarhub.com.NetworkConnection;
import khabarhub.com.R;
import se.emilsjolander.flipview.FlipView;

public class EnglishBreakingNewsFlipView extends Fragment implements EnglishBreakingNewsFlipView_adapber.OnItemClickListner, SwipeRefreshLayout.OnRefreshListener, FlipView.OnFlipListener {

    Dialog dialog;
    EnglishJustnewsViewModel englishJustnewsViewModel;
    NetworkConnection networkConnection;
    EnglishBreakingNewsFlipView_adapber adapter;
    SwipeRefreshLayout mswiperefresh;
    FlipView flipView;
    //List<EnglishBreakingNewsDao_Impl> breakingNewsDtoList;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.nepalibreakingnews_flipview, container, false);
        //  viewPager = view.findViewById(R.id.breakingnews_viewpager);
        flipView=view.findViewById(R.id.flip_view_nepali_breaking);
        mswiperefresh=view.findViewById(R.id.swiperefresh_nepali_breaking_fragmentrecy);
        mswiperefresh.setRefreshing(false);
        mswiperefresh.setEnabled(false);
        AndroidNetworking.initialize(getContext());
        networkConnection=new NetworkConnection(getContext());

        //  dialog.show();
        flipView.setOnFlipListener(this);
        flipView.peakNext(false);
        //    flipView.setOverFlipMode(OverFlipMode.RUBBER_BAND);

        flipView.setOnFlipListener(this);

        prodressdialog();
        englishJustnewsViewModel = ViewModelProviders.of(this).get(EnglishJustnewsViewModel.class);
        englishJustnewsViewModel.getenglishbreakingnews().observe(this, new Observer<List<Dto_EnglishBreakingNews>>() {
            @Override
            public void onChanged(List<Dto_EnglishBreakingNews> breakingNewsDtos) {
                dialog.show();
                englishJustnewsViewModel.getenglishjustnews().observe(EnglishBreakingNewsFlipView.this, new Observer<List<JustInNews_english>>() {
                    @Override
                    public void onChanged(List<JustInNews_english> justInNewsDtos) {
                   if (breakingNewsDtos.size()>0){
                       if (justInNewsDtos.size()>0){
                           adapter=new EnglishBreakingNewsFlipView_adapber(getContext(), breakingNewsDtos,justInNewsDtos,EnglishBreakingNewsFlipView.this );
                           flipView.setAdapter((ListAdapter) adapter);

                       }
                       else{
                           Log.e("Khabarhub_log","justnewsengsize"+justInNewsDtos.size());
                       }
                   }
                   else{
                       Log.e("Khabarhub_log","breaknewsengsize"+breakingNewsDtos.size());
                   }

                    }
                });

                dialog.dismiss();

            }
        });

        mswiperefresh.setOnRefreshListener(this);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    void prodressdialog()
    {
        ProgressBar progressBar=new ProgressBar(getContext());

        progressBar.setIndeterminate(true);
        progressBar.setScrollBarSize(2);
        dialog = new Dialog(getActivity());
        progressBar.setMinimumWidth(10);
        progressBar.setMinimumHeight(10);

        //  dialog.getWindow().setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL);
        dialog.getWindow().setBackgroundDrawable(new
                ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(progressBar);

        dialog.setCancelable(true);
        //  dialog.setContentView(R.layout.progressdialoglayout);


    }

    @Override
    public void onclickBreakMain(String title, String date, String news, List<Dto_EnglishBreakingNews> newslists, int position, String url_link) {
        Intent intent = new Intent(getContext(), RelativeNews_Breaking.class);
        intent.putExtra("news_title", newslists.get(position).getNews_title());
        //  intent.putExtra("news_title",newslist.get(getAdapterPosition()).getNews_title());
        intent.putExtra("news_date", newslists.get(position).getNews_date());
        intent.putExtra("news_details",newslists.get(position).getNews_body());
        intent.putExtra("newslist", (Serializable) newslists);
        intent.putExtra("clickposition",position);
        intent.putExtra("link",newslists.get(position).getNews_link());

        startActivity(intent);
    }

    @Override
    public void onclickBreakRelated(String title, String date, String news, List<JustInNews_english> newslists, int position, String url_link) {
        Intent intent = new Intent(getContext(), RelativeNews_JustIn_english.class);
        intent.putExtra("news_title", newslists.get(position).getNews_title());
        //  intent.putExtra("news_title",newslist.get(getAdapterPosition()).getNews_title());
        intent.putExtra("news_date", newslists.get(position).getNews_date());
        intent.putExtra("news_details",newslists.get(position).getNews_body());
        intent.putExtra("newslist", (Serializable) newslists);
        intent.putExtra("clickposition",position);
        intent.putExtra("link",newslists.get(position).getNews_link());

        startActivity(intent);

    }

    @Override
    public void onRefresh() {
//        npbreakingnews.getBreakingNepalinews().observe(this, new Observer<List<BreakingNewsDto>>() {
//            @Override
//            public void onChanged(List<BreakingNewsDto> breakingNewsDtos) {
//                dialog.show();
//                adapter=new NepaliBreakingNewsFlipView_adapber(getContext(), breakingNewsDtos,NepaliBreakingNewsFlipView.this );
//                flipView.setAdapter((ListAdapter) adapter);
//                mswiperefresh.setRefreshing(false);
//                dialog.dismiss();
//            }
//        });
    }

    @Override
    public void onFlippedToPage(FlipView v, int position, long id) {

        if (position==0){

            mswiperefresh.setEnabled(true);
            mswiperefresh.setRefreshing(true);
        }
        else {
            mswiperefresh.setEnabled(false);
        }

    }
}

