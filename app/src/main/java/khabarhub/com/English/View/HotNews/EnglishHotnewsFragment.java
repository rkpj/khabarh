package khabarhub.com.English.View.HotNews;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.google.android.material.snackbar.Snackbar;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import khabarhub.com.English.Model.EnglishJustnewsViewModel;
import khabarhub.com.English.Model.JustInNews_english;
import khabarhub.com.Nepali.Model.Categories;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.JustInNewsDto;
import khabarhub.com.NetworkConnection;
import khabarhub.com.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class EnglishHotnewsFragment extends Fragment implements List_of_english_hot_news.OnItemClickListner, SwipeRefreshLayout.OnRefreshListener {

private RecyclerView mrecycleview_hotnews;
    private List_of_english_hot_news adapter;
    Dialog dialog;
    NetworkConnection networkConnection;
    boolean ntstatus;
    SwipeRefreshLayout mswiperefresh;
    EnglishJustnewsViewModel justInnewsViewModel;
    List<JustInNews_english> newslist;

    int currentpage =1;

    private int mPreviousTotal = 0;
    static int last_page;
    private boolean mLoading = true;
    int total=0;

    public EnglishHotnewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_recycle_view, container, false);
        mrecycleview_hotnews=view.findViewById(R.id.frag_recylce_view_recycleview);
        mswiperefresh=view.findViewById(R.id.swiperefresh_fragmentrecy);

        AndroidNetworking.initialize(getContext());
        networkConnection=new NetworkConnection(getContext());

        newslist= new ArrayList<>();
        prodressdialog();
      //  dialog.show();
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getContext());
        mrecycleview_hotnews.setLayoutManager(layoutManager);
        mrecycleview_hotnews.setHasFixedSize(true);

         justInnewsViewModel= ViewModelProviders.of(this).get(EnglishJustnewsViewModel.class);
        justInnewsViewModel.getenglishjustnews().observe(this, new Observer<List<JustInNews_english>>() {
            @Override
            public void onChanged(List<JustInNews_english> justInNews_englishes) {
                dialog.show();
                    newslist = justInNews_englishes;
                adapter=new List_of_english_hot_news(newslist,getContext(), EnglishHotnewsFragment.this );
                mrecycleview_hotnews.setAdapter(adapter);

                dialog.dismiss();

            }
        });



        mswiperefresh.setOnRefreshListener(this);
        mrecycleview_hotnews.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState==0){

                    onScrollrecycleview();
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

    }
    private void onScrollrecycleview() {

        int visibleItemCount = mrecycleview_hotnews.getChildCount();
        int totalItemCount = mrecycleview_hotnews.getLayoutManager().getItemCount();

        int firstVisibleItem = ((LinearLayoutManager) mrecycleview_hotnews.getLayoutManager()).findFirstVisibleItemPosition();

        if (mLoading) {

            if (total > totalItemCount) {
                mLoading = false;
                mPreviousTotal = totalItemCount;
            }

        }

        int visibleThreshold = 5;
        if ( (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + visibleThreshold)) {



            dialog.show();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    currentpage += 1;

                    makenewslist(currentpage);

                    mLoading = true;
                }


            }, 3000);


        }
        else {

        }
    }



    private void makenewslist(int currentpages) {
        AndroidNetworking.get("https://english.khabarhub.com/wp-json/appharurest/v2/posts/0/"+ currentpages)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        dialog.dismiss();
                        for (int i=0;i<response.length();i++){
                            try {
                                JSONObject jsonObject = response.getJSONObject(i);
                                int id=(jsonObject.getInt("id"));
                                String title=(jsonObject.optString("title"));
                                String img =(jsonObject.optString("image_link_medium"));

                                String date = (jsonObject.optString("date"));
                                String desc=(jsonObject.optString("body"));
                                String link=(jsonObject.optString("permalink"));
                                String authorname=(jsonObject.optString("author_name"));
                                String authorimg=(jsonObject.optString("author_image"));
                                JustInNews_english newsPojoClass = new JustInNews_english(id,title,date,img,link,desc,authorname,authorimg);

                                newslist.add(newsPojoClass);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        adapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                    }
                });
    }






    @Override
    public void onclick(String title, String date, String news, List<JustInNews_english> newslists, int clickposition, String url_link) {
        ntstatus=networkConnection.isOnline();
        if (ntstatus==true) {
            Intent intent = new Intent(getContext(), RelativeNews_JustIn_english.class);
            intent.putExtra("news_title", title);
            //  intent.putExtra("news_title",newslist.get(getAdapterPosition()).getNews_title());
            intent.putExtra("news_date", date);
            intent.putExtra("news_details",news);
            intent.putExtra("newslist", (Serializable) newslists);
            intent.putExtra("clickposition",clickposition);
            intent.putExtra("link",url_link);

            startActivity(intent);

        }
        else {
            Snackbar.make(getView(),"Something is Wrong",Snackbar.LENGTH_LONG).show();
        }
    }


    void prodressdialog()
    {
        ProgressBar progressBar=new ProgressBar(getContext());

        progressBar.setIndeterminate(true);
        progressBar.setScrollBarSize(2);
        dialog = new Dialog(getActivity());
        progressBar.setMinimumWidth(10);
        progressBar.setMinimumHeight(10);

        //  dialog.getWindow().setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL);
        dialog.getWindow().setBackgroundDrawable(new
                ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(progressBar);

        dialog.setCancelable(true);
        //  dialog.setContentView(R.layout.progressdialoglayout);


    }

    @Override
    public void onRefresh() {
        justInnewsViewModel.getenglishjustnews().observe(this, new Observer<List<JustInNews_english>>() {
            @Override
            public void onChanged(List<JustInNews_english> justInNews_englishes) {
                dialog.show();

                adapter=new List_of_english_hot_news(justInNews_englishes,getContext(), EnglishHotnewsFragment.this );
                mrecycleview_hotnews.setAdapter(adapter);
                mswiperefresh.setRefreshing(false);
                dialog.dismiss();

            }
        });

    }
}
