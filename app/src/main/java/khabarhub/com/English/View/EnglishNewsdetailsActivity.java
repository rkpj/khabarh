package khabarhub.com.English.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import khabarhub.com.Nepali.ApiOpseration;
import khabarhub.com.Nepali.Model.NewsDetails;
import khabarhub.com.NewsshareInterface;
import khabarhub.com.R;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class EnglishNewsdetailsActivity extends AppCompatActivity implements NewsshareInterface {

    Toolbar toolbar;
    ImageView imageView;
    TextView mtitle,mdate,fonticrease,fontdecrease;
    WebView mbody;
    ApiOpseration apiOpseration;
    List<NewsDetails> newsDetailsList;
    String news_title,news_date,news_body;
    NewsshareInterface newsshareInterface;
    String newsurl;
    Dialog dialog;
    WebSettings settings;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news_details_pages);
        toolbar=findViewById(R.id.toolbar_collsp);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Khabarhub");
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        imageView=findViewById(R.id.img_collapse);
        mtitle=findViewById(R.id.news_detail_title);
        mdate=findViewById(R.id.news_detail_id);
        mbody=findViewById(R.id.news_detail_body);
       // fonticrease=findViewById(R.id.news_fontincrease);
       // fontdecrease=findViewById(R.id.news_fontdecrease);
        prodressdialog();
        Intent i=getIntent();
        newsshareInterface=this;
        newsDetailsList=new ArrayList<>();
        Picasso.with(this).load(i.getStringExtra("news_image")).placeholder(R.drawable.logo).into(imageView);
        apiOpseration=new ApiOpseration(this);
        getnewsdetails(i.getExtras().getInt("news_id"));
         settings = mbody.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setLoadsImagesAutomatically(true);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
         int id = item.getItemId();

        if (id == R.id.menu_sharebutton) {
            Intent intent=new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
           // String uri= ("https://www.khabarhub.com/2019/21/31456/");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            intent.putExtra(Intent.EXTRA_TITLE,"Khabarhub");
            intent.putExtra(Intent.EXTRA_TEXT,newsurl);
            startActivity(Intent.createChooser(intent,"Share Using"));

            return true;
        }
        if (id==R.id.menu_fontdecreament){

            settings.setTextZoom(settings.getTextZoom() - 10);
        }
        if (id==R.id.menu_fontincreament){
            settings.setTextZoom(settings.getTextZoom() + 10);
        }

        return super.onOptionsItemSelected(item);
    }

    public  void getnewsdetails(int post_id){
        dialog.show();
        AndroidNetworking.get("https://english.khabarhub.com/wp-json/appharurest/v2/posts/"+post_id)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        dialog.dismiss();
                        news_date= response.optString("date");
                        news_title=response.optString("title");

                        news_body=response.optString("body");

                        mtitle.setText(""+news_title);
                        newsshareInterface.Seturl(response.optString("permalink"));

                        mdate.setText(""+news_date);
                        mbody.loadDataWithBaseURL(null, "<style>img{display: inline;height: auto;max-width: 100%;}p{text-align: justify;}</style>" +news_body,"text/html","utf-8",null);
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                    }
                });


    }

    void prodressdialog()
    {
        ProgressBar progressBar=new ProgressBar(this);

        progressBar.setIndeterminate(true);
        progressBar.setScrollBarSize(2);
        dialog = new Dialog(this);
        progressBar.setMinimumWidth(10);
        progressBar.setMinimumHeight(10);

        //  dialog.getWindow().setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL);
        dialog.getWindow().setBackgroundDrawable(new
                ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(progressBar);

        dialog.setCancelable(true);
        //  dialog.setContentView(R.layout.progressdialoglayout);


    }

    @Override
    public void Seturl(String url) {
        newsurl=url;
    }
}
