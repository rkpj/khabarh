package khabarhub.com.English.View;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import khabarhub.com.English.View.BreakingNews.EnglishBreakingNewsFlipView;
import khabarhub.com.English.View.CategoriesNes.Fragment_of_english_categories;
import khabarhub.com.English.View.HotNews.EnglishHotnewsFragment;

class CustomFragmentPageAdapterfor_english extends FragmentPagerAdapter {
    private static final String TAG = CustomFragmentPageAdapterfor_english.class.getSimpleName();
    private static final int FRAGMENT_COUNT = 15;

    public CustomFragmentPageAdapterfor_english(FragmentManager fm) {
        super(fm);
    }
    @Override
    public Fragment getItem(int position) {
        Fragment_of_english_categories fragment_of_english_categories=new Fragment_of_english_categories();
        Bundle b=new Bundle();

        switch (position){
            case 0:
                return  new EnglishBreakingNewsFlipView();
            case 1:
                return new EnglishHotnewsFragment();
            case 2:
                b.putInt("cat_id",13);
                fragment_of_english_categories.setArguments(b);
                return fragment_of_english_categories;
            case 3:

                b.putInt("cat_id",20);
                fragment_of_english_categories.setArguments(b);
                return fragment_of_english_categories;
            case 4:
                b.putInt("cat_id",4);
                fragment_of_english_categories.setArguments(b);
                return fragment_of_english_categories;

            case 5:
                b.putInt("cat_id",9);
                fragment_of_english_categories.setArguments(b);
                return fragment_of_english_categories;
            case 6:
                b.putInt("cat_id",150);
                fragment_of_english_categories.setArguments(b);
                return fragment_of_english_categories;


            case 7:
                b.putInt("cat_id",2);
                fragment_of_english_categories.setArguments(b);
                return fragment_of_english_categories;
            case 8:
                b.putInt("cat_id",149);
                fragment_of_english_categories.setArguments(b);
                return fragment_of_english_categories;

            case 9:
                b.putInt("cat_id",41);
                fragment_of_english_categories.setArguments(b);
                return fragment_of_english_categories;
            case 10:
                b.putInt("cat_id",49);
                fragment_of_english_categories.setArguments(b);
                return fragment_of_english_categories;

            case 11:
                b.putInt("cat_id",5);
                fragment_of_english_categories.setArguments(b);
                return fragment_of_english_categories;

            case 12:
                b.putInt("cat_id",15);
                fragment_of_english_categories.setArguments(b);
                return fragment_of_english_categories;


            case 13:
                b.putInt("cat_id",10);
                fragment_of_english_categories.setArguments(b);
                return fragment_of_english_categories;



            case 14:
                b.putInt("cat_id",16);
                fragment_of_english_categories.setArguments(b);
                return fragment_of_english_categories;






        }
        return null;
    }
    @Override
    public int getCount() {
        return FRAGMENT_COUNT;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "TOP STORIES";
            case 1:
                return "JUST IN";
            case 2:
                //id not found
                return "NEWS";
            case 3:
                return "POLITICS";
            case 4:
                return "BUSINESS";
            case 5:
                return "OPINION";
            case 6:
                return "INTERVIEW";
            case 7:
                return "INTERNATIONAL";
            case 8:
                return "FEATURE";
            case 9:
                return "HEALTH";
            case 10:
                return "TRAVEL";
            case 11:
                return "SPORTS";
            case 12:
                return "SCIENCE/TECHNOLOGY";
            case 13:
                //id not found
                return "ENTERTAINMENT";

             case 14:
                return "LIFESTYLE";





        }
        return null;
    }
}
