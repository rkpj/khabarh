package khabarhub.com.English.View.SaveNews;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import khabarhub.com.English.Model.Savenews_English_Dto;
import khabarhub.com.R;

public class Savenews_English_Adapter extends RecyclerView.Adapter<Savenews_English_Adapter.MyViewHolder> {

        List<Savenews_English_Dto> newslist;
        Context context;
        OnItemClickListner monClickListener;
public interface OnItemClickListner{
    void onclick(String title, String date, String news, List<Savenews_English_Dto> newslist, int positionclick, String news_link);
    void ondelete(int news_id);
}
    public Savenews_English_Adapter(List<Savenews_English_Dto> newslist, Context context, OnItemClickListner onItemClickListner) {
        this.newslist = newslist;
        this.context=context;
        monClickListener=onItemClickListner;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.savenews_wedgets,parent,false);

        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Savenews_English_Dto JustinNews=newslist.get(position);

        Picasso.with(context).load(JustinNews.getNews_img()).placeholder(R.drawable.opcity_logo).into(holder.img);
        holder.title.setText(Html.fromHtml(JustinNews.getNews_title()));
        holder.date.setText(JustinNews.getNews_date());
    }

    @Override
    public int getItemCount() {
        return newslist.size();
    }


public class MyViewHolder extends RecyclerView.ViewHolder {
    ImageView img,delete;
    TextView title, date;

    public MyViewHolder(@NonNull View itemView) {
        super(itemView);
        img = itemView.findViewById(R.id.card_news_img_save);
        title = itemView.findViewById(R.id.card_news_title_save);
        date = itemView.findViewById(R.id.card_news_date_save);
        delete=itemView.findViewById(R.id.deletenews_imgview);
        itemView.setTag(this);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                monClickListener.onclick(newslist.get(getAdapterPosition()).getNews_title(), newslist.get(getAdapterPosition()).getNews_date(), "adfsdf", newslist, getAdapterPosition(), newslist.get(getAdapterPosition()).getNews_link());

            }
        });
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                monClickListener.ondelete(newslist.get(getAdapterPosition()).getNew_id());
            }
        });

    }
}
}
