package khabarhub.com.English.View;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Calendar;
import java.util.List;

import khabarhub.com.Nepali.Model.ForexModel;
import khabarhub.com.Nepali.RepositoryPackage.GetForexRepo;
import khabarhub.com.Nepali.Views.DailyutilPage.Adapter_Forex;
import khabarhub.com.Nepali.Views.DailyutilPage.Forex_interface;
import khabarhub.com.R;
import khabarhub.com.Utils;

public class EnglishForexActivity extends AppCompatActivity implements Forex_interface {

    RecyclerView mforex_recycleview;
    ProgressBar progressBar;
    LinearLayout linearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forex);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        mforex_recycleview = findViewById(R.id.recy_forex);
        progressBar = findViewById(R.id.progressbar_forex);
        linearLayout =findViewById(R.id.linear_layout_forex);
        mforex_recycleview.setLayoutManager(new LinearLayoutManager(this));
        new ForexAsyncTask(this,this).execute();
        LayoutInflater inflator = (LayoutInflater) this .getSystemService(this.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.toolbar_custom, null);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setCustomView(v);
    }

    @Override
    public void setforex(List<ForexModel> forexModels) {
        if (forexModels!=null){
            mforex_recycleview.setAdapter(new Adapter_Forex(EnglishForexActivity.this,forexModels));
            progressBar.setVisibility(View.GONE);
            linearLayout.setVisibility(View.VISIBLE);
        }

    }

    private class ForexAsyncTask extends AsyncTask<Void,Void,Void> {
        Forex_interface forex_interface;
        Context context;

        public ForexAsyncTask(Forex_interface forex_interface, Context context) {
            this.forex_interface = forex_interface;
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
            int month = (Calendar.getInstance().get(Calendar.MONTH)+1);
            String months= String.format( "%02d",month);
            int day = (Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
            String days= String.format( "%02d",day);


            new GetForexRepo(context,forex_interface).getForexData(Utils.forex+year+"&MM="+months+"&DD="+days);
            return null;
        }
    }
}
