package khabarhub.com.English.View.HotNews;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import khabarhub.com.English.Model.JustInNews_english;
import khabarhub.com.R;

public class List_of_english_hot_news extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<JustInNews_english> newslist;
    Context context;
    private static final int TYPE_ONE = 1;
    private static final int TYPE_TWO = 2;

    OnItemClickListner monClickListener;
    public interface OnItemClickListner{
     //   void onclick(int news_id,String news_img );
        void onclick(String title,String date,String news,List<JustInNews_english> newslist,int positionclick,String news_link);

    }
    public List_of_english_hot_news(List<JustInNews_english> newslist, Context context, OnItemClickListner onItemClickListner) {
        this.newslist = newslist;
        this.context=context;
        monClickListener=onItemClickListner;
    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType==TYPE_ONE){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_single_news, parent, false);

            return new MyViewHolder2(view);
        }
        else {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_design_for_news, parent, false);
            return new MyViewHolder(view);
        }



    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()){
            case TYPE_ONE:
                setcardlist((MyViewHolder2) holder,position);
                break;
            case TYPE_TWO:
                setlistview((MyViewHolder) holder,position);
                break;

        }
    }

    public void setlistview(MyViewHolder holder, int position){
        JustInNews_english justInNews_english=newslist.get(position);
        Picasso.with(context).load(justInNews_english.getNews_img()).placeholder(R.drawable.opcity_logo).into(holder.img);
        holder.title.setText(Html.fromHtml(justInNews_english.getNews_title()));
        holder.date.setText(justInNews_english.getNews_date());
        holder.author_name.setText(justInNews_english.getAuthor_name());



    }
    public void setcardlist(MyViewHolder2 holder, int position){


        JustInNews_english justInNews_english=newslist.get(position);

        Picasso.with(context).load(justInNews_english.getNews_img()).placeholder(R.drawable.opcity_logo).into(holder.imgs);
        holder.titles.setText(Html.fromHtml(justInNews_english.getNews_title()));
        holder.dates.setText(justInNews_english.getNews_date());
        holder.author_name.setText(justInNews_english.getAuthor_name());
    }


    @Override
    public int getItemViewType(int position) {
        if (position==0)
        {
            return TYPE_ONE;
        }
        else {
            return TYPE_TWO;
        }
        //   return super.getItemViewType(position);
    }
    @Override
    public int getItemCount() {
        return newslist.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView title, date,author_name;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.card_news_img);
            title = itemView.findViewById(R.id.card_news_title);
            date = itemView.findViewById(R.id.card_news_date);
            author_name = itemView.findViewById(R.id.card_news_author_name);
            itemView.setTag(this);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    monClickListener.onclick(newslist.get(getAdapterPosition()).getNews_title(), newslist.get(getAdapterPosition()).getNews_date(),newslist.get(getAdapterPosition()).getNews_body(),newslist,getAdapterPosition(),newslist.get(getAdapterPosition()).getNews_link());

                }
            });
        }
    }

    public class MyViewHolder2 extends RecyclerView.ViewHolder {
        ImageView imgs;
        TextView titles, dates,author_name;

        public MyViewHolder2(@NonNull View itemView) {
            super(itemView);
            imgs = itemView.findViewById(R.id.card_news_img_singleview);
            titles = itemView.findViewById(R.id.card_news_title_singleview);
            dates = itemView.findViewById(R.id.card_news_date_singleview);
            author_name = itemView.findViewById(R.id.card_news_author_name_singleview);
            itemView.setTag(this);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    monClickListener.onclick(newslist.get(getAdapterPosition()).getNews_title(), newslist.get(getAdapterPosition()).getNews_date(),newslist.get(getAdapterPosition()).getNews_body(),newslist,getAdapterPosition(),newslist.get(getAdapterPosition()).getNews_link());

                }
            });
        }
    }



}












