package khabarhub.com.English.View.BreakingNews;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import khabarhub.com.English.Model.Dto_EnglishBreakingNews;
import khabarhub.com.Nepali.Views.Newsdetailspage_with_Related_News;
import khabarhub.com.Nepali.Views.NpCategoriesNews.RelatednewsActivity_with_news_details_page;
import khabarhub.com.R;
import khabarhub.com.radio.AudioManage;
import khabarhub.com.radio.MyService;

public class RelativeNews_Breaking extends AppCompatActivity implements TextToSpeech.OnInitListener {

    ViewPager pager;
    MyPageAdapter obj_adapter;
    String str_device;
    String str;
    String title,date,news,news_url;
    List<Dto_EnglishBreakingNews> newsarraylist;
    int position;
    int ACT_CHECK_TTS_DATA=0;
    private TextToSpeech mTTS;
    LinearLayoutCompat radio_linearlayout;
    AudioManage audioManage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_relatednews_with_news_details_page);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        Intent checkTTSIntent = new Intent();
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTTSIntent, ACT_CHECK_TTS_DATA);
        audioManage = AudioManage.INSTANCE(this);
        init();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    public  void speakWords(String speech) {

        Intent intent = new Intent(RelativeNews_Breaking.this, MyService.class);
        intent.setAction("com.android.music.playstatechanged");
        stopService(intent);
        radio_linearlayout.setVisibility(View.GONE);
        //speak straight away
        mTTS.speak(speech, TextToSpeech.QUEUE_FLUSH, null);
    }


    @Override
    public void onActivityReenter(int resultCode, Intent data) {
        super.onActivityReenter(resultCode, data);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        if (requestCode == ACT_CHECK_TTS_DATA) {
            if (resultCode ==
                    TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                // Data exists, so we instantiate the TTS engine
                mTTS = new TextToSpeech(this, this);
            } else {

                Intent installIntent = new Intent();
                installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installIntent);
            }
        }
    }

    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {
            if (mTTS != null) {
                int result = mTTS.setLanguage(Locale.US);
                if (result == TextToSpeech.LANG_MISSING_DATA || result ==
                        TextToSpeech.LANG_NOT_SUPPORTED) {
                    Toast.makeText(this, "TTS language is not supported",
                            Toast.LENGTH_LONG).show();
                } else {
                    // Do something here
                }
            }
        } else {
            Toast.makeText(this, "TTS initialization failed",
                    Toast.LENGTH_LONG).show();
        }
    }


    private void init() {
        pager = (ViewPager) findViewById(R.id.related_viewpager);
        Intent i=getIntent();
        title= i.getExtras().getString("news_title");
        date= i.getExtras().getString("news_date");
        news= i.getExtras().getString("news_details");
        position=i.getExtras().getInt("clickposition");
        news_url=i.getExtras().getString("link");
        newsarraylist= (List<Dto_EnglishBreakingNews>) i.getSerializableExtra("newslist");

        differentDensityAndScreenSize(getApplicationContext());
        List<Fragment> fragments = getFragments();
        pager.setAdapter(obj_adapter);
        //  pager.setCurrentItem(6);
        pager.setClipToPadding(false);
        pager.setPageMargin(5);

        radio_linearlayout = findViewById(R.id.layout_linear_buttomradio_newsdetailspage);
        AppCompatImageView play =findViewById(R.id.imgbtn_play_bottomradio_newsdetails_nepali);
        if(audioManage.getAudioFlag() == true){

            radio_linearlayout.setVisibility(View.VISIBLE);
            pager.setPadding(0,0,0,80);
            pager.setPageMargin(80);
        }
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RelativeNews_Breaking.this, MyService.class);
                intent.setAction("com.android.music.playstatechanged");
                stopService(intent);
                radio_linearlayout.setVisibility(View.GONE);
            }
        });

        if (str.equals("normal-hdpi")){
            pager.setPadding(20, 0, 20, 0);
        }else if (str.equals("normal-mdpi")){
            pager.setPadding(20, 0, 20, 0);
        }else if (str.equals("normal-xhdpi")){
            pager.setPadding(20, 0, 20, 0);
        }else if (str.equals("normal-xxhdpi")){
            pager.setPadding(20, 0, 20, 0);
        }else if (str.equals("normal-xxxhdpi")){
            pager.setPadding(20, 0, 20, 0);
        }
        else if (str.equals("normal-unknown")){
            pager.setPadding(20, 0, 20, 0);
        }


        obj_adapter = new MyPageAdapter(getSupportFragmentManager(), fragments);
        //  pager.setPageTransformer(true, new ExpandingViewPagerTransformer());
        pager.setAdapter(obj_adapter);
        pager.setCurrentItem(position);
    }
    class MyPageAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments;

        public MyPageAdapter(FragmentManager fm, List<Fragment> fragments) {

            super(fm);

            this.fragments = fragments;

        }

        @Override

        public Fragment getItem(int position) {

            return this.fragments.get(position);

        }

        @Override

        public int getCount() {

            return this.fragments.size();

        }

    }

    private List<Fragment> getFragments() {
        List<Fragment> fList = new ArrayList<Fragment>();

        for (int i=0;i<newsarraylist.size();i++){
            fList.add(EngNewsdetailspageFlipView.newInstance(newsarraylist.get(i).getNew_id(),newsarraylist.get(i).getNews_title(),newsarraylist.get(i).getNews_date(),newsarraylist.get(i).getNews_body(),newsarraylist.get(i).getNews_link(),newsarraylist.get(i).getNews_img(),newsarraylist.get(i).getAuthor_name()));

        }

        return fList;

    }

    public int differentDensityAndScreenSize(Context context) {
        int value = 20;

        if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL) {
            switch (context.getResources().getDisplayMetrics().densityDpi) {
                case DisplayMetrics.DENSITY_LOW:
                    str = "small-ldpi";
                    value = 20;
                    break;
                case DisplayMetrics.DENSITY_MEDIUM:
                    str = "small-mdpi";
                    value = 20;
                    break;
                case DisplayMetrics.DENSITY_HIGH:
                    str = "small-hdpi";
                    value = 20;
                    break;
                case DisplayMetrics.DENSITY_XHIGH:
                    str = "small-xhdpi";
                    value = 20;
                    break;
                case DisplayMetrics.DENSITY_XXHIGH:
                    str = "small-xxhdpi";
                    value = 20;
                    break;
                case DisplayMetrics.DENSITY_XXXHIGH:
                    str = "small-xxxhdpi";
                    value = 20;
                    break;
                case DisplayMetrics.DENSITY_TV:
                    str = "small-tvdpi";
                    value = 20;
                    break;
                default:
                    str = "small-unknown";
                    value = 20;
                    break;
            }

        } else if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            switch (context.getResources().getDisplayMetrics().densityDpi) {
                case DisplayMetrics.DENSITY_LOW:
                    str = "normal-ldpi";
                    str_device = "normal-ldpi";
                    value = 82;
                    break;
                case DisplayMetrics.DENSITY_MEDIUM:
                    str = "normal-mdpi";
                    value = 82;
                    str_device = "normal-mdpi";
                    break;
                case DisplayMetrics.DENSITY_HIGH:
                    str = "normal-hdpi";
                    str_device = "normal-hdpi";
                    value = 82;
                    break;
                case DisplayMetrics.DENSITY_XHIGH:
                    str = "normal-xhdpi";
                    str_device = "normal-xhdpi";
                    value = 90;
                    break;
                case DisplayMetrics.DENSITY_XXHIGH:
                    str = "normal-xxhdpi";
                    str_device = "normal-xxhdpi";
                    value = 96;
                    break;
                case DisplayMetrics.DENSITY_XXXHIGH:
                    str = "normal-xxxhdpi";
                    str_device = "normal-xxxhdpi";
                    value = 96;
                    break;
                case DisplayMetrics.DENSITY_TV:
                    str = "normal-tvdpi";
                    str_device = "normal-tvmdpi";
                    value = 96;
                    break;
                default:
                    str = "normal-unknown";
                    str_device = "normal-unknown";
                    value = 82;
                    break;
            }
        } else if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            switch (context.getResources().getDisplayMetrics().densityDpi) {
                case DisplayMetrics.DENSITY_LOW:
                    str = "large-ldpi";
                    value = 78;
                    break;
                case DisplayMetrics.DENSITY_MEDIUM:
                    str = "large-mdpi";
                    value = 78;
                    break;
                case DisplayMetrics.DENSITY_HIGH:
                    str = "large-hdpi";
                    value = 78;
                    break;
                case DisplayMetrics.DENSITY_XHIGH:
                    str = "large-xhdpi";
                    value = 125;
                    break;
                case DisplayMetrics.DENSITY_XXHIGH:
                    str = "large-xxhdpi";
                    value = 125;
                    break;
                case DisplayMetrics.DENSITY_XXXHIGH:
                    str = "large-xxxhdpi";
                    value = 125;
                    break;
                case DisplayMetrics.DENSITY_TV:
                    str = "large-tvdpi";
                    value = 125;
                    break;
                default:
                    str = "large-unknown";
                    value = 78;
                    break;
            }

        } else if ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_XLARGE) {
            switch (context.getResources().getDisplayMetrics().densityDpi) {
                case DisplayMetrics.DENSITY_LOW:
                    str = "xlarge-ldpi";
                    value = 125;
                    break;
                case DisplayMetrics.DENSITY_MEDIUM:
                    str = "xlarge-mdpi";
                    value = 125;
                    break;
                case DisplayMetrics.DENSITY_HIGH:
                    str = "xlarge-hdpi";
                    value = 125;
                    break;
                case DisplayMetrics.DENSITY_XHIGH:
                    str = "xlarge-xhdpi";
                    value = 125;
                    break;
                case DisplayMetrics.DENSITY_XXHIGH:
                    str = "xlarge-xxhdpi";
                    value = 125;
                    break;
                case DisplayMetrics.DENSITY_XXXHIGH:
                    str = "xlarge-xxxhdpi";
                    value = 125;
                    break;
                case DisplayMetrics.DENSITY_TV:
                    str = "xlarge-tvdpi";
                    value = 125;
                    break;
                default:
                    str = "xlarge-unknown";
                    value = 125;
                    break;
            }
        }

        return value;
    }

    void stopAudio(){
        if (mTTS !=null){
            mTTS.stop();

        }
    }
    @Override
    protected void onDestroy() {
        if (mTTS !=null){
            mTTS.stop();
            mTTS.shutdown();
        }
        super.onDestroy();
    }
}
