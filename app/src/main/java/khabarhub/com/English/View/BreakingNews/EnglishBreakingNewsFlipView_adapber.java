package khabarhub.com.English.View.BreakingNews;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.cardview.widget.CardView;

import com.squareup.picasso.Picasso;

import java.util.List;

import khabarhub.com.English.Model.Dto_EnglishBreakingNews;
import khabarhub.com.English.Model.JustInNews_english;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.BreakingNewsDto;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.JustInNewsDto;
import khabarhub.com.R;
import khabarhub.com.radio.AudioManage;

public class EnglishBreakingNewsFlipView_adapber extends BaseAdapter {
    Context context;
    List<Dto_EnglishBreakingNews> list;
    List<JustInNews_english>justInNewsDtoList;
    OnItemClickListner monClickListener;
    int pos;
    int currrentpos=0;

    public interface OnItemClickListner {
        void onclickBreakMain(String title, String date, String news, List<Dto_EnglishBreakingNews> newslists, int clickposition, String url_link);
        void onclickBreakRelated(String title, String date, String news, List<JustInNews_english> newslists, int clickposition, String url_link);

    }

    public EnglishBreakingNewsFlipView_adapber(Context context, List<Dto_EnglishBreakingNews> list, List<JustInNews_english> justInNewsDtos, OnItemClickListner monClickListener) {
        this.context = context;
        this.list = list;
        this.justInNewsDtoList=justInNewsDtos;
        this.monClickListener = monClickListener;
    }

    @Override
    public int getCount() {
        if (list.size()>6) {
            return 5;
        }
        else {
            return list.size();
        }

    }

    @Override
    public Object getItem(int position) {
        return 0;
    }

    @Override
    public long getItemId(int position) {
       return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
        if (convertView ==null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.nepalibreakingnews_flipview_adapter,parent,false);
            holder.imageView1 = convertView.findViewById(R.id.img_nepalibreaking_flipview_image);
            holder.title1 =convertView.findViewById(R.id.txt_nepalibreaking_flipview_title);
            holder.date1 = convertView.findViewById(R.id.txt_nepalibreaking_flipview_date);

            holder.desc1 = convertView.findViewById(R.id.desc_npbreak_1);
            holder.cardView1 = convertView.findViewById(R.id.cardview_nepalibreaking_adapter_1news);

            holder.cardView2 = convertView.findViewById(R.id.cardview_nepalibreaking_adapter_2news);
            holder.imageView2 = convertView.findViewById(R.id.btn_nepalibreaking_flipview_2news_img);
            holder.title2 =convertView.findViewById(R.id.btn_nepalibreaking_flipview_2news_title);
            holder.date2 = convertView.findViewById(R.id.btn_nepalibreaking_flipview_2news_date);

            holder.cardView3 = convertView.findViewById(R.id.cardview_nepalibreaking_adapter_3news);
            holder.imageView3 = convertView.findViewById(R.id.btn_nepalibreaking_flipview_3news_img);
            holder.title3 =convertView.findViewById(R.id.btn_nepalibreaking_flipview_3news_title);
            holder.date3 = convertView.findViewById(R.id.btn_nepalibreaking_flipview_3news_date);


            if(AudioManage.INSTANCE(context).getAudioFlag() == true){
                //  LinearLayout.LayoutParams linear = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,0,5);
                holder.linearLayout = convertView.findViewById(R.id.linear_nepali_flipview);
                holder.liner_sideby_side = convertView.findViewById(R.id.linear_flip_other_sidebyside);
                LinearLayoutCompat.LayoutParams linear= (LinearLayoutCompat.LayoutParams) holder.linearLayout.getLayoutParams();

                linear.weight = 4f;
                holder.linearLayout.setLayoutParams(linear);
                holder.desc1.setVisibility(View.GONE);

            }
            convertView.setTag(holder);
        }
        else {
           holder = (ViewHolder) convertView.getTag();
        }

        if (position ==0){
            setValues(holder,position,position,position+1);
        }

        if (position ==1){
            setValues(holder,position,position+1,position+2);
        }
        if (position ==2){
            setValues(holder,position,position+2,position+3);
        }
        if (position ==3){
            setValues(holder,position,position+3,position+4);
        }
        if (position ==4){
            setValues(holder,position,position+4,position+5);
        }





        return convertView;
    }

    private void setValues(ViewHolder holder, int pos, int i, int i1) {
                    holder.title1.setText(""+list.get(pos).getNews_title());
            holder.date1.setText(""+list.get(pos).getNews_date());
        holder.desc1.setText(""+list.get(pos).getNews_desc());

        Picasso.with(context).load(list.get(pos).getNews_img()).placeholder(R.drawable.opcity_logo).into(holder.imageView1);
            holder.cardView1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    monClickListener.onclickBreakMain(list.get(pos).getNews_title(),list.get(pos).getNews_date(),list.get(pos).getNews_body(),list,pos,list.get(pos).getNews_link());
                }
            });


            holder.title2.setText(""+justInNewsDtoList.get(i).getNews_title());
            holder.date2.setText(""+justInNewsDtoList.get(i).getNews_date());
            Picasso.with(context).load(justInNewsDtoList.get(i).getNews_img()).placeholder(R.drawable.opcity_logo).into(holder.imageView2);
        holder.imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                monClickListener.onclickBreakRelated(justInNewsDtoList.get(i).getNews_title(),justInNewsDtoList.get(i).getNews_date(),justInNewsDtoList.get(i).getNews_body(),justInNewsDtoList,i,justInNewsDtoList.get(i).getNews_link());

            }
        });

        holder.cardView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    monClickListener.onclickBreakRelated(justInNewsDtoList.get(i).getNews_title(),justInNewsDtoList.get(i).getNews_date(),justInNewsDtoList.get(i).getNews_body(),justInNewsDtoList,i,justInNewsDtoList.get(i).getNews_link());
                }
            });


            holder.title3.setText(""+justInNewsDtoList.get(i1).getNews_title());
            holder.date3.setText(""+justInNewsDtoList.get(i1).getNews_date());
            Picasso.with(context).load(justInNewsDtoList.get(i1).getNews_img()).placeholder(R.drawable.opcity_logo).into(holder.imageView3);
           holder.imageView3.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   monClickListener.onclickBreakRelated(justInNewsDtoList.get(i1).getNews_title(),justInNewsDtoList.get(i1).getNews_date(),justInNewsDtoList.get(i1).getNews_body(),justInNewsDtoList,i1,justInNewsDtoList.get(i1).getNews_link());

               }
           });

            holder.cardView3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    monClickListener.onclickBreakRelated(justInNewsDtoList.get(i1).getNews_title(),justInNewsDtoList.get(i1).getNews_date(),justInNewsDtoList.get(i1).getNews_body(),justInNewsDtoList,i1,justInNewsDtoList.get(i1).getNews_link());
                }
            });
    }

    static class ViewHolder {
        TextView title1,date1,desc1,title2,date2,title3,date3;
        ImageView imageView1,imageView2,imageView3;
     //   TextView webView;
        CardView cardView1,cardView2,cardView3;
        LinearLayoutCompat linearLayout;
        LinearLayout liner_sideby_side;
    }
}
