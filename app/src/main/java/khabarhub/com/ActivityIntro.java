package khabarhub.com;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import khabarhub.com.Nepali.Views.SplashActivity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;

import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class ActivityIntro extends AppCompatActivity {

    ViewPager viewPager;
    Button nxt_btn,startbtn;
    TabLayout tabLayout;
    Introview_PageAdapter introview_pageAdapter;
    List<ScreenItem> mlist;
    int position=0;


    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    private static final int ALL_PERMISSIONS_RESULT = 1011;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (restoresharepref()==true){

            Intent i=new Intent(getApplicationContext(),SplashActivity.class);
            startActivity(i);
            finish();

        }
        setContentView(R.layout.activity_intro);
        viewPager=findViewById(R.id.intro_viewpager);
        nxt_btn=findViewById(R.id.next_btn);
        tabLayout=findViewById(R.id.tabLayout);
        startbtn=findViewById(R.id.intro_startbtn);

        mlist=new ArrayList<>();

        mlist.add(new ScreenItem("Welcome To Khabarhub","",R.drawable.khabarlogo1));
        mlist.add(new ScreenItem("","",R.drawable.intro1));
      //  mlist.add(new ScreenItem("","",R.drawable.intropage2));

        introview_pageAdapter=new Introview_PageAdapter(this,mlist);
        viewPager.setAdapter(introview_pageAdapter);
        tabLayout.setupWithViewPager(viewPager);

        nxt_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                position=viewPager.getCurrentItem();
                if (position<mlist.size()){
                    position++;
                    viewPager.setCurrentItem(position);
                    loadfirstscreen();
                }
                if (position==mlist.size()-1){


                    loadlastscreen();


                }
            }
        });
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition()==mlist.size()-1){
                    loadlastscreen();

                }
                else {
                    loadfirstscreen();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        startbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityIntro.this, SplashActivity.class));

                sharepref();
                finish();

            }
        });
    }

    private boolean restoresharepref() {
        SharedPreferences sharedPreferences=getSharedPreferences("khabar_intropage",MODE_PRIVATE);
        Boolean isopen=sharedPreferences.getBoolean("alreadyview",false);
        return isopen;
    }

    private void loadfirstscreen() {
        startbtn.setVisibility(View.INVISIBLE);
        tabLayout.setVisibility(View.VISIBLE);
        nxt_btn.setVisibility(View.VISIBLE);



    }

    private void loadlastscreen() {
        startbtn.setVisibility(View.VISIBLE);
        tabLayout.setVisibility(View.INVISIBLE);
        nxt_btn.setVisibility(View.INVISIBLE);


        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION);

        permissionsToRequest = permissionsToRequest(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0) {
                requestPermissions(permissionsToRequest.
                        toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
            }
        }
    }

    private void sharepref(){
        SharedPreferences sharedPreferences=getSharedPreferences("khabar_intropage",MODE_PRIVATE);
        SharedPreferences.Editor editor=sharedPreferences.edit();
        editor.putBoolean("alreadyview",true);
        editor.commit();
    }

    private ArrayList<String> permissionsToRequest(ArrayList<String> wantedPermissions) {
        ArrayList<String> result = new ArrayList<>();

        for (String perm : wantedPermissions) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }
    private boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED;
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode) {
            case ALL_PERMISSIONS_RESULT:
                for (String perm : permissionsToRequest) {
                    if (!hasPermission(perm)) {
                        permissionsRejected.add(perm);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            new AlertDialog.Builder(ActivityIntro.this).
                                    setMessage("These permissions are mandatory to get your location. You need to allow them.").
                                    setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(permissionsRejected.
                                                        toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    }).setNegativeButton("Cancel", null).create().show();

                            return;
                        }
                    }
                }

        }
    }
}
