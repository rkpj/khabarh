package khabarhub.com;

import android.content.Context;
import android.content.SharedPreferences;

public class Sharepref_sessions {



        public static Sharepref_sessions instance=null;
        Context context;
        SharedPreferences sharedPreferences;
        SharedPreferences.Editor editor;

        public Sharepref_sessions(Context context) {
            this.context = context;
        }
        public static Sharepref_sessions Instance(Context context){
            if (instance==null){
                instance=new Sharepref_sessions(context);

            }
            return instance;
        }


        public void setnotificationstate(Boolean notificationstate){
            sharedPreferences=context.getSharedPreferences("Khabarhubs",Context.MODE_PRIVATE);
            editor=sharedPreferences.edit();
            editor.putBoolean("notificationstate",notificationstate);
            editor.commit();
        }

        public Boolean getnotificationstate(){
            sharedPreferences=context.getSharedPreferences("Khabarhubs",Context.MODE_PRIVATE);
            Boolean notificationstate=sharedPreferences.getBoolean("notificationstate",true);
            return notificationstate;
        }

        public void setlatlong(String lat,String longs){
            sharedPreferences=context.getSharedPreferences("Khabarhubs",Context.MODE_PRIVATE);
            editor=sharedPreferences.edit();
            editor.putString("lat",lat);
            editor.putString("long",longs);
            editor.commit();
        }
        public String getlat(){
            sharedPreferences=context.getSharedPreferences("Khabarhubs",Context.MODE_PRIVATE);
            String lat=sharedPreferences.getString("lat",null);
            return lat;
        }

        public String getlong(){
            sharedPreferences=context.getSharedPreferences("Khabarhubs",Context.MODE_PRIVATE);
            String longs=sharedPreferences.getString("long",null);
            return longs;
        }

        public void setWeather(String icon,String temp){
            sharedPreferences=context.getSharedPreferences("Khabarhubs",Context.MODE_PRIVATE);
            editor=sharedPreferences.edit();
            editor.putString("temp_icon",icon);
            editor.putString("temp_degree",temp);
            editor.commit();
        }
    public String getTemp(){
        sharedPreferences=context.getSharedPreferences("Khabarhubs",Context.MODE_PRIVATE);
        String temp_degree=sharedPreferences.getString("temp_degree",null);
        return temp_degree;
    }
    public String gettempIcon(){
        sharedPreferences=context.getSharedPreferences("Khabarhubs",Context.MODE_PRIVATE);
        String temp_icon=sharedPreferences.getString("temp_icon",null);
        return temp_icon;
    }
    }



