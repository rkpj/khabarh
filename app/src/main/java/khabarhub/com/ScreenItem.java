package khabarhub.com;

public class ScreenItem {
    String title,describtion;
    int screenimg;

    public ScreenItem(String title, String describtion, int screenimg) {
        this.title = title;
        this.describtion = describtion;
        this.screenimg = screenimg;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescribtion() {
        return describtion;
    }

    public void setDescribtion(String describtion) {
        this.describtion = describtion;
    }

    public int getScreenimg() {
        return screenimg;
    }

    public void setScreenimg(int screenimg) {
        this.screenimg = screenimg;
    }
}
