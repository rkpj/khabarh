package khabarhub.com.calendarnepali;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import khabarhub.com.R;


public class WeekAdapter extends RecyclerView.Adapter<WeekAdapter.MyViewHolder> {


    Context context;
    List<String> weeklist;

    public WeekAdapter(Context context, List<String> weeklist) {
        this.context = context;
        this.weeklist = weeklist;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.date_weeks,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.dateholder.setText(""+weeklist.get(position));

    }

    @Override
    public int getItemCount() {
        return weeklist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView dateholder;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            dateholder = itemView.findViewById(R.id.day_week);
        }
    }
}
