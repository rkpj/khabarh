package khabarhub.com.calendarnepali;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Calendar;

import khabarhub.com.Nepali.Views.DailyutilPage.Adapter_holidays;
import khabarhub.com.R;
import khabarhub.com.radio.AudioManage;


public class CalendarActivity extends Fragment {
    String[] weeks = {"आइत","सोम","मंगल","बुध","बिहि","शुक्र","शनि"};
    String[] engmon = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

    ImageButton prev,next;
    TextView month_year;
    CardView holiday;
    CardView holidayexpand;
    DateConverter dc;
    RecyclerView mRecycleview_week;
    RecyclerView mRecycleview_day;
    RecyclerView mrecycleview_holiday;
    int year,month,day,startdatno,noofdayinmonth,engstartdate,engenddate;
   // int dayofdate,dayweek,npdayofweek;
   DayAdapter dayAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view= inflater.inflate(R.layout.activity_calendar, container, false);
        prev= view.findViewById(R.id.btn_prev);
        next = view.findViewById(R.id.btn_next);
        month_year = view.findViewById(R.id.txt_monthyear);
        mRecycleview_day = view.findViewById(R.id.dayRecycle);
        mRecycleview_week =view. findViewById(R.id.weekRecycle);
        holiday = view.findViewById(R.id.cardholiday);
        holidayexpand = view.findViewById(R.id.holidayexpand);
        mrecycleview_holiday =view.findViewById(R.id.recyholiday);
        mRecycleview_week.setLayoutManager(new GridLayoutManager(getContext(),7));
        mRecycleview_week.setAdapter(new WeekAdapter(getContext(), Arrays.asList(weeks)));
//
        dc = new DateConverter();
        Model mCalendar;
        mCalendar= dc.getTodayNepaliDate();
        year =mCalendar.getYear();
        month =mCalendar.getMonth()+1;
        mRecycleview_day.setLayoutManager(new GridLayoutManager(getContext(),7));

        getdates(year,month);
        mrecycleview_holiday.setLayoutManager(new LinearLayoutManager(getContext()));
       mrecycleview_holiday.setAdapter(new Adapter_holidays(getContext(), CalendarActivity.this.getResources().getStringArray(R.array.holiday)));
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (month ==1){
                    year--;
                    month=12;
                }
                else {
                    month--;
                }
                getdates(year,month);


            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (month ==12){
                    year++;
                    month=1;
                }
                else {
                    month++;
                }
                getdates(year,month);

            }
        });





        return view;
    }

    void getdates(int year,int month){
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                String years= getStringToNelaliFromNumber(year);
                startdatno =dc.getFirstWeekDayMonth(year,month);
                noofdayinmonth =dc.noOfDaysInMonth(year,month);
                int currentday = dc.getTodayNepaliDate().getDay();
                engenddate =dc.getEnglishDate(year,month,noofdayinmonth).getDay();
                engstartdate = dc.getEnglishDate(year,month,1).getDay();
                int engyear = dc.getEnglishDate(year,month,1).getYear();
                int  engmonth = dc.getEnglishDate(year,month,1).getMonth();
                int  engnextmonth = dc.getEnglishDate(year,month,noofdayinmonth).getMonth();
                 month_year.setText(""+getResources().getString(dc.getNepaliMonthString(month-1)) +" "+years+" - "+engmon[engmonth]+"/"+engmon[engnextmonth]+" "+engyear);

                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, engyear);
                calendar.set(Calendar.MONTH, engmonth);
                int engnumDays = calendar.getActualMaximum(Calendar.DATE);
                setdate(year,month,noofdayinmonth,startdatno,engstartdate,engnumDays,currentday);
                // update the ui from here
            }
        });

    }
    void setdate(final int npyear, final int npmonth, final int noofdayinmonth, final int startdatno, final int engstartdate, final int engnumdays, final int currentday ){

        new Handler().post(new Runnable() {
            @Override
            public void run() {
                dayAdapter = new DayAdapter(getContext(),noofdayinmonth,startdatno,engstartdate,engnumdays,npyear,npmonth,currentday);
                mRecycleview_day.setAdapter(dayAdapter);

            }
        });
    }
        public static String getStringToNelaliFromNumber(int number) {
        String[] nepaliNumArray = new String[] { "०", "१", "२", "३", "४", "५", "६", "७", "८", "९" };
        StringBuilder result = new StringBuilder();
        while (number > 0) {
            int n = number % 10;
            result.insert(0, nepaliNumArray[n]);
            number /= 10;
        }
        return result.toString();
    }

    @Override
    public void onStart() {
        super.onStart();

    }
}
