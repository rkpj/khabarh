package khabarhub.com.calendarnepali;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import khabarhub.com.R;


public class DayAdapter extends RecyclerView.Adapter<DayAdapter.MyViewHolder> {


    Context context;
    List<Integer> engdaylist;
    int totalDaysInMonth;
    int npstartDayofMonth;
    int engstartdate;
    int engenddate;
    int npyear;
    int npmonth;
    int currentday;
    int start=1;
    int remainingday;
    DateConverter dateConverter = new DateConverter();
    List<SelectedRecyItemModel> selectedRecyItemModels = new ArrayList<>();

    int selected = 0;
    String[] npdates= {"०","१","२","३","४","५","६","७","८","९","१०","११","१२","१३","१४","१५","१६","१७","१८","१९","२०","२१","२२","२३","२४","२५","२६","२७","२८","२९","३०","३१","३२"};



    public DayAdapter(Context context, int totalDaysInMonth, int npstartDayofMonth, int engstartdate, int engenddate, int npyear, int npmonth,int currentday) {
        this.context = context;
        this.totalDaysInMonth = totalDaysInMonth;
        this.npstartDayofMonth = npstartDayofMonth;
        this.engstartdate = engstartdate;
        this.engenddate = engenddate;
        this.npyear = npyear;
        this.npmonth = npmonth;
        this.currentday = currentday;
      //  this.engdaylist=engdates;
        remainingday = engenddate-engstartdate;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.dayslayout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        if (npstartDayofMonth-1>position){

        }
        else {
            int npd=position+2-npstartDayofMonth;
            holder.dateholder.setText(npdates[npd]);
            if (dateConverter.getWeekDay(npyear,npmonth,npd) ==7){
                holder.dateholder.setTextColor(Color.RED);
            }
            if(npd ==currentday){
               holder.linearLayout.setBackgroundResource(R.color.blue);
               holder.dateholder.setTextColor(Color.parseColor("#FFFFFF"));
                holder.engdate.setTextColor(Color.parseColor("#FFFFFF"));

            }
            if(remainingday+1>0){
                holder.engdate.setText(""+engstartdate);
                engstartdate++;
                remainingday--;
            }
            else if(remainingday+1<=0){
                holder.engdate.setText(""+start);
                start++;
            }

        }
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


    }

    @Override
    public int getItemCount() {
        return (totalDaysInMonth+npstartDayofMonth-1);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView dateholder;
        TextView engdate;
        LinearLayout linearLayout;
        RelativeLayout relativeLayout;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            relativeLayout = itemView.findViewById(R.id.npdate_relate);
            linearLayout = itemView.findViewById(R.id.linear);
            dateholder = itemView.findViewById(R.id.npday);
            engdate = itemView.findViewById(R.id.engday);




        }
    }
}
