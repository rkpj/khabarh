package khabarhub.com;

import android.app.Application;
import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONException;

public class ApplicationClass extends Application {

    Sharepref_sessions sharepref_sessions;
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        sharepref_sessions=Sharepref_sessions.Instance(this);

        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(sharepref_sessions.getnotificationstate())

                .setNotificationReceivedHandler(new OneSignal.NotificationReceivedHandler() {
                    @Override
                    public void notificationReceived(OSNotification notification) {
                        Log.d("Khabarhub_log","rec:"+notification.payload.title);
                        try {
                            Log.d("Khabarhub_log","rec:"+notification.payload.additionalData.getJSONObject("data"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("Khabarhub_log","rec:"+e.getMessage());

                        }
                        Log.d("Khabarhub_log","rec:"+notification.payload.additionalData.toString());
                        Log.d("Khabarhub_log","rec:"+notification.payload.groupMessage);
                        Log.d("Khabarhub_log","rec:"+notification.androidNotificationId);
                        Log.d("Khabarhub_log","rec:"+notification.displayType.name());
                        Log.d("Khabarhub_log","rec:"+notification.groupedNotifications.size());
                        Log.d("Khabarhub_log","rec:"+notification.payload.launchURL);
                        Log.d("Khabarhub_log","rec:"+notification.payload.body);
                        Log.d("Khabarhub_log","rec:"+notification.payload.smallIcon);

                        Log.d("Khabarhub_log","rec:"+notification.payload.notificationID);
                        Log.d("Khabarhub_log","rec:"+notification.payload.bigPicture);
                        Log.d("Khabarhub_log","rec:"+notification.payload.fromProjectNumber);

                    }
                })
                .setNotificationOpenedHandler(new PushNotificationOpenHandler())
                .init();
    }

    private class PushNotificationOpenHandler implements OneSignal.NotificationOpenedHandler {
        @Override
        public void notificationOpened(OSNotificationOpenResult result) {
            Log.d("Khabarhub_log","id:"+result.notification.payload.notificationID);
            try {
                Log.d("Khabarhub_log","id:"+result.notification.payload.additionalData.getJSONObject("data"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("Khabarhub_log","id:"+result.notification.payload.launchURL);
            Log.d("Khabarhub_log","id:"+result.notification.payload.groupMessage);
            Log.d("Khabarhub_log","ids:"+result.notification.payload.additionalData.optString("id"));
            Log.d("Khabarhub_log","ids:"+result.notification.payload.additionalData.toString());

            Log.d("Khabarhub_log","id:"+result.notification.payload.templateId);


            Log.d("Khabarhub_log","id:"+result.notification.payload.templateName);
            Log.d("Khabarhub_log","id:"+result.notification.payload.title);
            Log.d("Khabarhub_log","id:"+result.notification.payload.actionButtons);
            Log.d("Khabarhub_log","id:"+result.notification.androidNotificationId);


            Log.d("Khabarhub_log","id:"+result.notification.displayType.name());
            Log.d("Khabarhub_log","id:"+result.notification.groupedNotifications.size());
            Log.d("Khabarhub_log","id:"+result.action.actionID);
            Log.d("Khabarhub_log","id:"+result.action.type);


        }
    }
}
