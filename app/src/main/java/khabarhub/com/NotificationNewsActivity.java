package khabarhub.com;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.text.Html;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.Locale;
import java.util.zip.Inflater;

import khabarhub.com.English.Model.Savenews_English_Dao;
import khabarhub.com.English.Model.Savenews_English_Dto;
import khabarhub.com.English.View.BreakingNews.RelativeNews_Breaking;
import khabarhub.com.English.View.EnglishMainActivity;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.JustInDb;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.NepaliNewsSaveDao;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.NepaliNewsSaveDto;
import khabarhub.com.Nepali.Views.MainActivity;
import khabarhub.com.Nepali.Views.Newsdetailspage_with_Related_News;
import khabarhub.com.Nepali.Views.NpCategoriesNews.RelatednewsActivity_with_news_details_page;
import khabarhub.com.radio.MyService;

public class NotificationNewsActivity extends AppCompatActivity implements TextToSpeech.OnInitListener{

    private int news_id=1;
    private String mtitle="मन्त्रीहरु हेरफेर हुने हल्ला सुनियोजित: प्रधानमन्त्री";
    private String mdate="342";
    private String mAuthor="34234";
    private String news="<p><strong>काठमाडौं–</strong> प्रधानमन्त्री केपी शर्मा ओलीले मन्त्री हेरफेरबारे सुनियोजित रुपमा हल्ला फैलाइएको भन्दै त्यसबाट प्रभावित नभई काम गर्न मन्त्रीहरुलाई निर्देशन दिएका छन् ।</p>\\n<p>आजको मन्त्रिपरिषद् बैठकमा प्रधानमन्त्री ओलीले मन्त्री हेरफेर हुने हल्लाले तरंगित पार्दै मन्त्रीहरुलाई निराश पारिएको धारणा राखेको एक मन्त्रीले खबरहबलाई बताए ।</p>\\n<p>&#8216;प्रधानमन्त्री ज्यूले बजारमा अनेक हल्ला चलाई मन्त्रीहरुको ध्यान अन्यत्र तान्ने र तरंगित पार्ने काम भइरहेकोमा सचेत हुँदै आफ्नो काममा निरन्तरता दिन आग्रह गर्नुभयो,&#8217; ती मन्त्रीले भने ।</p>\\n<p>केही दिनयता प्रधानमन्त्रीले मन्त्रीहरू हेरफेर गर्न लागेको चर्चा चलेको थियो । यस्तो हल्ला चलेपछि ओलीले तत्का\u200Cल मन्त्रीहरु हेरफेर नहुने स्पष्ट पारेका हुन् ।</p>\\n";
    private String news_url="https://www.khabarhub.com/?p=199911";
    private String imgs="https://i0.wp.com/sgp1.digitaloceanspaces.com/appharus1/khubs3/uploads/2020/04/kp-oli.jpg?resize=400%2C220&ssl=1";
    private WebSettings settings;

    LanguageSharepreference languageSharepreference;
    String language;
    MenuItem voice_menu,pause_menu;
    int ACT_CHECK_TTS_DATA=0;
    private TextToSpeech mTTS;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_newsdetailspage_with_related_news);

        TextView title=findViewById(R.id.txt_news_detail_title);
        TextView date=findViewById(R.id.txt_news_detail_date);
        TextView author_name=findViewById(R.id.txt_news_detail_author_name);

        ImageView img=findViewById(R.id.img_relatednews);
        WebView webView=findViewById(R.id.related_news_detail_body);
        languageSharepreference=LanguageSharepreference.Instance(this);
        language=languageSharepreference.getlanguage();

        settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setLoadsImagesAutomatically(true);
        webView.setWebViewClient(new WebViewClient());
        title.setText(mtitle);
        title.setTextSize(24);
        date.setText(mdate);
        author_name.setText(mAuthor+" | काठमाडौँ");
        Intent checkTTSIntent = new Intent();
        checkTTSIntent.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(checkTTSIntent, ACT_CHECK_TTS_DATA);

        webView.loadDataWithBaseURL(null, "<style>img{display: inline;height: auto;max-width: 100%;}p{text-align: justify;}</style>" +news,"text/html","utf-8",null);
        Picasso.with(this).load(imgs).placeholder(R.drawable.opcity_logo).into(img);
        settings.setTextZoom(settings.getTextZoom()+20);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
      getMenuInflater().inflate(R.menu.main,menu);
        voice_menu = menu.findItem(R.id.menu_voice);
        pause_menu = menu.findItem(R.id.menu_pause);
        if (language!=null && language.equalsIgnoreCase("Nepali")) {
            voice_menu.setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }




    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_sharebutton) {
            Intent intent=new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            intent.putExtra(Intent.EXTRA_TITLE,"Khabarhub");
            intent.putExtra(Intent.EXTRA_TEXT,news_url);
            startActivity(Intent.createChooser(intent,"Share Using"));

            return true;
        }
        if (id==R.id.menu_fontdecreament){
            settings.setTextZoom(settings.getTextZoom() - 10);
        }
        if (id==R.id.menu_fontincreament){
            settings.setTextZoom(settings.getTextZoom() + 10);
        }
        if (id==R.id.menu_savebutton){

            if (language!=null && language.equalsIgnoreCase("English")){
                JustInDb justInDb = JustInDb.Instance(this);
                new SaveEnglishNewsAsyncTask(justInDb).execute();
            }
            else {
                JustInDb justInDb = JustInDb.Instance(this);
                new SaveNewsAsyncTask(justInDb).execute();
            }
        }
        if (id == R.id.menu_voice){
            speakWords(mtitle+"."+ Html.fromHtml(news));
            item.setVisible(false);
            pause_menu.setVisible(true);
        }
        if (id == R.id.menu_pause){
            pause_menu.setVisible(false);
            stopAudio();
            voice_menu.setVisible(true);
        }
        if (id==R.id.menu_home){

            if (language!=null && language.equalsIgnoreCase("English")){
                startActivity(new Intent(this, EnglishMainActivity.class));
            }
            else {
                startActivity(new Intent(this, MainActivity.class));
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private class SaveNewsAsyncTask extends AsyncTask<Void,Void,Void> {
        JustInDb justInDbs;
        NepaliNewsSaveDao nepaliNewsSaveDao;
        public SaveNewsAsyncTask(JustInDb justInDb) {
            justInDbs=justInDb;
            nepaliNewsSaveDao=justInDb.nepaliNewsSaveDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            justInDbs.nepaliNewsSaveDao().insertall(new NepaliNewsSaveDto(news_id,mtitle,mdate,imgs,news_url,news,mAuthor,""));
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(NotificationNewsActivity.this,"Saved",Toast.LENGTH_SHORT).show();
            super.onPostExecute(aVoid);
        }
    }

    private class SaveEnglishNewsAsyncTask extends AsyncTask<Void,Void,Void>{
        JustInDb justInDbs;
        Savenews_English_Dao savenews_english_dao;
        public SaveEnglishNewsAsyncTask(JustInDb justInDb) {
            justInDbs=justInDb;
            savenews_english_dao=justInDb.savenews_english_dao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            justInDbs.savenews_english_dao().insertall(new Savenews_English_Dto(news_id,mtitle,mdate,imgs,news_url,news,mAuthor,""));

            return null;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(NotificationNewsActivity.this,"Saved",Toast.LENGTH_SHORT).show();

            super.onPostExecute(aVoid);
        }
    }

    @Override
    public void onInit(int i) {
        if (i == TextToSpeech.SUCCESS) {
            if (mTTS != null) {
                int result = mTTS.setLanguage(Locale.US);
                if (result == TextToSpeech.LANG_MISSING_DATA || result ==
                        TextToSpeech.LANG_NOT_SUPPORTED) {
                    Toast.makeText(this, "TTS language is not supported",
                            Toast.LENGTH_LONG).show();
                } else {
                    // Do something here
                }
            }
        } else {
            Toast.makeText(this, "TTS initialization failed",
                    Toast.LENGTH_LONG).show();
        }
    }

    public  void speakWords(String speech) {

        Intent intent = new Intent(this, MyService.class);
        intent.setAction("com.android.music.playstatechanged");
        stopService(intent);
        //speak straight away
        mTTS.speak(speech, TextToSpeech.QUEUE_FLUSH, null);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        if (requestCode == ACT_CHECK_TTS_DATA) {
            if (resultCode ==
                    TextToSpeech.Engine.CHECK_VOICE_DATA_PASS) {
                // Data exists, so we instantiate the TTS engine
                mTTS = new TextToSpeech(this, this);
            } else {

                Intent installIntent = new Intent();
                installIntent.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(installIntent);
            }
        }
    }
    void stopAudio(){
        if (mTTS !=null){
            mTTS.stop();

        }
    }
    @Override
    protected void onDestroy() {
        if (mTTS !=null){
            mTTS.stop();
            mTTS.shutdown();
        }
        super.onDestroy();
    }
}
