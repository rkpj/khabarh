package khabarhub.com;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.zip.Inflater;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class Introview_PageAdapter extends PagerAdapter {
    Context context;
    List<ScreenItem> screenItems;

    public Introview_PageAdapter(Context context, List<ScreenItem> screenItems) {
        this.context = context;
        this.screenItems = screenItems;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        LayoutInflater layoutInflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View inflateview=layoutInflater.inflate(R.layout.introscreen1,null);
        ImageView introimg=inflateview.findViewById(R.id.intro_img);
        TextView intro_title=inflateview.findViewById(R.id.intro_title);
        TextView intro_desc=inflateview.findViewById(R.id.intro_desc);

        intro_title.setText(screenItems.get(position).getTitle());
        intro_desc.setText(screenItems.get(position).getDescribtion());
        introimg.setImageResource(screenItems.get(position).getScreenimg());
        if (position==1){
            introimg.getLayoutParams().height= ViewGroup.LayoutParams.MATCH_PARENT;
            introimg.getLayoutParams().width= ViewGroup.LayoutParams.MATCH_PARENT;

        }
        container.addView(inflateview);
        return  inflateview;
    }

    @Override
    public int getCount() {
        return screenItems.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view==object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {


        container.removeView((View)object);
    }
}
