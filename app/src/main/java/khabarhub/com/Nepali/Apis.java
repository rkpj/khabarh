package khabarhub.com.Nepali;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface Apis {
    String BASE_URL = "https://www.khabarhub.com/wp-json/";
    String weather_base_url="http://api.openweathermap.org/data/2.5/weather?";
    String apikey_weather="53a289dd27235070504b9f5cbbc76a7b";
    String rasifal_daily="https://www.khabarhub.com/category/%E0%A4%B0%E0%A4%BE%E0%A4%B6%E0%A4%BF%E0%A4%AB%E0%A4%B2/feed/?fbclid=IwAR2cDmCYGF1CAqolyWoy0ytqJA-HKMpNtBxajjO0HT-MEqSgXp4mdk9-e98";
    String rasifal_weekly="https://www.khabarhub.com/category/%E0%A4%B8%E0%A4%BE%E0%A4%AA%E0%A5%8D%E0%A4%A4%E0%A4%BE%E0%A4%B9%E0%A4%BF%E0%A4%95-%E0%A4%B0%E0%A4%BE%E0%A4%B6%E0%A4%BF%E0%A4%AB%E0%A4%B2/feed/?fbclid=IwAR1o-J1mDlMZCO2wNOxx-TJlMkjgUT1Iytev3RWPXoQOj3KIQrrWqqAPHJE";
    String breakingnewsnepali="https://www.khabarhub.com/category/%E0%A4%AA%E0%A5%8D%E0%A4%B0%E0%A4%AE%E0%A5%81%E0%A4%96-%E0%A4%B8%E0%A4%AE%E0%A4%BE%E0%A4%9A%E0%A4%BE%E0%A4%B0/feed/?fbclid=IwAR2Bu3cEN3ExEKCde-pq0cdbhSLScBJUFdieelIArrpPILLgEoVFhnNuCrk";
    String rasifal_yearly="https://www.khabarhub.com/category/%E0%A4%B0%E0%A4%BE%E0%A4%B6%E0%A4%BF%E0%A4%AB%E0%A4%B2-2/%E0%A4%AE%E0%A4%BE%E0%A4%B8%E0%A4%BF%E0%A4%95-%E0%A4%B0%E0%A4%BE%E0%A4%B6%E0%A4%BF%E0%A4%AB%E0%A4%B2/feed/?fbclid=IwAR2R-M6rwZ3CNpgddg3q8xrb5lltHL5ScvpH2XTPA2D4AbmbP4tmE49b3zo";
    String breakingenglish="https://www.khabarhub.com/category/breaking-news/feed/?fbclid=IwAR0dCdqVQlvtnQjKt1jVmPW3_KKodHBI57ap03z04azv7DSxKRO-xIJMTS0";
    @GET("appharurest/v2/posts/0/1/")
    Call<List<RecycleViewPojomodel>> getJustinImage();
}
