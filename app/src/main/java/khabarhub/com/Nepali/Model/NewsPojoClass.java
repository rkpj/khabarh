package khabarhub.com.Nepali.Model;

import java.io.Serializable;

public class NewsPojoClass implements Serializable {
    String title,date,image,news_body,desc,news_link,author_name,author_img;
    int news_id;


    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getNews_link() {
        return news_link;
    }

    public void setNews_link(String news_link) {
        this.news_link = news_link;
    }

    public NewsPojoClass() {
    }

    public NewsPojoClass(String title, String date, String image, int news_id,String desc,String news_link,String author_name,String author_img) {
        this.title = title;
        this.date = date;
        this.image = image;
        this.news_id = news_id;
        this.desc=desc;
        this.news_link=news_link;
        this.author_name = author_name;
        this.author_img = author_img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getNews_id() {
        return news_id;
    }

    public void setNews_id(int news_id) {
        this.news_id = news_id;
    }

    public String getNews_body() {
        return news_body;
    }

    public void setNews_body(String news_body) {
        this.news_body = news_body;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public String getAuthor_img() {
        return author_img;
    }

    public void setAuthor_img(String author_img) {
        this.author_img = author_img;
    }
}
