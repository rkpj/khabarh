package khabarhub.com.Nepali.Model;

public class ShareMarketIndexModel {
    String IndexName;
    String IndexValue;
    String PercentageChange;
    String AbsoluteChange;

    public String getIndexName() {
        return IndexName;
    }

    public void setIndexName(String indexName) {
        IndexName = indexName;
    }

    public String getIndexValue() {
        return IndexValue;
    }

    public void setIndexValue(String indexValue) {
        IndexValue = indexValue;
    }

    public String getPercentageChange() {
        return PercentageChange;
    }

    public void setPercentageChange(String percentageChange) {
        PercentageChange = percentageChange;
    }

    public String getAbsoluteChange() {
        return AbsoluteChange;
    }

    public void setAbsoluteChange(String absoluteChange) {
        AbsoluteChange = absoluteChange;
    }
}
