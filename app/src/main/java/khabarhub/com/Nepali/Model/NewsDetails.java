package khabarhub.com.Nepali.Model;

public class NewsDetails {
    String id,title,date,body;

    public NewsDetails() {
    }

    public NewsDetails(String id, String title, String date, String body) {
        this.id = id;
        this.title = title;
        this.date = date;
        this.body = body;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
