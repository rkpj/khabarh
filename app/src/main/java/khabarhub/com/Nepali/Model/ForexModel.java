package khabarhub.com.Nepali.Model;

public class ForexModel {
    String BaseCurrency;
    String TargetCurrency;
    String BaseValue;
    String TargetBuy;
    String TargetSell;

    public ForexModel(String baseCurrency, String targetCurrency, String baseValue, String targetBuy, String targetSell) {
        BaseCurrency = baseCurrency;
        TargetCurrency = targetCurrency;
        BaseValue = baseValue;
        TargetBuy = targetBuy;
        TargetSell = targetSell;
    }

    public String getBaseCurrency() {
        return BaseCurrency;
    }

    public void setBaseCurrency(String baseCurrency) {
        BaseCurrency = baseCurrency;
    }

    public String getTargetCurrency() {
        return TargetCurrency;
    }

    public void setTargetCurrency(String targetCurrency) {
        TargetCurrency = targetCurrency;
    }

    public String getBaseValue() {
        return BaseValue;
    }

    public void setBaseValue(String baseValue) {
        BaseValue = baseValue;
    }

    public String getTargetBuy() {
        return TargetBuy;
    }

    public void setTargetBuy(String targetBuy) {
        TargetBuy = targetBuy;
    }

    public String getTargetSell() {
        return TargetSell;
    }

    public void setTargetSell(String targetSell) {
        TargetSell = targetSell;
    }
}
