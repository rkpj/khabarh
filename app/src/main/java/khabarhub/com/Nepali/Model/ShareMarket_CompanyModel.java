package khabarhub.com.Nepali.Model;

public class ShareMarket_CompanyModel {
    int RowTotal;
    String StockSymbol;
    String StockName;
    int MaxPrice;
    int MinPrice;
    int ClosingPrice;
    int Difference;
    String TradedShares;
    String TradedAmount;
    int PreviousClosing;
    int PercentDifference;
    String AsOfDateString;

    public int getRowTotal() {
        return RowTotal;
    }

    public void setRowTotal(int rowTotal) {
        RowTotal = rowTotal;
    }

    public String getStockSymbol() {
        return StockSymbol;
    }

    public void setStockSymbol(String stockSymbol) {
        StockSymbol = stockSymbol;
    }

    public String getStockName() {
        return StockName;
    }

    public void setStockName(String stockName) {
        StockName = stockName;
    }

    public int getMaxPrice() {
        return MaxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        MaxPrice = maxPrice;
    }

    public int getMinPrice() {
        return MinPrice;
    }

    public void setMinPrice(int minPrice) {
        MinPrice = minPrice;
    }

    public int getClosingPrice() {
        return ClosingPrice;
    }

    public void setClosingPrice(int closingPrice) {
        ClosingPrice = closingPrice;
    }

    public int getDifference() {
        return Difference;
    }

    public void setDifference(int difference) {
        Difference = difference;
    }

    public String getTradedShares() {
        return TradedShares;
    }

    public void setTradedShares(String tradedShares) {
        TradedShares = tradedShares;
    }

    public String getTradedAmount() {
        return TradedAmount;
    }

    public void setTradedAmount(String tradedAmount) {
        TradedAmount = tradedAmount;
    }

    public int getPreviousClosing() {
        return PreviousClosing;
    }

    public void setPreviousClosing(int previousClosing) {
        PreviousClosing = previousClosing;
    }

    public int getPercentDifference() {
        return PercentDifference;
    }

    public void setPercentDifference(int percentDifference) {
        PercentDifference = percentDifference;
    }

    public String getAsOfDateString() {
        return AsOfDateString;
    }

    public void setAsOfDateString(String asOfDateString) {
        AsOfDateString = asOfDateString;
    }
}
