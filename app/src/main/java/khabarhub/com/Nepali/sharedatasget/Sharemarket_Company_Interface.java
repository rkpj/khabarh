package khabarhub.com.Nepali.sharedatasget;

import java.util.List;

import khabarhub.com.Nepali.Model.ShareMarketIndexModel;
import khabarhub.com.Nepali.Model.ShareMarket_CompanyModel;

public interface Sharemarket_Company_Interface {
     void setCompanyShare(List<ShareMarket_CompanyModel> companyShare);

}
