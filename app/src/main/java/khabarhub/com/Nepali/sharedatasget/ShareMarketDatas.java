package khabarhub.com.Nepali.sharedatasget;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import khabarhub.com.Nepali.Model.ShareMarketIndexModel;
import khabarhub.com.Nepali.Model.ShareMarket_CompanyModel;

public class ShareMarketDatas {
    List<ShareMarket_CompanyModel> shareMarket_companyModels;
    List<ShareMarketIndexModel> shareMarketIndexModels;
    Sharemarket_Company_Interface company_interface;
   // ShareMarket_Index_subIndex_Interface shareMarket_index_subIndex_interface;
    Context context;

    public ShareMarketDatas(Sharemarket_Company_Interface company_interface, Context context) {
        this.company_interface = company_interface;
     //   this.shareMarket_index_subIndex_interface= shareMarket_index_subIndex_interface;
        this.context = context;
    }

    public void getSharemarketCompany(String url, int offset, String limit){
       shareMarket_companyModels = new ArrayList<>();
       JSONObject jsonObject = new JSONObject();
       try {
           jsonObject.put("fromdate","");
           jsonObject.put("toDate","");
           jsonObject.put("stockSymbol","");
           jsonObject.put("offset",offset);
           jsonObject.put("limit",limit);


       } catch (JSONException e) {
           e.printStackTrace();
       }
       AndroidNetworking.post(url)
              .addJSONObjectBody(jsonObject)
               .setPriority(Priority.LOW)
               .build()
               .getAsJSONObject(new JSONObjectRequestListener() {
                   @Override
                   public void onResponse(JSONObject response) {

                       try {
                           JSONArray jsonArray = response.getJSONArray("d");
                           for (int i=0; i<jsonArray.length();i++){
                               ShareMarket_CompanyModel shareMarket_companyModel= new ShareMarket_CompanyModel();

                               JSONObject jsonObject = jsonArray.getJSONObject(i);
                               shareMarket_companyModel.setAsOfDateString(jsonObject.getString("AsOfDateString"));
                               shareMarket_companyModel.setClosingPrice(jsonObject.getInt("ClosingPrice"));
                               shareMarket_companyModel.setDifference(jsonObject.getInt("Difference"));
                               shareMarket_companyModel.setMaxPrice(jsonObject.getInt("MaxPrice"));
                               shareMarket_companyModel.setMinPrice(jsonObject.getInt("MinPrice"));
                               shareMarket_companyModel.setPercentDifference(jsonObject.getInt("PercentDifference"));
                               shareMarket_companyModel.setRowTotal(jsonObject.getInt("RowTotal"));
                               shareMarket_companyModel.setPreviousClosing(jsonObject.getInt("PreviousClosing"));
                               shareMarket_companyModel.setStockName(jsonObject.getString("StockName"));
                               shareMarket_companyModel.setStockSymbol(jsonObject.getString("StockSymbol"));
                               shareMarket_companyModel.setTradedAmount(jsonObject.getString("TradedAmount"));
                               shareMarket_companyModel.setTradedShares(jsonObject.getString("TradedShares"));
                               shareMarket_companyModels.add(shareMarket_companyModel);
                           }

                           company_interface.setCompanyShare(shareMarket_companyModels);

                       } catch (JSONException e) {
                           e.printStackTrace();
                        }
                   }

                   @Override
                   public void onError(ANError anError) {
                   }

               });


    }

   public void getIndexs(String url,String datefrom,String dateto,String indexs){
        shareMarketIndexModels = new ArrayList<>();

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("IndexName",indexs);
                jsonObject.put("IndexDateFrom",datefrom);
                jsonObject.put("IndexDateTo",dateto);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            AndroidNetworking.post(url)
                    .addJSONObjectBody(jsonObject)
                    .setPriority(Priority.LOW)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                JSONArray jsonArray= response.getJSONArray("d");

                                for (int i=0;i<jsonArray.length(); i++){
                                    ShareMarketIndexModel shareMarketIndexModel = new ShareMarketIndexModel();


                                    if (i==jsonArray.length()-1)  {
                                     JSONObject jsonObject1= jsonArray.getJSONObject(jsonArray.length()-1);
                                     shareMarketIndexModel.setIndexName(jsonObject1.getString("IndexName"));
                                     shareMarketIndexModel.setIndexValue(jsonObject1.getString("IndexValue"));
                                     shareMarketIndexModel.setAbsoluteChange(jsonObject1.getString("AbsoluteChange"));
                                     shareMarketIndexModel.setPercentageChange(jsonObject1.getString("PercentageChange"));

                                     shareMarketIndexModels.add(shareMarketIndexModel);

                                 }
                                }
       //                         shareMarket_index_subIndex_interface.setIndexs(shareMarketIndexModels);
                            } catch (JSONException e) {
                                e.printStackTrace();

                            }

                        }

                        @Override
                        public void onError(ANError anError) {


                        }
                    });




    }
}
