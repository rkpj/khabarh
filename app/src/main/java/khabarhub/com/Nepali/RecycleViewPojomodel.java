package khabarhub.com.Nepali;

public class RecycleViewPojomodel {
    String title,date,body;
    String img;

    public RecycleViewPojomodel() {
    }

    public RecycleViewPojomodel(String img, String title, String date) {
        this.img = img;
        this.title = title;
        this.date = date;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
