package khabarhub.com.Nepali.ViewModelPackage;

import android.app.Application;
import android.util.Log;

import java.util.Collections;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import khabarhub.com.Nepali.RepositoryPackage.Apis.Repo_Justnews;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.BreakingNewsDto;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.JustInNewsDto;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.NepaliNewsSaveDto;

public class JustInnewsViewModel extends AndroidViewModel {

    Repo_Justnews repo_justnews;
    SetBreakingnews setBreakingnews;
    interface SetBreakingnews{
        void breaknews(List<BreakingNewsDto> breakingNewsDtos);
    }

    public JustInnewsViewModel(@NonNull Application application) {
        super(application);
        repo_justnews=Repo_Justnews.instance(application);
    }


    //we will call this method to get the data
    public LiveData<List<JustInNewsDto>> getJustInnews() {
        return repo_justnews.getjustnews();
    }

    public LiveData<List<BreakingNewsDto>> getBreakingNepalinews() {
        return repo_justnews.getbreakingnewsnepali();
    }

    public LiveData<List<NepaliNewsSaveDto>> getNepalinewsSaved() {
        return repo_justnews.getnepalisavenews();
    }


}
