package khabarhub.com.Nepali;

import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
        import androidx.recyclerview.widget.RecyclerView;


import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import khabarhub.com.Nepali.Model.NewsPojoClass;
        import khabarhub.com.R;
import khabarhub.com.calendarnepali.DateConverter;

public class Multiplelayout_list_adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<NewsPojoClass> newslist;
    Context context;
    OnItemClickListner monClickListener;
    private static final int TYPE_ONE = 1;
    private static final int TYPE_TWO = 2;


    public interface OnItemClickListner {
        void onclick(int news_id, String news_img);
        void click(String title,String date,String news,List<NewsPojoClass> newslist,int positionclick,String news_link);
    }

    @Override
    public int getItemViewType(int position) {
        if (position==0)
        {
            return TYPE_ONE;
        }
        else {
            return TYPE_TWO;
        }
     //   return super.getItemViewType(position);
    }

    public Multiplelayout_list_adapter(List<NewsPojoClass> newslist, Context context, OnItemClickListner onItemClickListner) {
        this.newslist = newslist;
        this.context = context;
        monClickListener = onItemClickListner;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==TYPE_ONE){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_single_news, parent, false);

            return new MyViewHolder2(view);
        }
        else {

            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_design_for_news, parent, false);
            return new MyViewHolder(view);
        }



    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
switch (holder.getItemViewType()){
    case TYPE_ONE:
        setcardlist((MyViewHolder2) holder,position);
        break;
    case TYPE_TWO:
        setlistview((MyViewHolder) holder,position);
        break;

}
}
public void setlistview(MyViewHolder holder,int position){
        Log.d("mediaplayer","data "+ newslist.get(position).getTitle());
    NewsPojoClass JustinNews = newslist.get(position);
    Picasso.with(context).load(JustinNews.getImage()).placeholder(R.drawable.opcity_logo).into(holder.img);
    holder.title.setText(Html.fromHtml(JustinNews.getTitle()));
    holder.date.setText((JustinNews.getDate()));
    holder.author_name.setText(JustinNews.getAuthor_name());
}
public void setcardlist(MyViewHolder2 holder,int position){
    NewsPojoClass JustinNews = newslist.get(position);
    Picasso.with(context).load(JustinNews.getImage()).placeholder(R.drawable.opcity_logo).into(holder.imgs);
    holder.titles.setText(Html.fromHtml(JustinNews.getTitle()));
    holder.dates.setText((JustinNews.getDate()));
    holder.author_name.setText(JustinNews.getAuthor_name());

//    holder.dates.setText(JustinNews.getDate());
}





    @Override
    public int getItemCount() {
        return newslist.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView img;
        TextView title, date,author_name;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.card_news_img);
            title = itemView.findViewById(R.id.card_news_title);
            date = itemView.findViewById(R.id.card_news_date);
            author_name = itemView.findViewById(R.id.card_news_author_name);
            itemView.setTag(this);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    monClickListener.click(newslist.get(getAdapterPosition()).getTitle(),newslist.get(getAdapterPosition()).getDate(),"adfsdf",newslist,getAdapterPosition(),newslist.get(getAdapterPosition()).getNews_link());
                 //   monClickListener.onclick(newslist.get(getAdapterPosition()).getNews_id(), newslist.get(getAdapterPosition()).getImage());

                }
            });
        }
    }

    public class MyViewHolder2 extends RecyclerView.ViewHolder {
        ImageView imgs;
        TextView titles, dates,author_name;

        public MyViewHolder2(@NonNull View itemView) {
            super(itemView);
            imgs = itemView.findViewById(R.id.card_news_img_singleview);
            titles = itemView.findViewById(R.id.card_news_title_singleview);
            dates = itemView.findViewById(R.id.card_news_date_singleview);
            author_name = itemView.findViewById(R.id.card_news_author_name_singleview);
            itemView.setTag(this);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    monClickListener.click(newslist.get(getAdapterPosition()).getTitle(),newslist.get(getAdapterPosition()).getDate(),"adfsdf",newslist,getAdapterPosition(),newslist.get(getAdapterPosition()).getNews_link());

                    //  monClickListener.onclick(newslist.get(getAdapterPosition()).getNews_id(), newslist.get(getAdapterPosition()).getImage());

                }
            });
        }
    }



}

