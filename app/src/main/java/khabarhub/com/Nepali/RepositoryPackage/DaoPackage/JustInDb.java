package khabarhub.com.Nepali.RepositoryPackage.DaoPackage;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import khabarhub.com.English.Model.DaoForJustNewsEnglish;
import khabarhub.com.English.Model.Dto_EnglishBreakingNews;
import khabarhub.com.English.Model.EnglishBreakingNewsDao;
import khabarhub.com.English.Model.JustInNews_english;
import khabarhub.com.English.Model.Savenews_English_Dao;
import khabarhub.com.English.Model.Savenews_English_Dto;
import khabarhub.com.Nepali.Apis;
import khabarhub.com.calendarnepali.DateConverter;

@Database(entities = {JustInNewsDto.class, JustInNews_english.class,BreakingNewsDto.class,Dto_EnglishBreakingNews.class,NepaliNewsSaveDto.class, Savenews_English_Dto.class}, version = 1,exportSchema = false)
public abstract class JustInDb extends RoomDatabase {
    public abstract JustInDao justInDao();
    public abstract Breakingnews_nepali_Dao breakingnews_nepali_dao();
    public abstract DaoForJustNewsEnglish daoForJustNewsEnglish();
    public abstract EnglishBreakingNewsDao englishBreakingNewsDao();
    public abstract NepaliNewsSaveDao nepaliNewsSaveDao();
    public abstract Savenews_English_Dao savenews_english_dao();
  static DateConverter dateConverter = new DateConverter();


    private static JustInDb INSTANCE = null;

    public static JustInDb Instance(Context context) {
        if (INSTANCE == null) {
            synchronized (JustInDb.class) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(), JustInDb.class, "Khabarhubnews")
                        .allowMainThreadQueries()
                        .fallbackToDestructiveMigration()
                        .addCallback(roomdbcallback)
                        .build();
            }
        }

        return INSTANCE;
    }

    public static RoomDatabase.Callback roomdbcallback = new RoomDatabase.Callback() {
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            new InsertJustNewsAsync(INSTANCE).execute();
            new InsertBreakingNewsNepaliAsync(INSTANCE).execute();
            new InsertBreakingNewsEnglisAsync(INSTANCE).execute();
            new InsertJustNewsenglishAsync(INSTANCE).execute();



        }
    };


    private static class InsertJustNewsAsync extends AsyncTask<Void, Void,Void> {
        JustInDb justInDb;
        JustInDao justInDao;

        public InsertJustNewsAsync(JustInDb justnewdao) {
            justInDb=justnewdao;
            justInDao=justnewdao.justInDao();

        }

        @Override
        protected Void doInBackground(Void... voids) {

            AndroidNetworking.get(Apis.BASE_URL+"appharurest/v2/posts/0/1/")
                    .setPriority(Priority.LOW)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                                justInDao.deleteoldvalue();
                            for (int i=0;i<response.length();i++){
                                try {
                                    JSONObject jsonObject=response.getJSONObject(i);
                                    int news_id=jsonObject.getInt("id");
                                    String news_title=jsonObject.optString("title");
                                    String news_date=dateChangeToAgoNews(jsonObject.optString("date_english"));
                                    String news_body=jsonObject.optString("body");
                                    String news_img=jsonObject.optString("image_link_medium");
                                    String news_link=jsonObject.optString("permalink");

                                    String author_name = jsonObject.optString("author_name");
                                    String author_img = jsonObject.optString("author_image");
                                    justInDb.justInDao().insertall(new JustInNewsDto(news_id,news_title,news_date,news_img,news_link,news_body,author_name,author_img));
                                    Log.d("Khabarhub_log","getcategorieslist: added");

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.d("Khabarhub_log","getcategorieslist catch: "+e.getMessage());
                                }

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d("Khabarhub_log","getcategorieslist catcherror: "+anError.getMessage());
                        }
                    });
            //   justInDaos.insertall(justInNewsDtos[0]);
            return null;
        }
    }
    private static class InsertBreakingNewsNepaliAsync extends AsyncTask<Void, Void,Void> {
        JustInDb justInDb;
        Breakingnews_nepali_Dao breakingnews_nepali_dao;
        public InsertBreakingNewsNepaliAsync(JustInDb breaking) {
            justInDb=breaking;
            breakingnews_nepali_dao=breaking.breakingnews_nepali_dao();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            AndroidNetworking.get(Apis.BASE_URL+"appharurest/v2/posts/19/1?fbclid=IwAR1_P1HgYEd7-nM4Npr2cjNHI5RiGWBq0MCQ8yGQ3NSM6dH_yPVAh4HchKQ")
                    .setPriority(Priority.LOW)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            breakingnews_nepali_dao.deleteoldvalue();
                            for (int i=0;i<response.length();i++){
                                try {
                                    JSONObject jsonObject=response.getJSONObject(i);
                                    int news_id=jsonObject.getInt("id");
                                    String news_title=jsonObject.optString("title");
                                    String news_date=dateChangeToAgoNews(jsonObject.optString("date_english"));
                                    String news_body=jsonObject.optString("body");
                                    String news_img=jsonObject.optString("image_link_medium");
                                    String news_link=jsonObject.optString("permalink");
                                    String news_desc=jsonObject.optString("desc");
                                    String author_name = jsonObject.optString("author_name");
                                    String author_img = jsonObject.optString("author_image");
                                    justInDb.breakingnews_nepali_dao().insertall(new BreakingNewsDto(news_id,news_title,news_date,news_img,news_link,news_body,news_desc,author_name,author_img));
                                    Log.d("Khabarhub_log","getcategorieslist: added");

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.d("Khabarhub_log","getcategorieslist catch: "+e.getMessage());
                                }

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d("Khabarhub_log","getcategorieslist catcherror: "+anError.getMessage());
                        }
                    });
            //   justInDaos.insertall(justInNewsDtos[0]);
            return null;
        }
    }

    private static class InsertJustNewsenglishAsync extends AsyncTask<Void, Void,Void> {
        JustInDb justInDb;
        DaoForJustNewsEnglish daoForJustNewsEnglish;
        public InsertJustNewsenglishAsync(JustInDb justInDbs) {
            justInDb=justInDbs;
            daoForJustNewsEnglish=justInDbs.daoForJustNewsEnglish();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            AndroidNetworking.get("https://english.khabarhub.com/wp-json/appharurest/v2/posts/0/1/")
                    .setPriority(Priority.LOW)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            daoForJustNewsEnglish.deleteoldvalue();
                            for (int i=0;i<response.length();i++){
                                try {
                                    JSONObject jsonObject=response.getJSONObject(i);
                                    int news_id=jsonObject.getInt("id");
                                    String news_date=jsonObject.optString("date");
                                    String news_title=jsonObject.optString("title");
                                    String news_body=jsonObject.optString("body");
                                    String news_img=jsonObject.optString("image_link_medium");
                                    String news_link=jsonObject.optString("permalink");
                                    String author_name = jsonObject.optString("author_name");
                                    String author_img = jsonObject.optString("author_image");
                                    justInDb.daoForJustNewsEnglish().insertall(new JustInNews_english(news_id,news_title,news_date,news_img,news_link,news_body,author_name,author_img));
                                    Log.d("Khabarhub_log","getcategorieslist: added");

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.d("Khabarhub_log","getcategorieslist catch: "+e.getMessage());
                                }

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d("Khabarhub_log","getcategorieslist catcherror: "+anError.getMessage());
                        }
                    });
            //   justInDaos.insertall(justInNewsDtos[0]);
            return null;
        }
    }


    private static class InsertBreakingNewsEnglisAsync extends AsyncTask<Void, Void,Void> {
        JustInDb justInDb;
        EnglishBreakingNewsDao englishBreakingNewsDao;
        public InsertBreakingNewsEnglisAsync(JustInDb justInDbs) {
            justInDb=justInDbs;
            englishBreakingNewsDao=justInDbs.englishBreakingNewsDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {

            AndroidNetworking.get("https://english.khabarhub.com/wp-json/appharurest/v2/posts/19/1?fbclid=IwAR17cbna64hIJ1aP8YwMpD44kyCgSB-FnPHw_X7C1DnFfJQbbjbPPBpjqH8")
                    .setPriority(Priority.LOW)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            englishBreakingNewsDao.deleteoldvalue();
                            for (int i=0;i<response.length();i++){
                                try {
                                    JSONObject jsonObject=response.getJSONObject(i);
                                    int news_id=jsonObject.getInt("id");
                                    String news_date=jsonObject.optString("date");
                                    String news_title=jsonObject.optString("title");
                                    String news_img=jsonObject.optString("image_link_medium");
                                    String news_body=jsonObject.optString("body");
                                    String news_link=jsonObject.optString("permalink");
                                    String news_desc=jsonObject.optString("desc");
                                    String author_name = jsonObject.optString("author_name");
                                    String author_img = jsonObject.optString("author_image");
                                    justInDb.englishBreakingNewsDao().insertall(new Dto_EnglishBreakingNews(news_id,news_title,news_date,news_img,news_link,news_body,news_desc,author_name,author_img));
                                    Log.d("Khabarhub_log","getcategorieslist: added");

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.d("Khabarhub_log","getcategorieslist catch: "+e.getMessage());
                                }

                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.d("Khabarhub_log","getcategorieslist catcherror: "+anError.getMessage());
                        }
                    });
            //   justInDaos.insertall(justInNewsDtos[0]);
            return null;
        }
    }

    private static String dateChangeToAgoNews(String date){
        String[] date_time = date.split(" ");
//        String newsDate = date_time[0];
//        String newsTime = date_time[1];
        SimpleDateFormat formats = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        SimpleDateFormat timeformat = new SimpleDateFormat("HH:mm:ss");

        LocalDate current_date = LocalDate.now();
        LocalTime currentdatetime = LocalTime.now();
        String currentdate = current_date+" "+currentdatetime;
        Log.d("mediaplayer","newdatetime:"+date+" currentdate: "+currentdate);
        String res = "";
        Date d1= null;
        Date d2 = null;

        try{
            d1 = formats.parse(date);
            d2 = formats.parse(currentdate);
//            Date date1 = timeformat.parse(newsTime);
//
//            Date date2 = timeformat.parse(""+currentdatetime);
//
//            long difference = date2.getTime() - date1.getTime();
//
//            int minutes = (int) TimeUnit.MILLISECONDS.toMinutes(difference);
            long diff =Math.abs( d2.getTime() - d1.getTime());
            long diffSeconds = (diff / 1000) % 60;
            int diffMinutes = (int) ((diff / (60 * 1000)) % 60);
            int diffHours = (int) ((diff / (60 * 60 * 1000))%24);
            int diffInDays = (int) ((diff / (1000 * 60 * 60 * 24)));
            int day = 365-diffInDays;
            Log.d("mediaplayer"," min: "+diffMinutes+" hh: "+diffHours+" day: "+diffInDays);
//            Log.d("mediaplayer"," min: "+minutes);
//
//
//            Log.d("mediaplayer"," modulus min :: "+(minutes%60));

            if (diffMinutes < 60 && diffHours == 0 && diffInDays == 0) {
                res = getStringToNelaliFromNumber(diffMinutes) + " मिनेट अगाडि";
            } else if (diffHours < 24 && diffInDays == 0) {
                res = getStringToNelaliFromNumber(diffHours) + " घण्टा अगाडि";
            } else {
                //   Log.d("mediaplayer"," minsss: "+minutes);
                String[] arrayOfDates = date.split("-");
                int engyear = Integer.parseInt(arrayOfDates[0]);
                int engMonth = Integer.parseInt(arrayOfDates[1]);
                int engday = Integer.parseInt(arrayOfDates[2]);
                int npyear =   dateConverter.getNepaliDate(engyear,engMonth,engday).getYear();
                int npmonth =   dateConverter.getNepaliDate(engyear,engMonth,engday).getMonth();
                int npday =   dateConverter.getNepaliDate(engyear,engMonth,engday).getDay();
                res= getStringToNelaliFromNumber(npyear)+"-"+getStringToNelaliFromNumber(npmonth)+"-"+getStringToNelaliFromNumber(npday);


            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return res;
    }


    public static String getStringToNelaliFromNumber(int number) {
        String[] nepaliNumArray = new String[] { "०", "१", "२", "३", "४", "५", "६", "७", "८", "९" };
        StringBuilder result = new StringBuilder();
        while (number > 0) {
            int n = number % 10;
            result.insert(0, nepaliNumArray[n]);
            number /= 10;
        }
        return result.toString();
    }

}