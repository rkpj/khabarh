package khabarhub.com.Nepali.RepositoryPackage.DaoPackage;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface Breakingnews_nepali_Dao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertall(BreakingNewsDto breakingNewsDto);




    @Query("SELECT * FROM breakingnewsnepali")
    LiveData<List<BreakingNewsDto>> getallbreakingnews();

    @Query("SELECT * FROM breakingnewsnepali where id IN(:ids)")
    List<BreakingNewsDto> getbodyofnews(int ids);

    @Query("DELETE FROM breakingnewsnepali")
    public void deleteoldvalue();

}
