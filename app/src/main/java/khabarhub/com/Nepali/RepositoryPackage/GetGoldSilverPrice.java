package khabarhub.com.Nepali.RepositoryPackage;

import android.content.Context;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import khabarhub.com.Nepali.Views.DailyutilPage.Gold_Silver_Interface;
import khabarhub.com.Utils;

public class GetGoldSilverPrice {
    Gold_Silver_Interface gold_silver_interface;
    Context context;

    public GetGoldSilverPrice(Gold_Silver_Interface gold_silver_interface, Context context) {
        this.gold_silver_interface = gold_silver_interface;
        this.context = context;
    }

    public void  getGoldSilverPrice(){
      AndroidNetworking.get(Utils.gold_silver)
              .setPriority(Priority.LOW)
              .build()
              .getAsJSONArray(new JSONArrayRequestListener() {
                  @Override
                  public void onResponse(JSONArray response) {
                      Map<String,String> map = new HashMap<>();
                      try {

                          JSONObject jsonObject = response.getJSONObject(response.length()-1);
                          int finegold = jsonObject.getInt("finegold");
                          int tejabigold = jsonObject.getInt("tejabigold");
                          int silver = jsonObject.getInt("silver");
                          String published_date = jsonObject.getString("published_date");

                          map.put("finegold", String.valueOf(finegold));
                          map.put("tejabigold", String.valueOf(tejabigold));
                          map.put("silver", String.valueOf(silver));
                          map.put("published_date", published_date);
                          gold_silver_interface.goldSilverPrice(map);
                      } catch (JSONException e) {
                          e.printStackTrace();

                      }
                  }

                  @Override
                  public void onError(ANError anError) {


                  }
              });

    }
}
