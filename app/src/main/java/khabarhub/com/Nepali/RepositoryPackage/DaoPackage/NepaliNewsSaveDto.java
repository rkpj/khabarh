package khabarhub.com.Nepali.RepositoryPackage.DaoPackage;

import java.io.Serializable;
import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.PrimaryKey;
import androidx.room.Query;


@Entity(tableName = "nepalisavenews",indices = @Index(value = {"news_id"},unique = true))
public class NepaliNewsSaveDto implements Serializable {


    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name="news_id")
    private int new_id;



    @ColumnInfo(name="news_title")
    private String news_title;
    @ColumnInfo(name="news_date")
    private String news_date;
    @ColumnInfo(name="news_img")
    private String news_img;

    @ColumnInfo(name="news_link")
    private String news_link;
    @ColumnInfo(name="news_body")
    private String news_body;
    @ColumnInfo(name="author_name")
    private String author_name;
    @ColumnInfo(name="author_img")
    private String author_img;



    public NepaliNewsSaveDto(int new_id, String news_title, String news_date, String news_img, String news_link, String news_body,String author_name,String author_img) {
        this.new_id = new_id;
        this.news_title = news_title;
        this.news_date = news_date;
        this.news_img = news_img;
        this.news_link = news_link;
        this.news_body = news_body;
        this.author_img = author_img;
        this.author_name = author_name;
    }

    public String getNews_body() {
        return news_body;
    }

    public void setNews_body(String news_body) {
        this.news_body = news_body;
    }

    public String getNews_link() {
        return news_link;
    }

    public void setNews_link(String news_link) {
        this.news_link = news_link;
    }

    public int getNew_id() {
        return new_id;
    }

    public void setNew_id(int new_id) {
        this.new_id = new_id;
    }

    public String getNews_title() {
        return news_title;
    }

    public void setNews_title(String news_title) {
        this.news_title = news_title;
    }

    public String getNews_date() {
        return news_date;
    }

    public void setNews_date(String news_date) {
        this.news_date = news_date;
    }

    public String getNews_img() {
        return news_img;
    }

    public void setNews_img(String news_img) {
        this.news_img = news_img;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAuthor_name() {
        return author_name;
    }

    public void setAuthor_name(String author_name) {
        this.author_name = author_name;
    }

    public String getAuthor_img() {
        return author_img;
    }

    public void setAuthor_img(String author_img) {
        this.author_img = author_img;
    }
}
