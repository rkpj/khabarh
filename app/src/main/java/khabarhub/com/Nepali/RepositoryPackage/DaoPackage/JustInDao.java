package khabarhub.com.Nepali.RepositoryPackage.DaoPackage;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

//@Entity(tableName = "JustInNews")
@Dao
public interface   JustInDao {



    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertall(JustInNewsDto justInNewsDto);




    @Query("SELECT * FROM justnews")
    LiveData<List<JustInNewsDto>> getalljustnews();

        @Query("SELECT * FROM justnews where id IN(:ids)")
    List<JustInNewsDto> getbodyofnews(int ids);

    @Query("DELETE FROM justnews")
    public void deleteoldvalue();

}
