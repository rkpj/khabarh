package khabarhub.com.Nepali.RepositoryPackage.Apis;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import java.util.Collections;
import java.util.List;

import androidx.lifecycle.LiveData;
import khabarhub.com.English.Model.DaoForJustNewsEnglish;
import khabarhub.com.English.Model.JustInNews_english;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.BreakingNewsDto;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.Breakingnews_nepali_Dao;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.JustInDao;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.JustInDb;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.JustInNewsDto;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.NepaliNewsSaveDao;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.NepaliNewsSaveDto;

public class Repo_Justnews {
    private static Repo_Justnews singleton_class=null;
    Application application;
    JustInDb justInDb;
    JustInDao justnewdao;
    Breakingnews_nepali_Dao breakingnews_nepali_dao;
    DaoForJustNewsEnglish daoForJustNewsEnglish;
    LiveData<List<JustInNewsDto>> justnewslist;
    LiveData<List<BreakingNewsDto>> breakingnewsEnglishlist;
    LiveData<List<JustInNews_english>> englishnewslist;

    NepaliNewsSaveDao nepaliNewsSaveDao;
    LiveData<List<NepaliNewsSaveDto>> nepalinewssavelist;
    public static Repo_Justnews instance(Application application){
        if (singleton_class==null){
            singleton_class=new Repo_Justnews(application);
        }
        return singleton_class;
    }

    public Repo_Justnews(Application application) {
        this.application = application;
        justInDb= JustInDb.Instance(application);
        justnewdao=justInDb.justInDao();
        justnewslist=justnewdao.getalljustnews();
        breakingnews_nepali_dao=justInDb.breakingnews_nepali_dao();
        breakingnewsEnglishlist=breakingnews_nepali_dao.getallbreakingnews();
        daoForJustNewsEnglish=justInDb.daoForJustNewsEnglish();
        englishnewslist=daoForJustNewsEnglish.getallenglishnewsjust();
        nepaliNewsSaveDao=justInDb.nepaliNewsSaveDao();
        nepalinewssavelist=nepaliNewsSaveDao.getallnepalisavenews();



    }

    public LiveData<List<JustInNewsDto>> getjustnews(){

        return justnewslist;

    }
    public LiveData<List<NepaliNewsSaveDto>> getnepalisavenews(){
        return nepalinewssavelist;

    }
    public void insertjustnews(JustInNewsDto jnews){
        new InsertJustNewsAsync(justnewdao).execute(jnews);
    }

    public LiveData<List<BreakingNewsDto>> getbreakingnewsnepali(){
        return breakingnewsEnglishlist;

    }
    public void insertnepalibreakingnews(BreakingNewsDto jnews){
        new InsertBreakingNepaliNewsAsync(breakingnews_nepali_dao).execute(jnews);
    }
    private class InsertJustNewsAsync extends AsyncTask<JustInNewsDto,Void,Void>{
        private JustInDao justInDaos;
        public InsertJustNewsAsync(JustInDao justInDao) {
            justInDaos=justInDao;

        }



        @Override
        protected Void doInBackground(JustInNewsDto... justInDao) {
            justInDaos.insertall(justInDao[0]);

            return null;
        }
    }



    private class InsertBreakingNepaliNewsAsync extends AsyncTask<BreakingNewsDto,Void,Void>{
        private Breakingnews_nepali_Dao breakingnews_nepali_dao;
        public InsertBreakingNepaliNewsAsync(Breakingnews_nepali_Dao breakingnews_nepali_dao) {
            breakingnews_nepali_dao=breakingnews_nepali_dao;

        }



        @Override
        protected Void doInBackground(BreakingNewsDto... justInDao) {
            breakingnews_nepali_dao.insertall(justInDao[0]);

            return null;
        }
    }

}
