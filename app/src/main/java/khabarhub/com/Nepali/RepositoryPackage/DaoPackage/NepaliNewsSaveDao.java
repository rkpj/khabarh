package khabarhub.com.Nepali.RepositoryPackage.DaoPackage;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

@Dao
public interface NepaliNewsSaveDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertall(NepaliNewsSaveDto nepaliNewsSaveDto);

    @Query("SELECT * FROM nepalisavenews")
    LiveData<List<NepaliNewsSaveDto>> getallnepalisavenews();

    @Query("SELECT * FROM nepalisavenews where id IN(:ids)")
    List<NepaliNewsSaveDto> getbodyofnews(int ids);

    @Query("DELETE FROM nepalisavenews")
    public void deleteoldvalue();

    @Query("DELETE FROM nepalisavenews where news_id IN(:ids)")
    public void deletenews(int ids);

}
