package khabarhub.com.Nepali.RepositoryPackage;

import android.content.Context;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import khabarhub.com.Nepali.Model.ForexModel;
import khabarhub.com.Nepali.Views.DailyutilPage.Forex_interface;

public class GetForexRepo {
    List<ForexModel> forexModelList;
    Context context;
    Forex_interface forex_interface;

    public GetForexRepo( Context context, Forex_interface forex_interface) {
        this.context = context;
        this.forex_interface = forex_interface;
    }

  public void getForexData(String url){
        forexModelList = new ArrayList<>();
        AndroidNetworking.get(url)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONObject jsonObject = response.getJSONObject("Conversion");
                            JSONArray jsonArray = jsonObject.getJSONArray("Currency");
                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject jobj = jsonArray.getJSONObject(i);
                                ForexModel forexModel = new ForexModel(jobj.getString("BaseCurrency"),jobj.getString("TargetCurrency"),jobj.getString("BaseValue"),jobj.getString("TargetBuy"),jobj.getString("TargetSell"));
                                forexModelList.add(forexModel);
                            }
                            forex_interface.setforex(forexModelList);
                        } catch (JSONException e) {
                            e.printStackTrace();
                       }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
}
