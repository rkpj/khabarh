package khabarhub.com.Nepali.Views.NpHotNews;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import khabarhub.com.Nepali.Model.Categories;
import khabarhub.com.Nepali.Model.NewsPojoClass;
import khabarhub.com.Nepali.RecycleViewPojomodel;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.JustInNewsDto;
import khabarhub.com.Nepali.ViewModelPackage.JustInnewsViewModel;
import khabarhub.com.Nepali.Views.List_of_news_adapters;
import khabarhub.com.NetworkConnection;
import khabarhub.com.R;
import khabarhub.com.calendarnepali.DateConverter;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.google.android.material.snackbar.Snackbar;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecycleViewFragment extends Fragment implements List_of_news_adapters.OnItemClickListner {
    List<Categories> categorieslist;
    Categories categories;
    Dialog dialog;

    List<JustInNewsDto> newslist;
    RecyclerView recyclerView_news;
    List_of_news_adapters adapter;
    SwipeRefreshLayout mswiperefress;
    NetworkConnection networkConnection;
    boolean ntstatus;
    DateConverter dateConverter ;
    int currentpage =1;

    private int mPreviousTotal = 0;
    static int last_page;
    private boolean mLoading = true;
    int total=0;

    public RecycleViewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_recycle_view, container, false);
        recyclerView_news=view.findViewById(R.id.frag_recylce_view_recycleview);
        mswiperefress=view.findViewById(R.id.swiperefresh_fragmentrecy);
        categorieslist=new ArrayList<>();
        dateConverter = new DateConverter();
        newslist =new ArrayList<>();
        prodressdialog();

        networkConnection=new NetworkConnection(getContext());
        AndroidNetworking.initialize(getContext());

        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getContext());
        recyclerView_news.setLayoutManager(layoutManager);
        recyclerView_news.setHasFixedSize(true);
       // recyclerView_news.setAnimation();
        JustInnewsViewModel model= ViewModelProviders.of(this).get(JustInnewsViewModel.class);

        getdataFromObserver(model);
        mswiperefress.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
              getdataFromObserver(model);
            }
        });
//        adapter.setOnItemClickListner(onClickListener);
     //   makenewslist();

        recyclerView_news.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState==0){

                    onScrollrecycleview();
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });
        adapter=new List_of_news_adapters(newslist,getContext(),RecycleViewFragment.this );
        recyclerView_news.setAdapter(adapter);

        return  view;
    }

    private void getdataFromObserver(JustInnewsViewModel model) {
        model.getJustInnews().observe(this, new Observer<List<JustInNewsDto>>() {
            @Override
            public void onChanged(List<JustInNewsDto> recycleViewPojomodels) {
                Log.d("mediaplayer","outsidefor");

                newslist = recycleViewPojomodels;
                adapter=new List_of_news_adapters(newslist,getContext(),RecycleViewFragment.this );
                recyclerView_news.setAdapter(adapter);

                mswiperefress.setRefreshing(false);



            }
        });


    }

    private void onScrollrecycleview() {

        int visibleItemCount = recyclerView_news.getChildCount();
        int totalItemCount = recyclerView_news.getLayoutManager().getItemCount();

        int firstVisibleItem = ((LinearLayoutManager) recyclerView_news.getLayoutManager()).findFirstVisibleItemPosition();

        if (mLoading) {

            if (total > totalItemCount) {
                mLoading = false;
                mPreviousTotal = totalItemCount;
            }

        }

        int visibleThreshold = 5;
        if ( (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + visibleThreshold)) {



            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    currentpage += 1;
                    dialog.show();

                    makenewslist(currentpage);

                    mLoading = true;
                }


            }, 3000);


        }
        else {

        }
    }



    private void makenewslist(int currentpages) {
        AndroidNetworking.get("https://www.khabarhub.com/wp-json/appharurest/v2/posts/0/"+ currentpages)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        dialog.dismiss();
                        for (int i=0;i<response.length();i++){
                            try {
                                categories=new Categories();
                                JSONObject jsonObject = response.getJSONObject(i);
                                int id=(jsonObject.getInt("id"));
                                String title=(jsonObject.optString("title"));
                                String img =(jsonObject.optString("image_link_medium"));

                                String date = dateChangeToAgoNews(jsonObject.optString("date_english"));
                                String desc=(jsonObject.optString("body"));
                                String link=(jsonObject.optString("permalink"));
                                String authorname=(jsonObject.optString("author_name"));
                                String authorimg=(jsonObject.optString("author_image"));
                                JustInNewsDto newsPojoClass = new JustInNewsDto(id,title,date,img,link,desc,authorname,authorimg);

                                newslist.add(newsPojoClass);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        adapter.notifyDataSetChanged();

                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                    }
                });
    }


    @Override
    public void onclick(String title, String date, String news, List<JustInNewsDto> newslists, int clickposition, String url_link) {
        ntstatus=networkConnection.isOnline();
        if (ntstatus==true) {
            Intent intent = new Intent(getContext(), Relativenews_JustIn_nepali.class);
            intent.putExtra("news_title", title);
            //  intent.putExtra("news_title",newslist.get(getAdapterPosition()).getNews_title());
            intent.putExtra("news_date", date);
            intent.putExtra("news_details",news);
            intent.putExtra("newslist", (Serializable) newslists);
            intent.putExtra("clickposition",clickposition);
            intent.putExtra("link",url_link);

            startActivity(intent);

        }
        else {
            Snackbar.make(getView(),"Something is Wrong",Snackbar.LENGTH_LONG).show();
        }
    }



    private String dateChangeToAgoNews(String date){
        String[] date_time = date.split(" ");
        String newsDate = date_time[0];
//        String newsTime = date_time[1];
        SimpleDateFormat formats = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        SimpleDateFormat timeformat = new SimpleDateFormat("HH:mm:ss");

        LocalDate current_date = LocalDate.now();
        LocalTime currentdatetime = LocalTime.now();
        String currentdate = current_date+" "+currentdatetime;
        Log.d("mediaplayer","newdatetime:"+date+" currentdate: "+currentdate);
        String res = "";
        Date d1= null;
        Date d2 = null;

        try{
            d1 = formats.parse(date);
            d2 = formats.parse(currentdate);
//            Date date1 = timeformat.parse(newsTime);
//
//            Date date2 = timeformat.parse(""+currentdatetime);
//
//            long difference = date2.getTime() - date1.getTime();
//
//            int minutes = (int) TimeUnit.MILLISECONDS.toMinutes(difference);
            long diff =Math.abs( d2.getTime() - d1.getTime());
            long diffSeconds = (diff / 1000) % 60;
            int diffMinutes = (int) ((diff / (60 * 1000)) % 60);
            int diffHours = (int) ((diff / (60 * 60 * 1000))%24);
            int diffInDays = (int) ((diff / (1000 * 60 * 60 * 24)));
            int day = 365-diffInDays;
            Log.d("mediaplayer"," min: "+diffMinutes+" hh: "+diffHours+" day: "+diffInDays);
//            Log.d("mediaplayer"," min: "+minutes);
//
//
//            Log.d("mediaplayer"," modulus min :: "+(minutes%60));

            if (diffMinutes < 60 && diffHours == 0 && diffInDays == 0) {
                res = getStringToNelaliFromNumber(diffMinutes) + " मिनेट अगाडि";
            } else if (diffHours < 24 && diffInDays == 0) {
                res = getStringToNelaliFromNumber(diffHours) + " घण्टा अगाडि";
            } else {
             //   Log.d("mediaplayer"," minsss: "+minutes);
                String[] arrayOfDates = newsDate.split("-");
                int engyear = Integer.parseInt(arrayOfDates[0]);
                int engMonth = Integer.parseInt(arrayOfDates[1]);
                int engday = Integer.parseInt(arrayOfDates[2]);
                int npyear =   dateConverter.getNepaliDate(engyear,engMonth,engday).getYear();
                int npmonth =   dateConverter.getNepaliDate(engyear,engMonth,engday).getMonth();
                int npday =   dateConverter.getNepaliDate(engyear,engMonth,engday).getDay();
                res= getStringToNelaliFromNumber(npyear)+"-"+getStringToNelaliFromNumber(npmonth)+"-"+getStringToNelaliFromNumber(npday);


            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return res;
    }


    public static String getStringToNelaliFromNumber(int number) {
        String[] nepaliNumArray = new String[] { "०", "१", "२", "३", "४", "५", "६", "७", "८", "९" };
        StringBuilder result = new StringBuilder();
        while (number > 0) {
            int n = number % 10;
            result.insert(0, nepaliNumArray[n]);
            number /= 10;
        }
        return result.toString();
    }
    void prodressdialog()
    {
        ProgressBar progressBar=new ProgressBar(getContext());

        progressBar.setIndeterminate(true);
        progressBar.setScrollBarSize(2);
        dialog = new Dialog(getActivity());
        progressBar.setMinimumWidth(10);
        progressBar.setMinimumHeight(10);

        //  dialog.getWindow().setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL);
        dialog.getWindow().setBackgroundDrawable(new
                ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(progressBar);

        dialog.setCancelable(true);
        //  dialog.setContentView(R.layout.progressdialoglayout);


    }
}
