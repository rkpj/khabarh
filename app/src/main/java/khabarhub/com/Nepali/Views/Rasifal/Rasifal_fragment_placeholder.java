package khabarhub.com.Nepali.Views.Rasifal;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.StringRequestListener;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import khabarhub.com.Daily_Recycleview_Adapter;
import khabarhub.com.Nepali.Apis;
import khabarhub.com.R;

public class Rasifal_fragment_placeholder extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    RecyclerView mrecycleview;
    List mwebcontent;
    Daily_Recycleview_Adapter adapter;
    Dialog dialog;
    ProgressDialog progressDialog;
    public Rasifal_fragment_placeholder() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static Rasifal_fragment_placeholder newInstance(int sectionNumber) {
        Rasifal_fragment_placeholder fragment = new Rasifal_fragment_placeholder();

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_daily_rasifal, container, false);
        mrecycleview =  rootView.findViewById(R.id.daily_rasifal_recycleview);
        mwebcontent=new ArrayList();
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getContext());
        mrecycleview.setLayoutManager(layoutManager);
        mrecycleview.setHasFixedSize(true);
        mwebcontent.clear();
        prodressdialog();
        progressDialog=new ProgressDialog(getContext());
        progressDialog.setTitle("please wait");
      //  progressDialog.show();
        dialog.show();
        getxmlresponse();
        adapter=new Daily_Recycleview_Adapter(mwebcontent,getContext());
        mrecycleview.setAdapter(adapter);
        return rootView;

    }

    public void getxmlresponse(){
        dialog.show();
        AndroidNetworking.get(Apis.rasifal_daily)
                .setPriority(Priority.LOW)
                .build()
                .getAsString(new StringRequestListener() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        progressDialog.dismiss();
                        try {

                            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                            Document doc = dBuilder.parse(new InputSource(new ByteArrayInputStream(response.getBytes("utf-8"))));

                            Element element=doc.getDocumentElement();
                            element.normalize();

                            NodeList nList = doc.getElementsByTagName("item");
                           for (int i=0; i<nList.getLength(); i++) {
                                Node node = nList.item(i);
                                if (node.getNodeType() == Node.ELEMENT_NODE) {
                                    Element element2 = (Element) node;
                                    mwebcontent.add(getValue("content:encoded",element2));
                                  //  webView.loadData(getValue("content:encoded",element2),"text/html","utf-8");
                                  }
                            }
                            adapter.notifyDataSetChanged();

                        } catch (Exception e) {
                            dialog.dismiss();
                            progressDialog.dismiss();

                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();

                    }
                });
    }
    private static String getValue(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = nodeList.item(0);
        return node.getNodeValue();
    }


    void prodressdialog()
    {
        ProgressBar progressBar=new ProgressBar(getContext());

        progressBar.setIndeterminate(true);
        progressBar.setScrollBarSize(2);
        dialog = new Dialog(getActivity());
        progressBar.setMinimumWidth(10);
        progressBar.setMinimumHeight(10);

        //  dialog.getWindow().setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL);
        dialog.getWindow().setBackgroundDrawable(new
                ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(progressBar);

        dialog.setCancelable(true);
        //  dialog.setContentView(R.layout.progressdialoglayout);


    }
}
