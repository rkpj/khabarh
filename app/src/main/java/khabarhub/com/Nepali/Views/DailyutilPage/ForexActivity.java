package khabarhub.com.Nepali.Views.DailyutilPage;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Calendar;
import java.util.List;

import khabarhub.com.LanguageSharepreference;
import khabarhub.com.Nepali.Model.ForexModel;
import khabarhub.com.Nepali.RepositoryPackage.GetForexRepo;
import khabarhub.com.R;
import khabarhub.com.Utils;

public class ForexActivity extends AppCompatActivity implements Forex_interface {

    RecyclerView mforex_recycleview;
    ProgressBar progressBar;
    LinearLayout linearLayout;
    TextView forex_title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forex);
        mforex_recycleview = findViewById(R.id.recy_forex);
        progressBar = findViewById(R.id.progressbar_forex);
        linearLayout =findViewById(R.id.linear_layout_forex);
        forex_title = findViewById(R.id.txt_title_forex);
        String language = LanguageSharepreference.Instance(this).getlanguage();
        if (language!=null && language.equalsIgnoreCase("Nepali"))
            forex_title.setText("विदेशी विनिमय दर");
        mforex_recycleview.setLayoutManager(new LinearLayoutManager(this));
        LayoutInflater inflator = (LayoutInflater) this .getSystemService(this.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.toolbar_custom, null);
        ActionBar actionBar =getSupportActionBar();

        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setCustomView(v);
        new ForexAsyncTask(this,this).execute();

    }

    @Override
    public void setforex(List<ForexModel> forexModels) {
        if (forexModels!=null){
            mforex_recycleview.setAdapter(new Adapter_Forex(ForexActivity.this,forexModels));
            progressBar.setVisibility(View.GONE);
            linearLayout.setVisibility(View.VISIBLE);
        }

    }

    private class ForexAsyncTask extends AsyncTask<Void,Void,Void> {
        Forex_interface forex_interface;
        Context context;

        public ForexAsyncTask(Forex_interface forex_interface, Context context) {
            this.forex_interface = forex_interface;
            this.context = context;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
            int month = (Calendar.getInstance().get(Calendar.MONTH)+1);
            String months= String.format( "%02d",month);
            int day = (Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
            String days= String.format( "%02d",day);


            new GetForexRepo(context,forex_interface).getForexData(Utils.forex+year+"&MM="+months+"&DD="+days);
            return null;
        }
    }
}
