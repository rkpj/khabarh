 package khabarhub.com.Nepali.Views.DailyutilPage;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import java.util.ArrayList;
import java.util.List;

import khabarhub.com.Nepali.Model.ShareMarket_CompanyModel;
import khabarhub.com.Nepali.sharedatasget.ShareMarketDatas;
import khabarhub.com.Nepali.sharedatasget.Sharemarket_Company_Interface;
import khabarhub.com.R;

public class ShareMarketActivity extends AppCompatActivity implements Sharemarket_Company_Interface{

    Dialog dialog;
    List<ShareMarket_CompanyModel> list;
    RecyclerView mrecycle_sharecompany;
    Button mviewallbt;
    ProgressBar progressBar;
    LinearLayout linearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_market);
        mrecycle_sharecompany =findViewById(R.id.recy_sharemarket_shares);
        mviewallbt= findViewById(R.id.btn_viewall_shares);
        progressBar =findViewById(R.id.progressbar_circular);
        linearLayout = findViewById(R.id.linear_layout_sharemarket);
        mrecycle_sharecompany.setLayoutManager(new LinearLayoutManager(this));
      list= new ArrayList<>();
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        LayoutInflater inflator = (LayoutInflater) this .getSystemService(this.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.toolbar_custom, null);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setCustomView(v);
        Sharemarket_Company_Interface sharemarket_company_interface= this;
       new GetSharemarketsAsync(sharemarket_company_interface,this,1,"10").execute();
        progressBar.setVisibility(View.VISIBLE);
       mviewallbt.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
            String limit= String.valueOf(list.get(0).getRowTotal());
               new GetSharemarketsAsync(sharemarket_company_interface,ShareMarketActivity.this,1,limit).execute();

           }
       });
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }



    @Override
    public void setCompanyShare(List<ShareMarket_CompanyModel> companyShare) {
        if (companyShare!= null)
        {
            list=companyShare;
            progressBar.setVisibility(View.GONE);
            linearLayout.setVisibility(View.VISIBLE);
            mrecycle_sharecompany.setAdapter(new Adapter_ShareMarket_Company(ShareMarketActivity.this,companyShare));


        }

    }




    class GetSharemarketsAsync extends AsyncTask<Void, Void, Void> {
Sharemarket_Company_Interface sharemarket_company_interface;
        Context context;
        int offset;
        String limit;

        public GetSharemarketsAsync(Sharemarket_Company_Interface sharemarket_company_interface, Context context, int offset, String limit) {
            this.sharemarket_company_interface = sharemarket_company_interface;
            this.context = context;
            this.offset = offset;
            this.limit = limit;
        }

        @Override
        protected Void doInBackground(Void... voids) {

        ShareMarketDatas shareMarketDatas= new ShareMarketDatas(sharemarket_company_interface,context);
        shareMarketDatas.getSharemarketCompany("https://www.nepalipaisa.com/Modules/GraphModule/webservices/MarketWatchService.asmx/GetTodaySharePrices",offset,limit);

      return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);



        }
    }
}
