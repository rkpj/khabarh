package khabarhub.com.Nepali.Views.DailyutilPage;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import khabarhub.com.R;

public class Adapter_holidays extends RecyclerView.Adapter<Adapter_holidays.MyViews> {
    Context context;
    String [] text;

    public Adapter_holidays(Context context, String[] text) {
        this.context = context;
        this.text = text;
    }

    @NonNull
    @Override
    public MyViews onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context).inflate(R.layout.singletextview_layout,parent,false);
        return new MyViews(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViews holder, int position) {
        holder.holiday.setText(text[position]);
        holder.holiday.setTextColor(Color.parseColor("#FF0000"));
    }

    @Override
    public int getItemCount() {
        return text.length;
    }
    public class MyViews extends RecyclerView.ViewHolder {
        TextView holiday;
        public MyViews(@NonNull View itemView) {
            super(itemView);
            holiday = itemView.findViewById(R.id.singletextview);
        }
    }
}
