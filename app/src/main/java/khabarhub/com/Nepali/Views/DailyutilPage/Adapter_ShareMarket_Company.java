package khabarhub.com.Nepali.Views.DailyutilPage;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import khabarhub.com.Nepali.Model.ShareMarket_CompanyModel;
import khabarhub.com.R;

public class Adapter_ShareMarket_Company extends RecyclerView.Adapter<Adapter_ShareMarket_Company.Myviews> {
   Context context;
   List<ShareMarket_CompanyModel> shareMarket_companyModelList;

    public Adapter_ShareMarket_Company(Context context, List<ShareMarket_CompanyModel> shareMarket_companyModelList) {
        this.context = context;
        this.shareMarket_companyModelList = shareMarket_companyModelList;
    }

    @NonNull
    @Override
    public Myviews onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.sharemarket_adapter_layout,parent,false);

        return new Myviews(v);
    }

    @Override
    public void onBindViewHolder(@NonNull Myviews holder, int position) {
        ShareMarket_CompanyModel shareMarket_companyModel= shareMarket_companyModelList.get(position);

        holder.sym.setText(shareMarket_companyModel.getStockName());
        holder.min.setText(""+shareMarket_companyModel.getMinPrice());
        holder.max.setText(""+shareMarket_companyModel.getMaxPrice());
        holder.cp.setText(""+shareMarket_companyModel.getClosingPrice());
        holder.qty.setText(""+shareMarket_companyModel.getTradedShares());



    }

    @Override
    public int getItemCount() {
        return shareMarket_companyModelList.size();
    }

    public class Myviews extends RecyclerView.ViewHolder {
        TextView sym,min,max,cp,qty;

        public Myviews(@NonNull View itemView) {
            super(itemView);
            sym = itemView.findViewById(R.id.txt_sharecompany_sym);
            min= itemView.findViewById(R.id.txt_sharecompany_mins);
            max = itemView.findViewById(R.id.txt_sharecompany_maxs);
            cp = itemView.findViewById(R.id.txt_sharecompany_cps);
            qty = itemView.findViewById(R.id.txt_sharecompany_qtys);
        }
    }
}
