package khabarhub.com.Nepali.Views.Rasifal;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import khabarhub.com.R;

public class RasifalFragment_for_botton_tab extends Fragment {
    private SectionsPagerAdapter mSectionsPagerAdapters;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_rasifal, container, false);

       mSectionsPagerAdapters = new SectionsPagerAdapter(getFragmentManager());
        tabLayout = (TabLayout)view.findViewById(R.id.tab_rasifal);

        // Set up the ViewPager with the sections adapter.
        viewPager = (ViewPager) view.findViewById(R.id.container_rasifal);
        viewPager.setAdapter(mSectionsPagerAdapters);
        tabLayout.setupWithViewPager(viewPager);

        return view;
    }


        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return Rasifal_fragment_placeholder.newInstance(position + 1);

                case 1:


                    return Fragment_rasifal_weekly.newInstance(position + 8);

                case 2:
                    return new Rasifal_yearly_fragment();
            }
            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }


        @Override
        public CharSequence getPageTitle(int position) {
                            switch (position) {
                    case 0:
                        return "दैनिक राशिफल ";
                    case 1:
                        return "साप्ताहिक राशिफल ";

                    case 2:
                        return "बार्षिक राशिफल ";


            }

            return null;
        }
    }
}
