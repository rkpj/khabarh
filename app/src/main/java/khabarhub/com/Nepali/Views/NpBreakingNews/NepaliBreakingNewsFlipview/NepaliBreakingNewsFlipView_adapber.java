package khabarhub.com.Nepali.Views.NpBreakingNews.NepaliBreakingNewsFlipview;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.cardview.widget.CardView;

import com.squareup.picasso.Picasso;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.BreakingNewsDto;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.JustInNewsDto;
import khabarhub.com.R;
import khabarhub.com.calendarnepali.DateConverter;
import khabarhub.com.radio.AudioManage;

public class NepaliBreakingNewsFlipView_adapber extends BaseAdapter {
    Context context;
    List<BreakingNewsDto> list;
    List<JustInNewsDto>justInNewsDtoList;
    OnItemClickListner monClickListener;
    int pos;
    int currrentpos=0;
    DateConverter dateConverter;
    public interface OnItemClickListner {
        void onclickBreakMain(String title, String date, String news, List<BreakingNewsDto> newslists, int clickposition, String url_link,ImageView imageView);
        void onclickBreakRelated(String title, String date, String news, List<JustInNewsDto> newslists, int clickposition, String url_link,ImageView imageView);

    }

    public NepaliBreakingNewsFlipView_adapber(Context context, List<BreakingNewsDto> list,List<JustInNewsDto> justInNewsDtos, OnItemClickListner monClickListener) {
        this.context = context;
        this.list = list;
        this.justInNewsDtoList=justInNewsDtos;
        this.monClickListener = monClickListener;
     //   timemanage();

    }

    private void timemanage() {
        for (int i=0; i<justInNewsDtoList.size();i++){
            String time = dateChangeToAgoNews(justInNewsDtoList.get(i).getNews_date());
            justInNewsDtoList.get(i).setNews_date(time);
        }
        for (int i=0; i<list.size();i++){
            String time = dateChangeToAgoNews(list.get(i).getNews_date());
            list.get(i).setNews_date(time);
        }
    }

    @Override
    public int getCount()
    {
        if (list.size()>6) {
            return 5;
        }
        else {
            return list.size();
        }
    }

    @Override
    public Object getItem(int position) {
        return 0;
    }

    @Override
    public long getItemId(int position) {
       return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        dateConverter = new DateConverter();

        if (convertView ==null){
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.nepalibreakingnews_flipview_adapter,parent,false);

            holder.imageView1 = convertView.findViewById(R.id.img_nepalibreaking_flipview_image);
            holder.title1 =convertView.findViewById(R.id.txt_nepalibreaking_flipview_title);
            holder.date1 = convertView.findViewById(R.id.txt_nepalibreaking_flipview_date);
            holder.desc1 = convertView.findViewById(R.id.desc_npbreak_1);
            holder.cardView1 = convertView.findViewById(R.id.cardview_nepalibreaking_adapter_1news);
            holder.author_name = convertView.findViewById(R.id.txt_nepalibreaking_flipview_author);
            holder.cardView2 = convertView.findViewById(R.id.cardview_nepalibreaking_adapter_2news);
            holder.imageView2 = convertView.findViewById(R.id.btn_nepalibreaking_flipview_2news_img);
            holder.title2 =convertView.findViewById(R.id.btn_nepalibreaking_flipview_2news_title);
            holder.date2 = convertView.findViewById(R.id.btn_nepalibreaking_flipview_2news_date);
            holder.desc2 = convertView.findViewById(R.id.txt_large_desc);

            holder.cardView3 = convertView.findViewById(R.id.cardview_nepalibreaking_adapter_3news);
            holder.imageView3 = convertView.findViewById(R.id.btn_nepalibreaking_flipview_3news_img);
            holder.title3 =convertView.findViewById(R.id.btn_nepalibreaking_flipview_3news_title);
            holder.date3 = convertView.findViewById(R.id.btn_nepalibreaking_flipview_3news_date);
            holder.desc3 = convertView.findViewById(R.id.txt_large_desc3);


            holder.linearLayout = convertView.findViewById(R.id.linear_nepali_flipview);
            holder.liner_sideby_side = convertView.findViewById(R.id.linear_flip_other_sidebyside);
            LinearLayoutCompat.LayoutParams linear= (LinearLayoutCompat.LayoutParams) holder.linearLayout.getLayoutParams();

            if(AudioManage.INSTANCE(context).getAudioFlag() == true){
                //  LinearLayout.LayoutParams linear = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,0,5);
                linear.weight = 4f;
                holder.linearLayout.setLayoutParams(linear);
             //   holder.liner_sideby_side.setPadding(0,0,0,215);
                holder.desc1.setVisibility(View.GONE);

            }
            convertView.setTag(holder);
        }
        else {
           holder = (ViewHolder) convertView.getTag();
        }
        BreakingNewsDto breakingNewsDto = list.get(position);

        if (position ==0){
            setValues(holder,position,position,position+1);
        }

        if (position ==1){
            setValues(holder,position,position+1,position+2);
        }
        if (position ==2){
            setValues(holder,position,position+2,position+3);
        }
        if (position ==3){
            setValues(holder,position,position+3,position+4);
        }
        if (position ==4){
            setValues(holder,position,position+4,position+5);
        }





        return convertView;
    }

    private void setValues(ViewHolder holder, int pos, int i, int i1) {
                    holder.title1.setText(""+list.get(pos).getNews_title());
            holder.date1.setText(""+(list.get(pos).getNews_date()));
        holder.desc1.setText(""+list.get(pos).getNews_desc());
        holder.author_name.setText(list.get(pos).getAuthor_name());
        Picasso.with(context).load(list.get(pos).getNews_img()).placeholder(R.drawable.opcity_logo).into(holder.imageView1);
            holder.cardView1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    monClickListener.onclickBreakMain(list.get(pos).getNews_title(),list.get(pos).getNews_date(),list.get(pos).getNews_body(),list,pos,list.get(pos).getNews_link(),holder.imageView1);
                }
            });


            holder.title2.setText(""+justInNewsDtoList.get(i).getNews_title()+"...");
            holder.date2.setText(""+(justInNewsDtoList.get(i).getNews_date()));
            Picasso.with(context).load(justInNewsDtoList.get(i).getNews_img()).placeholder(R.drawable.opcity_logo).into(holder.imageView2);
        holder.imageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                monClickListener.onclickBreakRelated(justInNewsDtoList.get(i).getNews_title(),justInNewsDtoList.get(i).getNews_date(),justInNewsDtoList.get(i).getNews_body(),justInNewsDtoList,i,justInNewsDtoList.get(i).getNews_link(),holder.imageView2);

            }
        });

        holder.cardView2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    monClickListener.onclickBreakRelated(justInNewsDtoList.get(i).getNews_title(),justInNewsDtoList.get(i).getNews_date(),justInNewsDtoList.get(i).getNews_body(),justInNewsDtoList,i,justInNewsDtoList.get(i).getNews_link(),holder.imageView2);
                }
            });


            holder.title3.setText(""+justInNewsDtoList.get(i1).getNews_title()+"...");
            holder.date3.setText(""+(justInNewsDtoList.get(i1).getNews_date()));
            Picasso.with(context).load(justInNewsDtoList.get(i1).getNews_img()).placeholder(R.drawable.opcity_logo).into(holder.imageView3);
           holder.imageView3.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   monClickListener.onclickBreakRelated(justInNewsDtoList.get(i1).getNews_title(),justInNewsDtoList.get(i1).getNews_date(),justInNewsDtoList.get(i1).getNews_body(),justInNewsDtoList,i1,justInNewsDtoList.get(i1).getNews_link(),holder.imageView3);

               }
           });

            holder.cardView3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    monClickListener.onclickBreakRelated(justInNewsDtoList.get(i1).getNews_title(),justInNewsDtoList.get(i1).getNews_date(),justInNewsDtoList.get(i1).getNews_body(),justInNewsDtoList,i1,justInNewsDtoList.get(i1).getNews_link(),holder.imageView3);
                }
            });

    }

    static class ViewHolder {
        TextView title1,date1,desc1,title2,date2,desc2,title3,date3,desc3,author_name;

        ImageView imageView1,imageView2,imageView3;
     //   TextView webView;
        CardView cardView1,cardView2,cardView3;
        View bottomview;
        LinearLayoutCompat linearLayout;
        LinearLayout liner_sideby_side;
    }



    private String dateChangeToAgoNews(String date){
        String[] date_time = date.split(" ");
//        String newsDate = date_time[0];
//        String newsTime = date_time[1];
        SimpleDateFormat formats = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        SimpleDateFormat timeformat = new SimpleDateFormat("HH:mm:ss");

        LocalDate current_date = LocalDate.now();
        LocalTime currentdatetime = LocalTime.now();
        String currentdate = current_date+" "+currentdatetime;
        Log.d("mediaplayer","newdatetime:"+date+" currentdate: "+currentdate);
        String res = "";
        Date d1= null;
        Date d2 = null;

        try{
            d1 = formats.parse(date);
            d2 = formats.parse(currentdate);
//            Date date1 = timeformat.parse(newsTime);
//
//            Date date2 = timeformat.parse(""+currentdatetime);

        //    long difference = d2.getTime() - d1.getTime();

          //  int minutes = (int) TimeUnit.MILLISECONDS.toMinutes(difference);
            long diff =Math.abs( d2.getTime() - d1.getTime());
            long diffSeconds = (diff / 1000) % 60;
            int diffMinutes = (int) ((diff / (60 * 1000)) % 60);
            int diffHours = (int) ((diff / (60 * 60 * 1000))%24);
            int diffInDays = (int) ((diff / (1000 * 60 * 60 * 24)));
            int day = 365-diffInDays;
            Log.d("mediaplayer"," min: "+diffMinutes+" hh: "+diffHours+" day: "+diffInDays);
          //  Log.d("mediaplayer"," min: "+minutes);


        //    Log.d("mediaplayer"," modulus min :: "+(minutes%60));

            if (diffMinutes < 60 && diffHours == 0 && diffInDays == 0) {
                res = getStringToNelaliFromNumber(diffMinutes) + " मिनेट अगाडि";
            } else if (diffHours < 24 && diffInDays == 0) {
                res = getStringToNelaliFromNumber(diffHours) + " घण्टा अगाडि";
            } else {
              //  Log.d("mediaplayer"," minsss: "+minutes);
                String[] arrayOfDates = date.split("-");
                int engyear = Integer.parseInt(arrayOfDates[0]);
                int engMonth = Integer.parseInt(arrayOfDates[1]);
                int engday = Integer.parseInt(arrayOfDates[2]);
                int npyear =   dateConverter.getNepaliDate(engyear,engMonth,engday).getYear();
                int npmonth =   dateConverter.getNepaliDate(engyear,engMonth,engday).getMonth();
                int npday =   dateConverter.getNepaliDate(engyear,engMonth,engday).getDay();
                res= getStringToNelaliFromNumber(npyear)+"-"+getStringToNelaliFromNumber(npmonth)+"-"+getStringToNelaliFromNumber(npday);


            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return res;
    }


    public static String getStringToNelaliFromNumber(int number) {
        String[] nepaliNumArray = new String[] { "०", "१", "२", "३", "४", "५", "६", "७", "८", "९" };
        StringBuilder result = new StringBuilder();
        while (number > 0) {
            int n = number % 10;
            result.insert(0, nepaliNumArray[n]);
            number /= 10;
        }
        return result.toString();
    }
}
