package khabarhub.com.Nepali.Views.NpCategoriesNews;


import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import khabarhub.com.Nepali.Model.NewsPojoClass;
import khabarhub.com.Nepali.Multiplelayout_list_adapter;
import khabarhub.com.NetworkConnection;
import khabarhub.com.R;
import khabarhub.com.calendarnepali.DateConverter;

import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.google.android.material.snackbar.Snackbar;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;


/**
 * A simple {@link Fragment} subclass.
 */
public class Fragment_of_Categories_news_list extends Fragment implements Multiplelayout_list_adapter.OnItemClickListner {

    private ImageView imageView_singlenews;
    private TextView textView_singlenews_title,textView_singlenews_date;

    List<NewsPojoClass> newslist=new ArrayList<>();
    RecyclerView recyclerView_news;
    Multiplelayout_list_adapter adapter;
    SwipeRefreshLayout mswiperefresh;
    int categ_id;
    NetworkConnection networkConnection;
    boolean ntstatus;
    Dialog dialog;

    int total=0;
    private int mPreviousTotal = 0;
    static int current_page;
    static int last_page;
    /**
     * True if we are still waiting for the last set of data to load.
     */
    DateConverter dateConverter;

    private boolean mLoading = true;

    public Fragment_of_Categories_news_list() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_recycle_view, container, false);
        mswiperefresh=view.findViewById(R.id.swiperefresh_fragmentrecy);
        Bundle b=getArguments();
        networkConnection=new NetworkConnection(getContext());
        prodressdialog();
        dateConverter = new DateConverter();
        categ_id=b.getInt("cat_id");
        recyclerView_news=view.findViewById(R.id.frag_recylce_view_recycleview);
        AndroidNetworking.initialize(getContext());

        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getContext());
        recyclerView_news.setLayoutManager(layoutManager);
        recyclerView_news.setHasFixedSize(true);

        // recyclerView_news.setAnimation();
        newslist.clear();
        current_page=1;
        dialog.show();
        makenewslist(current_page);
        recyclerView_news.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (newState==0){

                    onScrollrecycleview();
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

            }
        });
        adapter=new Multiplelayout_list_adapter(newslist,getContext(),Fragment_of_Categories_news_list.this);
        recyclerView_news.setAdapter(adapter);
        mswiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                current_page=1;
                makenewslist(current_page);
                mswiperefresh.setRefreshing(false);

            }
        });
        return  view;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

    private void onScrollrecycleview() {

        int visibleItemCount = recyclerView_news.getChildCount();
        int totalItemCount = recyclerView_news.getLayoutManager().getItemCount();

        int firstVisibleItem = ((LinearLayoutManager) recyclerView_news.getLayoutManager()).findFirstVisibleItemPosition();

        if (mLoading) {

            if (total > totalItemCount) {
                mLoading = false;
                mPreviousTotal = totalItemCount;
            }

        }

        int visibleThreshold = 5;
        if ( (totalItemCount - visibleItemCount)
                <= (firstVisibleItem + visibleThreshold)) {




            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    current_page += 1;
                      //  dialog.show();
                        makenewslist(current_page);
                    mLoading = true;
                }


            }, 3000);


        }
        else {

        }
    }

    private void makenewslist(int pagenumber) {
        ntstatus=networkConnection.isOnline();
        if (ntstatus==true){


            AndroidNetworking.get("https://www.khabarhub.com/wp-json/appharurest/v2/posts/"+categ_id+"/"+pagenumber)
                    .setPriority(Priority.LOW)
                    .build()
                    .getAsJSONArray(new JSONArrayRequestListener() {
                        @Override
                        public void onResponse(JSONArray response) {
                            dialog.dismiss();

                            for (int i = 0; i < response.length(); i++) {
                                try {

                                    NewsPojoClass newsPojoClass = new NewsPojoClass();
                                    JSONObject jsonObject = response.getJSONObject(i);
                                    newsPojoClass.setNews_id(jsonObject.getInt("id"));
                                    newsPojoClass.setTitle(jsonObject.optString("title"));
                                    newsPojoClass.setImage(jsonObject.optString("image_link_medium"));

                                String date = dateChangeToAgoNews(jsonObject.optString("date_english"));
                                    newsPojoClass.setDate(date);
                                    newsPojoClass.setDesc(jsonObject.optString("body"));
                                    newsPojoClass.setNews_link(jsonObject.optString("permalink"));
                                    newsPojoClass.setAuthor_name(jsonObject.optString("author_name"));
                                    newsPojoClass.setAuthor_img(jsonObject.optString("author_image"));

                                       newslist.add(newsPojoClass);



                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.d("mediaplayer","error"+e.getMessage());

                                }

                            }
                            adapter.notifyDataSetChanged();

                        }

                        @Override
                        public void onError(ANError anError) {
                            dialog.dismiss();
                            Snackbar.make(getView(),"Something is wrong",Snackbar.LENGTH_LONG).show();
                        }
                    });

        }
        else {
          //  Snackbar.make(getView(),"Something is wrong",Snackbar.LENGTH_LONG).show();
        }

    }

    @Override
    public void onclick(int news_id, String news_img) {

    }

    @Override
    public void click(String title, String date, String news,List<NewsPojoClass> newslists,int clickposition,String url_link) {
        ntstatus=networkConnection.isOnline();
        if (ntstatus==true) {

            Intent intent = new Intent(getContext(), RelatednewsActivity_with_news_details_page.class);
            intent.putExtra("news_title", title);
            //  intent.putExtra("news_title",newslist.get(getAdapterPosition()).getNews_title());
            intent.putExtra("news_date", date);
              intent.putExtra("news_details",news);
              intent.putExtra("newslist", (Serializable) newslists);
              intent.putExtra("clickposition",clickposition);
              intent.putExtra("link",url_link);

            startActivity(intent);

        }
        else {
            Snackbar.make(getView(),"Something is Wrong",Snackbar.LENGTH_LONG).show();
        }

    }

    void prodressdialog()
    {
        ProgressBar progressBar=new ProgressBar(getContext());

        progressBar.setIndeterminate(true);
        progressBar.setScrollBarSize(2);
        dialog = new Dialog(getActivity());
        progressBar.setMinimumWidth(10);
        progressBar.setMinimumHeight(10);

      //  dialog.getWindow().setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL);
        dialog.getWindow().setBackgroundDrawable(new
                ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(progressBar);

        dialog.setCancelable(true);
      //  dialog.setContentView(R.layout.progressdialoglayout);


    }




    private String dateChangeToAgoNews(String date){
        String[] date_time = date.split(" ");
        String newsDate = date_time[0];
        String newsTime = date_time[1];
        SimpleDateFormat formats = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
        SimpleDateFormat timeformat = new SimpleDateFormat("HH:mm:ss");

        LocalDate current_date = LocalDate.now();
        LocalTime currentdatetime = LocalTime.now();
        String currentdate = current_date+" "+currentdatetime;
        Log.d("mediaplayer","newdatetime:"+date+" currentdate: "+currentdate);
        String res = "";
        Date d1= null;
        Date d2 = null;

        try{
            d1 = formats.parse(date);
            d2 = formats.parse(currentdate);
            Date date1 = timeformat.parse(newsTime);

            Date date2 = timeformat.parse(""+currentdatetime);

            long difference = date2.getTime() - date1.getTime();

            int minutes = (int) TimeUnit.MILLISECONDS.toMinutes(difference);
            long diff =Math.abs( d2.getTime() - d1.getTime());
            long diffSeconds = (diff / 1000) % 60;
            int diffMinutes = (int) ((diff / (60 * 1000)) % 60);
            int diffHours = (int) ((diff / (60 * 60 * 1000))%24);
            int diffInDays = (int) ((diff / (1000 * 60 * 60 * 24)));
            int day = 365-diffInDays;
            Log.d("mediaplayer"," min: "+diffMinutes+" hh: "+diffHours+" day: "+diffInDays);
            Log.d("mediaplayer"," min: "+minutes);


                Log.d("mediaplayer"," modulus min :: "+(minutes%60));

                if (diffMinutes < 60 && diffHours == 0 && diffInDays == 0) {
                    res = getStringToNelaliFromNumber(diffMinutes) + " मिनेट अगाडि";
                } else if (diffHours < 24 && diffInDays == 0) {
                    res = getStringToNelaliFromNumber(diffHours) + " घण्टा अगाडि";
                } else {
                    Log.d("mediaplayer"," minsss: "+minutes);
                    String[] arrayOfDates = newsDate.split("-");
                    int engyear = Integer.parseInt(arrayOfDates[0]);
                    int engMonth = Integer.parseInt(arrayOfDates[1]);
                    int engday = Integer.parseInt(arrayOfDates[2]);
                    int npyear =   dateConverter.getNepaliDate(engyear,engMonth,engday).getYear();
                    int npmonth =   dateConverter.getNepaliDate(engyear,engMonth,engday).getMonth();
                    int npday =   dateConverter.getNepaliDate(engyear,engMonth,engday).getDay();
                    res= getStringToNelaliFromNumber(npyear)+"-"+getStringToNelaliFromNumber(npmonth)+"-"+getStringToNelaliFromNumber(npday);


            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return res;
    }


    public static String getStringToNelaliFromNumber(int number) {
        String[] nepaliNumArray = new String[] { "०", "१", "२", "३", "४", "५", "६", "७", "८", "९" };
        StringBuilder result = new StringBuilder();
        while (number > 0) {
            int n = number % 10;
            result.insert(0, nepaliNumArray[n]);
            number /= 10;
        }
        return result.toString();
    }

}
