package khabarhub.com.Nepali.Views.NpBreakingNews.NepaliBreakingNewsFlipview;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.BreakingNewsDto;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.JustInDb;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.JustInNewsDto;
import khabarhub.com.Nepali.ViewModelPackage.JustInnewsViewModel;
import khabarhub.com.Nepali.Views.NpHotNews.Relativenews_JustIn_nepali;
import khabarhub.com.NetworkConnection;
import khabarhub.com.R;
import khabarhub.com.calendarnepali.DateConverter;
import khabarhub.com.radio.AudioManage;
import se.emilsjolander.flipview.FlipView;

public class NepaliBreakingNewsFlipView extends Fragment implements NepaliBreakingNewsFlipView_adapber.OnItemClickListner, SwipeRefreshLayout.OnRefreshListener, FlipView.OnFlipListener{

    Dialog dialog;
    JustInnewsViewModel npbreakingnews;
    NetworkConnection networkConnection;
    NepaliBreakingNewsFlipView_adapber adapter;
    SwipeRefreshLayout mswiperefresh;
    FlipView flipView;
   Map<String,List> mapList;
   LinearLayout linearLayout;
   DateConverter dateConverter;

    File file ;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.nepalibreakingnews_flipview, container, false);
        //  viewPager = view.findViewById(R.id.breakingnews_viewpager);
        flipView=view.findViewById(R.id.flip_view_nepali_breaking);
        mswiperefresh=view.findViewById(R.id.swiperefresh_nepali_breaking_fragmentrecy);
        mswiperefresh.setRefreshing(false);
        mswiperefresh.setEnabled(false);
        AndroidNetworking.initialize(getContext());
        networkConnection=new NetworkConnection(getContext());
        dateConverter = new DateConverter();
        file = new File(getContext().getFilesDir(),"audiopodo.json");
        //  dialog.show();
        flipView.setOnFlipListener(this);
        flipView.peakNext(false);
    //    flipView.setOverFlipMode(OverFlipMode.RUBBER_BAND);

        flipView.setOnFlipListener(this);

        mapList = new HashMap<>();
        prodressdialog();
        dialog.show();
        npbreakingnews = ViewModelProviders.of(this).get(JustInnewsViewModel.class);

        JustInnewsViewModel model= ViewModelProviders.of(this).get(JustInnewsViewModel.class);

      //  loadadapter();
        mswiperefresh.setOnRefreshListener(this);
        NetworkConnection networkConnection = new NetworkConnection(getContext());
        boolean ntstatus = networkConnection.isOnline();
        fetchprograms(ntstatus);

        return view;
    }

    private void loadadapter() {
        npbreakingnews.getJustInnews().observe(this, new Observer<List<JustInNewsDto>>() {
            @Override
            public void onChanged(List<JustInNewsDto> justInNewsDtos) {

                npbreakingnews.getBreakingNepalinews().observe(NepaliBreakingNewsFlipView.this, new Observer<List<BreakingNewsDto>>() {
                    @Override
                    public void onChanged(List<BreakingNewsDto> breakingNewsDtos) {
                        if (justInNewsDtos.size()>0)
                        {
                            if (breakingNewsDtos.size()>0){
                                adapter = new NepaliBreakingNewsFlipView_adapber(getContext(), breakingNewsDtos, justInNewsDtos, NepaliBreakingNewsFlipView.this);
                                flipView.setAdapter(adapter);
                                dialog.dismiss();


                            }
                            else {
                                Log.e("Khabarhub_log","breaknews nepali size"+breakingNewsDtos.size());

                            }

                        }else {
                            Log.e("Khabarhub_log","justnews nepali size"+justInNewsDtos.size());

                        }

                        //     mapList.put("breakingnews",breakingNewsDtos);


                    }
                });
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        loadadapter();
    }

    void prodressdialog()
    {
        ProgressBar progressBar=new ProgressBar(getContext());

        progressBar.setIndeterminate(true);
        progressBar.setScrollBarSize(2);
        dialog = new Dialog(getActivity());
        progressBar.setMinimumWidth(10);
        progressBar.setMinimumHeight(10);

        //  dialog.getWindow().setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL);
        dialog.getWindow().setBackgroundDrawable(new
                ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(progressBar);

        dialog.setCancelable(true);
        //  dialog.setContentView(R.layout.progressdialoglayout);


    }

    @Override
    public void onclickBreakMain(String title, String date, String news, List<BreakingNewsDto> newslists, int position, String url_link, ImageView imageView) {
//        for (int i =0;i<newslists.size();i++){
//            String dates = dateChangeToAgoNews(newslists.get(i).getNews_date());
//            newslists.get(i).setNews_date(dates);
//        }
        Intent intent = new Intent(getContext(), Relativenews_breaking_nepali.class);
        intent.putExtra("news_title", newslists.get(position).getNews_title());
        //  intent.putExtra("news_title",newslist.get(getAdapterPosition()).getNews_title());
        intent.putExtra("news_date", newslists.get(position).getNews_date());
        intent.putExtra("news_details",newslists.get(position).getNews_body());
        intent.putExtra("newslist", (Serializable) newslists);
        intent.putExtra("clickposition",position);
        intent.putExtra("link",newslists.get(position).getNews_link());
        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) getContext(),imageView, ViewCompat.getTransitionName(imageView));

        startActivity(intent,optionsCompat.toBundle());
    }

    @Override
    public void onclickBreakRelated(String title, String date, String news, List<JustInNewsDto> newslists, int position, String url_link, ImageView imageView) {
//        for (int i =0;i<newslists.size();i++){
//            String dates = dateChangeToAgoNews(newslists.get(i).getNews_date());
//            newslists.get(i).setNews_date(dates);
//        }
        Intent intent = new Intent(getContext(), Relativenews_JustIn_nepali.class);
        intent.putExtra("news_title", newslists.get(position).getNews_title());
        //  intent.putExtra("news_title",newslist.get(getAdapterPosition()).getNews_title());
        intent.putExtra("news_date", newslists.get(position).getNews_date());
        intent.putExtra("news_details",newslists.get(position).getNews_body());
        intent.putExtra("newslist", (Serializable) newslists);
        intent.putExtra("clickposition",position);
        intent.putExtra("link",newslists.get(position).getNews_link());
        ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) getContext(),imageView, ViewCompat.getTransitionName(imageView));

        startActivity(intent,optionsCompat.toBundle());

    }

    @Override
    public void onRefresh() {
//        npbreakingnews.getBreakingNepalinews().observe(this, new Observer<List<BreakingNewsDto>>() {
//            @Override
//            public void onChanged(List<BreakingNewsDto> breakingNewsDtos) {
//                dialog.show();
//                adapter=new NepaliBreakingNewsFlipView_adapber(getContext(), breakingNewsDtos,NepaliBreakingNewsFlipView.this );
//                flipView.setAdapter((ListAdapter) adapter);
//                mswiperefresh.setRefreshing(false);
//                dialog.dismiss();
//            }
//        });
    }

    @Override
    public void onFlippedToPage(FlipView v, int position, long id) {

        if (position==0){

            mswiperefresh.setEnabled(true);
            mswiperefresh.setRefreshing(true);
        }
        else {
            mswiperefresh.setEnabled(false);
        }

    }



    private void fetchprograms(boolean ntstatus) {
        if (ntstatus == true){
            AndroidNetworking.get("https://radiocandid.com/api/all-schedules?fbclid=IwAR3Mow6hvzVsO3CPr1_iUPx59nkD36pWaMlqVey6emvPYdBtRA1XWQEa77Q")
                    .setPriority(Priority.LOW)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                savejsonflie(response);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {

                        }
                    });
        }
    }
    private void savejsonflie(JSONObject jsonObject) throws IOException {
        String userString = jsonObject.toString();
// Define the File Path and its Name
        FileWriter fileWriter = new FileWriter(file);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        bufferedWriter.write(userString);
        bufferedWriter.close();
    }

//    private String dateChangeToAgoNews(String date){
//        String[] date_time = date.split(" ");
//    //    String newsDate = date_time[0];
////        String newsTime = date_time[1];
//        SimpleDateFormat formats = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
//        SimpleDateFormat timeformat = new SimpleDateFormat("HH:mm:ss");
//
//        LocalDate current_date = LocalDate.now();
//        LocalTime currentdatetime = LocalTime.now();
//        String currentdate = current_date+" "+currentdatetime;
//        Log.d("mediaplayer","newdatetime:"+date+" currentdate: "+currentdate);
//        String res = "";
//        Date d1= null;
//        Date d2 = null;
//
//        try{
//            d1 = formats.parse(date);
//            d2 = formats.parse(currentdate);
//        //    Date date1 = timeformat.parse(newsTime);
//
//         //   Date date2 = timeformat.parse(""+currentdatetime);
//
//            long difference = d1.getTime() - d2.getTime();
//
//            int minutes = (int) TimeUnit.MILLISECONDS.toMinutes(difference);
//            long diff =Math.abs( d2.getTime() - d1.getTime());
//            long diffSeconds = (diff / 1000) % 60;
//            int diffMinutes = (int) ((diff / (60 * 1000)) % 60);
//            int diffHours = (int) ((diff / (60 * 60 * 1000))%24);
//            int diffInDays = (int) ((diff / (1000 * 60 * 60 * 24)));
//            int day = 365-diffInDays;
//            Log.d("mediaplayer"," min: "+diffMinutes+" hh: "+diffHours+" day: "+diffInDays);
//            Log.d("mediaplayer"," min: "+minutes);
//
//
//            Log.d("mediaplayer"," modulus min :: "+(minutes%60));
//
//            if (diffMinutes < 60 && diffHours == 0 && diffInDays == 0) {
//                res = getStringToNelaliFromNumber(diffMinutes) + " मिनेट अगाडि";
//            } else if (diffHours < 24 && diffInDays == 0) {
//                res = getStringToNelaliFromNumber(diffHours) + " घण्टा अगाडि";
//            } else {
//                Log.d("mediaplayer"," minsss: "+minutes);
//                String[] arrayOfDates = date.split("-");
//                int engyear = Integer.parseInt(arrayOfDates[0]);
//                int engMonth = Integer.parseInt(arrayOfDates[1]);
//                int engday = Integer.parseInt(arrayOfDates[2]);
//                int npyear =   dateConverter.getNepaliDate(engyear,engMonth,engday).getYear();
//                int npmonth =   dateConverter.getNepaliDate(engyear,engMonth,engday).getMonth();
//                int npday =   dateConverter.getNepaliDate(engyear,engMonth,engday).getDay();
//                res= getStringToNelaliFromNumber(npyear)+"-"+getStringToNelaliFromNumber(npmonth)+"-"+getStringToNelaliFromNumber(npday);
//
//
//            }
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return res;
//    }
//
//    public static String getStringToNelaliFromNumber(int number) {
//        String[] nepaliNumArray = new String[] { "०", "१", "२", "३", "४", "५", "६", "७", "८", "९" };
//        StringBuilder result = new StringBuilder();
//        while (number > 0) {
//            int n = number % 10;
//            result.insert(0, nepaliNumArray[n]);
//            number /= 10;
//        }
//        return result.toString();
//    }

}
