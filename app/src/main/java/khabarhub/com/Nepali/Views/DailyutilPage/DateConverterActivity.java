package khabarhub.com.Nepali.Views.DailyutilPage;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import khabarhub.com.R;
import khabarhub.com.calendarnepali.DateConverter;

public class DateConverterActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    DateConverter dc;
    private TextView mtxt_np_eng,mtxt_np_eng_result,mtxt_eng_np,mtxt_eng_np_result;
    private Button mbtn_np_eng,mbtn_eng_np;
    private AppCompatSpinner mspinner_np_year,mspinner_np_month,mspinner_np_day,mspinner_eng_year,
            mspinner_eng_month,mspinner_eng_day;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_converter);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        LayoutInflater inflator = (LayoutInflater) this .getSystemService(this.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.toolbar_custom, null);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setCustomView(v);
         dc= new DateConverter();
        setid();
        setSpinnerdata();
        mspinner_np_year.setOnItemSelectedListener(this);
        mspinner_np_month.setOnItemSelectedListener(this);
        mspinner_eng_year.setOnItemSelectedListener(this);
        mspinner_eng_month.setOnItemSelectedListener(this);
        mbtn_np_eng.setOnClickListener(this);
        mbtn_eng_np.setOnClickListener(this);


    }

    private void setSpinnerdata() {
        mtxt_np_eng.setText("B.S to A.D परिवर्तन");
        mtxt_eng_np.setText("A.D to B.S परिवर्तन");
        mbtn_np_eng.setText("परिवर्तन");
        mbtn_eng_np.setText("परिवर्तन");
       setEngYearMonth();
      setNepaliYearMonth();

    }

    private void setNepaliYearMonth() {
        String[] npyeararray=  getResources().getStringArray(R.array.npyear);
        List<String> npyearnepali = new ArrayList<>();
        for (String npyears :npyeararray){
            String npy=  getStringToNelaliFromNumber(Integer.parseInt(npyears));
            npyearnepali.add(npy);
        }
        ArrayAdapter<String> npyearadapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, npyearnepali);
        npyearadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mspinner_np_year.setAdapter(npyearadapter);

        ArrayAdapter<CharSequence> npmonth = ArrayAdapter.createFromResource(this,
                R.array.npmonthnepali, android.R.layout.simple_spinner_item);
        npmonth.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //   mspinner_np_year.setAdapter(npyear);
        mspinner_np_month.setAdapter(npmonth);


        int npcurrentyearint=dc.getTodayNepaliDate().getYear();
        String npcurrentyear = getStringToNelaliFromNumber(npcurrentyearint);
        int npcurrentmonth = dc.getTodayNepaliDate().getMonth();
        mspinner_np_month.setSelection(npcurrentmonth);
        if (npcurrentyear !=null){
            int npyearpos= npyearadapter.getPosition(npcurrentyear);
            mspinner_np_year.setSelection(npyearpos);
        }

        setnepalidayspinner(npcurrentyearint,npcurrentmonth);

    }

    private void setEngYearMonth() {
        ArrayAdapter<CharSequence> engyear = ArrayAdapter.createFromResource(this,
                R.array.engyear, android.R.layout.simple_spinner_item);
        engyear.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter<CharSequence> engmonth = ArrayAdapter.createFromResource(this,
                R.array.engmonth, android.R.layout.simple_spinner_item);
        engmonth.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mspinner_eng_year.setAdapter(engyear);
        mspinner_eng_month.setAdapter(engmonth);

        String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        int month = (Calendar.getInstance().get(Calendar.MONTH));
        if (year !=null){
            int yearpos = engyear.getPosition(year);
            mspinner_eng_year.setSelection(yearpos);
        }
        mspinner_eng_month.setSelection(month);
        setengdayspinner(Integer.parseInt(year),month);
    }

    private void setnepalidayspinner(int npyear, int npmonth) {
        List<String> dayofmonth=new ArrayList<>();
        int monthdays = dc.noOfDaysInMonth(npyear,npmonth+1);
        int currentday = dc.getTodayNepaliDate().getDay();
        for (int i=0;i<monthdays;i++){
            dayofmonth.add(getStringToNelaliFromNumber(i+1));
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, dayofmonth);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mspinner_np_day.setAdapter(arrayAdapter);
        mspinner_np_day.setSelection(currentday-1);

    }

    private void setengdayspinner(int year, int month) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year,month,1);
        int day =calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        int currentday = (Calendar.getInstance().get(Calendar.DAY_OF_MONTH));
        List<String> dayofmonth=new ArrayList<>();
        for (int i=0;i<day;i++){
            dayofmonth.add(String.valueOf(i+1));
        }
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, dayofmonth);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mspinner_eng_day.setAdapter(arrayAdapter);
        mspinner_eng_day.setSelection(currentday-1);

    }

    private void setid() {
        mtxt_np_eng = findViewById(R.id.txt_np_to_eng_title);
        mtxt_np_eng_result = findViewById(R.id.txt_np_to_eng_result);
        mtxt_eng_np = findViewById(R.id.txt_eng_to_np_title);
        mtxt_eng_np_result =findViewById(R.id.txt_eng_to_np_result);
        mbtn_np_eng = findViewById(R.id.btn_np_to_eng);
        mbtn_eng_np = findViewById(R.id.btn_eng_to_np);
        mspinner_np_year = findViewById(R.id.sp_np_year);
        mspinner_np_month = findViewById(R.id.sp_np_month);
        mspinner_np_day = findViewById(R.id.sp_np_day);
        mspinner_eng_year = findViewById(R.id.sp_eng_year);
        mspinner_eng_month = findViewById(R.id.sp_eng_month);
        mspinner_eng_day = findViewById(R.id.sp_eng_day);
    }

    public static String getStringToNelaliFromNumber(int number) {
        String[] nepaliNumArray = new String[] { "०", "१", "२", "३", "४", "५", "६", "७", "८", "९" };
        StringBuilder result = new StringBuilder();
        while (number > 0) {
            int n = number % 10;
            result.insert(0, nepaliNumArray[n]);
            number /= 10;
        }
        return result.toString();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
      if(mspinner_np_year.getId() ==parent.getId()){
          int npyearselected = Integer.parseInt(getResources().getStringArray(R.array.npyear)[position]);
          int monthpost = mspinner_np_month.getSelectedItemPosition();
          setnepalidayspinner(npyearselected,monthpost);

      }
      else if(mspinner_np_month.getId() ==parent.getId()){
          int npyearselected = Integer.parseInt(getResources().getStringArray(R.array.npyear)[mspinner_np_year.getSelectedItemPosition()]);
          setnepalidayspinner(npyearselected,position);


      }
      else if(mspinner_eng_year.getId() ==parent.getId()){
          int engyearselected = Integer.parseInt(getResources().getStringArray(R.array.engyear)[position]);
          int engMonthselected = mspinner_eng_month.getSelectedItemPosition();
          setengdayspinner(engyearselected,engMonthselected);


      }
     else if(mspinner_eng_month.getId() ==parent.getId()){
          int engyearselected = Integer.parseInt(getResources().getStringArray(R.array.engyear)[mspinner_eng_year.getSelectedItemPosition()]);
          setengdayspinner(engyearselected,position);
      }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == mbtn_np_eng.getId()){
            int npselectedyear= Integer.parseInt(getResources().getStringArray(R.array.npyear)[mspinner_np_year.getSelectedItemPosition()]);
            int npselectedmonth= mspinner_np_month.getSelectedItemPosition()+1;
            int npselectedday = mspinner_np_day.getSelectedItemPosition()+1;
            int conterted_engyear=   dc.getEnglishDate(npselectedyear,npselectedmonth,npselectedday).getYear();
            int conterted_engmonth=   dc.getEnglishDate(npselectedyear,npselectedmonth+1,npselectedday).getMonth();
            int conterted_engday=   dc.getEnglishDate(npselectedyear,npselectedmonth,npselectedday).getDay();

            mtxt_np_eng_result.setText(""+conterted_engday+"/"+conterted_engmonth+"/"+conterted_engyear);
        }
        else if (v.getId() == mbtn_eng_np.getId()){
            int engselectedyear= Integer.parseInt(getResources().getStringArray(R.array.engyear)[mspinner_eng_year.getSelectedItemPosition()]);
            int engselectedmonth= mspinner_eng_month.getSelectedItemPosition()+1;
            int engselectedday = mspinner_eng_day.getSelectedItemPosition()+1;
            int conterted_engyear=   dc.getNepaliDate(engselectedyear,engselectedmonth,engselectedday).getYear();
            int conterted_engmonth=   dc.getNepaliDate(engselectedyear,engselectedmonth+1,engselectedday).getMonth();
            int conterted_engday=   dc.getNepaliDate(engselectedyear,engselectedmonth,engselectedday).getDay();

            mtxt_eng_np_result.setText(""+conterted_engday+"/"+conterted_engmonth+"/"+conterted_engyear);

        }

    }
}
