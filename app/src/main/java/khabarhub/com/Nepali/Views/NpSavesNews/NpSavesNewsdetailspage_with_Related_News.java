package khabarhub.com.Nepali.Views.NpSavesNews;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import khabarhub.com.English.Model.Savenews_English_Dao;
import khabarhub.com.English.Model.Savenews_English_Dto;
import khabarhub.com.English.View.EnglishMainActivity;
import khabarhub.com.English.View.HotNews.RelativeNews_JustIn_english;
import khabarhub.com.LanguageSharepreference;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.JustInDb;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.NepaliNewsSaveDao;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.NepaliNewsSaveDto;
import khabarhub.com.Nepali.Views.MainActivity;
import khabarhub.com.R;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link NpSavesNewsdetailspage_with_Related_News.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link NpSavesNewsdetailspage_with_Related_News#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NpSavesNewsdetailspage_with_Related_News extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";
    private static final String ARG_PARAM ="param";
    private static final String ARG_Author ="author";

    // TODO: Rename and change types of parameters
    private String mtitle;
    private String mdate;
    private String mAuthor;
    private String news,news_url,imgs;
    private WebSettings settings;

    LanguageSharepreference languageSharepreference;
    String language;
int news_id;
    private OnFragmentInteractionListener mListener;

    public NpSavesNewsdetailspage_with_Related_News() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static NpSavesNewsdetailspage_with_Related_News newInstance(int news_id, String title, String date, String body, String news_link, String img,String author_name) {
        NpSavesNewsdetailspage_with_Related_News fragment = new NpSavesNewsdetailspage_with_Related_News();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, title);
        args.putString(ARG_PARAM2, date);
        args.putString(ARG_PARAM3,body);
        args.putString(ARG_PARAM4,news_link);
        args.putString(ARG_PARAM5,img);
        args.putInt(ARG_PARAM,news_id);
        args.putString(ARG_Author,author_name);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

            mtitle = getArguments().getString(ARG_PARAM1);
            mdate = getArguments().getString(ARG_PARAM2);
            news=getArguments().getString(ARG_PARAM3);
            news_url=getArguments().getString(ARG_PARAM4);
            imgs=getArguments().getString(ARG_PARAM5);
            news_id=getArguments().getInt(ARG_PARAM);
            mAuthor= getArguments().getString(ARG_Author);

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_newsdetailspage_with_related_news, container, false);
            TextView title=v.findViewById(R.id.txt_news_detail_title);
        TextView date=v.findViewById(R.id.txt_news_detail_date);
        TextView author_name=v.findViewById(R.id.txt_news_detail_author_name);

        ImageView img=v.findViewById(R.id.img_relatednews);
        WebView webView=v.findViewById(R.id.related_news_detail_body);
        setHasOptionsMenu(true);
        languageSharepreference=LanguageSharepreference.Instance(getContext());
         language=languageSharepreference.getlanguage();
        settings = webView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setLoadsImagesAutomatically(true);


        webView.setWebViewClient(new WebViewClient());
        title.setText(mtitle);
        title.setTextSize(26);
        date.setText(mdate);
        author_name.setText(mAuthor+" | काठमाडौँ");

        webView.loadDataWithBaseURL(null, "<style>img{display: inline;height: auto;max-width: 100%;}p{text-align: justify;}</style>" +news,"text/html","utf-8",null);
        Picasso.with(getContext()).load(imgs).placeholder(R.drawable.opcity_logo).into(img);
        settings.setTextZoom(settings.getTextZoom()+20);
        return v;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.menu_sharebutton) {
            Intent intent=new Intent(Intent.ACTION_SEND);
            intent.setType("text/plain");
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            intent.putExtra(Intent.EXTRA_TITLE,"Khabarhub");
            intent.putExtra(Intent.EXTRA_TEXT,news_url);
            startActivity(Intent.createChooser(intent,"Share Using"));

            return true;
        }
        if (id==R.id.menu_fontdecreament){
              settings.setTextZoom(settings.getTextZoom() - 10);
        }
        if (id==R.id.menu_fontincreament){
               settings.setTextZoom(settings.getTextZoom() + 10);
        }
        if (id==R.id.menu_savebutton){

         if (language!=null && language.equalsIgnoreCase("English")){
             JustInDb justInDb = JustInDb.Instance(getContext());
             new SaveEnglishNewsAsyncTask(justInDb).execute();
         }
         else {
             JustInDb justInDb = JustInDb.Instance(getContext());
             new SaveNewsAsyncTask(justInDb).execute();
         }
        }
        if (id == R.id.menu_voice){
            SavedNewsOpenFragment relativeNews_justIn_english= (SavedNewsOpenFragment) getActivity();
            relativeNews_justIn_english.speakWords(mtitle);
        }
        if (id==R.id.menu_home){

            if (language!=null && language.equalsIgnoreCase("English")){
               startActivity(new Intent(getActivity(),EnglishMainActivity.class));
            }
            else {
                startActivity(new Intent(getActivity(),MainActivity.class));
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private class SaveNewsAsyncTask extends AsyncTask<Void,Void,Void> {
        JustInDb justInDbs;
        NepaliNewsSaveDao nepaliNewsSaveDao;
        public SaveNewsAsyncTask(JustInDb justInDb) {
            justInDbs=justInDb;
            nepaliNewsSaveDao=justInDb.nepaliNewsSaveDao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            justInDbs.nepaliNewsSaveDao().insertall(new NepaliNewsSaveDto(news_id,mtitle,mdate,imgs,news_url,news,mAuthor,""));
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(getActivity(),"Saved",Toast.LENGTH_SHORT).show();
            super.onPostExecute(aVoid);
        }
    }

    private class SaveEnglishNewsAsyncTask extends AsyncTask<Void,Void,Void>{
        JustInDb justInDbs;
        Savenews_English_Dao savenews_english_dao;
        public SaveEnglishNewsAsyncTask(JustInDb justInDb) {
            justInDbs=justInDb;
            savenews_english_dao=justInDb.savenews_english_dao();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            justInDbs.savenews_english_dao().insertall(new Savenews_English_Dto(news_id,mtitle,mdate,imgs,news_url,news,mAuthor,""));

            return null;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(getActivity(),"Saved",Toast.LENGTH_SHORT).show();

            super.onPostExecute(aVoid);
        }
    }
}
