package khabarhub.com.Nepali.Views.NpSavesNews;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.JustInDb;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.NepaliNewsSaveDao;
import khabarhub.com.Nepali.RepositoryPackage.DaoPackage.NepaliNewsSaveDto;
import khabarhub.com.Nepali.ViewModelPackage.JustInnewsViewModel;
import khabarhub.com.NetworkConnection;
import khabarhub.com.R;

public class NepaliNewsSavedFragment extends AppCompatActivity implements NepaliSaveNews_Adapter.OnItemClickListner {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    RecyclerView mrecyclerView;
    NepaliSaveNews_Adapter nepaliSaveNews_adapter;
    ProgressDialog progressDialog;
    Dialog dialog;
    String breakingNewsUri;
    NetworkConnection networkConnection;
    //private OnFragmentInteractionListener mListener;

    public NepaliNewsSavedFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_nepali_news_saved);
        mrecyclerView=findViewById(R.id.nepali_news_save_recycleview);
        networkConnection=new NetworkConnection(this);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        LayoutInflater inflator = (LayoutInflater) this .getSystemService(this.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.toolbar_custom, null);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setCustomView(v);
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(this);
        mrecyclerView.setLayoutManager(layoutManager);
        mrecyclerView.setHasFixedSize(true);
        prodressdialog();
        progressDialog=new ProgressDialog(this);
        progressDialog.setTitle("please wait");
        //  progressDialog.show();
        dialog.show();
        JustInnewsViewModel justInnewsViewModel= ViewModelProviders.of(this).get(JustInnewsViewModel.class);
        justInnewsViewModel.getNepalinewsSaved().observe(this, new Observer<List<NepaliNewsSaveDto>>() {
            @Override
            public void onChanged(List<NepaliNewsSaveDto> nepaliNewsSaveDtos) {
                dialog.dismiss();

                if (nepaliNewsSaveDtos.size() > 1) {
                    if (nepaliNewsSaveDtos.get(0).getId() == 1) {
                        Collections.reverse(nepaliNewsSaveDtos);
                    }
                }
                //
//
                nepaliSaveNews_adapter = new NepaliSaveNews_Adapter(nepaliNewsSaveDtos, NepaliNewsSavedFragment.this, NepaliNewsSavedFragment.this);
                mrecyclerView.setAdapter(nepaliSaveNews_adapter);



            }
        });
        // Inflate the layout for this fragment

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onclick(String title, String date, String news, List<NepaliNewsSaveDto> newslist, int position, String news_link) {

        Intent intent = new Intent(this, SavedNewsOpenFragment.class);
        intent.putExtra("news_title", newslist.get(position).getNews_title());
        //  intent.putExtra("news_title",newslist.get(getAdapterPosition()).getNews_title());
        intent.putExtra("news_date", newslist.get(position).getNews_date());
        intent.putExtra("news_details",newslist.get(position).getNews_body());
        intent.putExtra("newslist", (Serializable) newslist);
        intent.putExtra("clickposition",position);
        intent.putExtra("link",newslist.get(position).getNews_link());

        startActivity(intent);

    }

    @Override
    public void ondelete(int news_id) {
        JustInDb justInDb=JustInDb.Instance(this);
        new DeletenewsAsync(justInDb,news_id).execute();
    }


    // TODO: Rename method, update argument and hook method into UI event

    void prodressdialog()
    {
        ProgressBar progressBar=new ProgressBar(this);

        progressBar.setIndeterminate(true);
        progressBar.setScrollBarSize(2);
        dialog = new Dialog(this);
        progressBar.setMinimumWidth(10);
        progressBar.setMinimumHeight(10);

        //  dialog.getWindow().setGravity(Gravity.CENTER|Gravity.CENTER_HORIZONTAL);
        dialog.getWindow().setBackgroundDrawable(new
                ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(progressBar);

        dialog.setCancelable(true);
        //  dialog.setContentView(R.layout.progressdialoglayout);


    }

    private class DeletenewsAsync extends AsyncTask<Void,Void,Void> {
        JustInDb justInDbs;
        NepaliNewsSaveDao nepaliNewsSaveDao;
        int news_id;
        public DeletenewsAsync(JustInDb justInDb,int id) {
          justInDbs=justInDb;
          nepaliNewsSaveDao=justInDb.nepaliNewsSaveDao();
          news_id=id;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            justInDbs.nepaliNewsSaveDao().deletenews(news_id);
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            Toast.makeText(NepaliNewsSavedFragment.this,"Deleted",Toast.LENGTH_SHORT).show();
            super.onPostExecute(aVoid);
        }
    }
}
