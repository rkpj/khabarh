package khabarhub.com.Nepali.Views;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.squareup.picasso.Picasso;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import khabarhub.com.Aboutus;
import khabarhub.com.AlertBoxs;
import khabarhub.com.English.View.BreakingNews.RelativeNews_Breaking;
import khabarhub.com.Nepali.Apis;
import khabarhub.com.Nepali.Views.NpSavesNews.NepaliNewsSavedFragment;
import khabarhub.com.Nepali.TabFragment;
import khabarhub.com.Nepali.Views.DailyutilPage.DateConverterActivity;
import khabarhub.com.Nepali.Views.DailyutilPage.ForexActivity;
import khabarhub.com.Nepali.Views.DailyutilPage.Gold_SilverActivity;
import khabarhub.com.Nepali.Views.DailyutilPage.ShareMarketActivity;
import khabarhub.com.Nepali.Views.NpCategoriesNews.Fragment_of_Categories_news_list;
import khabarhub.com.R;
import khabarhub.com.Nepali.Views.Rasifal.RasifalFragment_for_botton_tab;
import khabarhub.com.SettingActivity;
import khabarhub.com.Sharepref_sessions;
import khabarhub.com.calendarnepali.CalendarActivity;
import khabarhub.com.radio.AudioManage;
import khabarhub.com.radio.MyService;
import khabarhub.com.radio.RadioActivity;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
       DrawerLayout drawer;
    TextView weather_temp;
    ImageView weather_icon;
    AlertBoxs alertBoxs;
BottomNavigationView bottomNavigationView;
    String lat,longs;
    Sharepref_sessions sharepreference;
    CardView radio_linearlayout;
    FrameLayout container;
    ConstraintLayout.LayoutParams params;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayShowTitleEnabled(false);
        alertBoxs=AlertBoxs.Instance(this);
         drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
         weather_icon=findViewById(R.id.weather_icon);
         weather_temp=findViewById(R.id.weather_temp);
        sharepreference=Sharepref_sessions.Instance(this);
        lat=sharepreference.getlat();
        longs=sharepreference.getlong();
        getweather(lat,longs);
         ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        replaceFragment(new TabFragment(),1);
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        bottomNavigationView=findViewById(R.id.bottom_navigation);
        bottomNavigationView.inflateMenu(R.menu.nepalibottomnav);

         radio_linearlayout = findViewById(R.id.layout_linear_buttomradio_mainpage);
        AppCompatImageView radiopausebtn = findViewById(R.id.imgbtn_play_bottomradio_mainpage);
         container = findViewById(R.id.contentview);
         params = (ConstraintLayout.LayoutParams) container.getLayoutParams();

        if (AudioManage.INSTANCE(this).getAudioFlag() == true){
            radio_linearlayout.setVisibility(View.VISIBLE);
            params.bottomMargin=165;
            container.setLayoutParams(params);
        }
        radiopausebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MyService.class);
                intent.setAction("com.android.music.playstatechanged");
                stopService(intent);
                radio_linearlayout.setVisibility(View.GONE);
                params.bottomMargin=0;
                container.setLayoutParams(params);

            }
        });
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
//                    case R.id.navigation_rasifal:
//                        replaceFragment(new RasifalFragment_for_botton_tab(),1);
//                        break;
                    case R.id.navigation_npcalendar:
                        replaceFragment(new CalendarActivity(),1);

                        break;
                    case R.id.navigation_setting:
                        replaceFragment(new SettingActivity(),1);
                        break;
                    case R.id.navigation_new:
                        replaceFragment(new TabFragment(),1);
                        break;

                    case R.id.navigation_radio:
                      startActivity(new Intent(MainActivity.this, RadioActivity.class));
                        break;
                }
                return true;
            }
        });


    }

    @Override
    protected void onStart() {
        super.onStart();
        lat=sharepreference.getlat();
        longs=sharepreference.getlong();
        getweather(lat,longs);
        String temp=sharepreference.getTemp();
        if (temp!=null){
            weather_temp.setText(sharepreference.getTemp()+"\u2103");
            Picasso.with(this).load("http://openweathermap.org/img/w/" + sharepreference.gettempIcon() + ".png").into(weather_icon);
        }
        if (AudioManage.INSTANCE(this).getAudioFlag() == true){
            radio_linearlayout.setVisibility(View.VISIBLE);
            params.bottomMargin=165;
            container.setLayoutParams(params);
        }
        if (AudioManage.INSTANCE(this).getAudioFlag() == false){
            radio_linearlayout.setVisibility(View.GONE);
            params.bottomMargin=0;
            container.setLayoutParams(params);

        }

    }

    private void getweather(String lat, String longs) {
       AndroidNetworking.get(Apis.weather_base_url+"lat="+lat+"&lon="+longs+"&units=metric&appid="+Apis.apikey_weather)
                .setPriority(Priority.LOW)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String city=response.optString("name");
                        try {
                            JSONObject jsonObject=response.getJSONObject("main");
                            String temp=jsonObject.optString("temp");
                         //   weather_temp.setText(temp+"\u2103");
                            JSONArray jsonArray=response.getJSONArray("weather");
                            for (int i=0;i<jsonArray.length();i++) {
                                JSONObject objforicons = jsonArray.getJSONObject(i);
                                String icon=objforicons.optString("icon");
                                String discribtion=objforicons.optString("description");
                                String iconUrl = "http://openweathermap.org/img/w/" + icon + ".png";
                                sharepreference.setWeather(icon,temp);
//                                Picasso.with(MainActivity.this).load(iconUrl).into(weather_icon);
//                                Log.d("Khabarhub_log","weather: "+city+" temp: "+temp+" describtion: "+discribtion);
//
                              }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                   }
                });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
            int seletedItemId = bottomNavigationView.getSelectedItemId();
            if (R.id.navigation_new != seletedItemId) {
                bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            } else {
                finishAffinity();
            }
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
         //   Toast.makeText(this,"home",Toast.LENGTH_LONG).show();
            replaceFragment(new TabFragment(),1);
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            // Handle the camera action
        } else if (id == R.id.nav_samachar) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
           // Toast.makeText(this,"home",Toast.LENGTH_LONG).show();
            replaceFragment(new Fragment_of_Categories_news_list(),13);

        } else if (id == R.id.nav_international) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            replaceFragment(new Fragment_of_Categories_news_list(),2);

        }
        else if (id == R.id.nav_bichar) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            replaceFragment(new Fragment_of_Categories_news_list(),9);

        }
        else if (id == R.id.nav_arthick) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            replaceFragment(new Fragment_of_Categories_news_list(),4);
         //   bottomNavigationView.setVisibility(View.GONE);
        }
        else if (id == R.id.nav_bichitra_sansar) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            replaceFragment(new Fragment_of_Categories_news_list(),12);
            //   bottomNavigationView.setVisibility(View.GONE);
        }
        else if (id == R.id.nav_imigration) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            replaceFragment(new Fragment_of_Categories_news_list(),34);
            //   bottomNavigationView.setVisibility(View.GONE);
        }
        else if (id == R.id.nav_desh_pradesh) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            replaceFragment(new Fragment_of_Categories_news_list(),6);
            //   bottomNavigationView.setVisibility(View.GONE);
        }
        else if (id == R.id.nav_travel) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            replaceFragment(new Fragment_of_Categories_news_list(),49);
         //   bottomNavigationView.setVisibility(View.GONE);
        } else if (id == R.id.nav_jiwansaily) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            replaceFragment(new Fragment_of_Categories_news_list(),16);
         //   bottomNavigationView.setVisibility(View.GONE);
        }
        else if (id == R.id.nav_health) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            replaceFragment(new Fragment_of_Categories_news_list(),41);

        } else if (id == R.id.nav_entertainment) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            replaceFragment(new Fragment_of_Categories_news_list(),10);
          //  bottomNavigationView.setVisibility(View.GONE);
        }
        else if (id == R.id.nav_technology) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            replaceFragment(new Fragment_of_Categories_news_list(),15);
           // bottomNavigationView.setVisibility(View.GONE);
        } else if (id == R.id.nav_politics) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            replaceFragment(new Fragment_of_Categories_news_list(),20);
           // bottomNavigationView.setVisibility(View.GONE);
        }
        else if (id == R.id.nav_security) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            replaceFragment(new Fragment_of_Categories_news_list(),149);
          //  bottomNavigationView.setVisibility(View.GONE);
        } else if (id == R.id.nav_sports) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            replaceFragment(new Fragment_of_Categories_news_list(),5);
          //  bottomNavigationView.setVisibility(View.GONE);

        }
        else if (id == R.id.nav_sanskriti) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            replaceFragment(new Fragment_of_Categories_news_list(),32);
            //  bottomNavigationView.setVisibility(View.GONE);

        }
        else if (id == R.id.nav_prawas) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_new);
            replaceFragment(new Fragment_of_Categories_news_list(),26);
            //  bottomNavigationView.setVisibility(View.GONE);

        }


        else if (id == R.id.npforex) {
            startActivity(new Intent(this, ForexActivity.class));
        }

        else if (id == R.id.npgold_silver) {
            startActivity(new Intent(this, Gold_SilverActivity.class));
        }

        else if (id == R.id.npSharemarket) {
            startActivity(new Intent(this, ShareMarketActivity.class));
        }

        else if (id == R.id.npdateConvert) {
            startActivity(new Intent(this, DateConverterActivity.class));
        }

        else if (id == R.id.nav_setting) {
            bottomNavigationView.setSelectedItemId(R.id.navigation_setting);
           replaceFragment(new SettingActivity(),1);
        }

        else if (id == R.id.nav_aboutus) {
            startActivity(new Intent(this, Aboutus.class));
        }
        else if (id == R.id.save) {
            startActivity(new Intent(this, NepaliNewsSavedFragment.class));
        }
        else if (id == R.id.nav_facebooklike) {
            String facebookId = "fb://page/483697488694219";
            String urlPage = "https://www.facebook.com/khabarhubofficial";

            try {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(facebookId )));
            } catch (Exception e) {
                //Open url web page.
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(urlPage)));
            }
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    void replaceFragment(Fragment fragment,int cat_id){
        if (fragment != null) {
            Bundle bundle=new Bundle();
            bundle.putInt("cat_id",cat_id);
            fragment.setArguments(bundle);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

            ft.replace(R.id.contentview, fragment);


            ft.commit();
        }
        drawer.closeDrawer(GravityCompat.START);
    }

}

