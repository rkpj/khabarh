package khabarhub.com.Nepali.Views.DailyutilPage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import khabarhub.com.Nepali.Model.ForexModel;
import khabarhub.com.R;

public class Adapter_Forex extends RecyclerView.Adapter<Adapter_Forex.MyViewHolder> {

    Context context;
    List<ForexModel> forexModelList;

    public Adapter_Forex(Context context, List<ForexModel> forexModelList) {
        this.context = context;
        this.forexModelList = forexModelList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.forex_adapter_layout,parent,false);

        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        ForexModel forexModel =forexModelList.get(position);
        if (forexModel.getBaseCurrency().equalsIgnoreCase("INR")){
            holder.currency.setText("Indian Rupees ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("USD")){
            holder.currency.setText("US Dollar ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("EUR")){
            holder.currency.setText("European Euro ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("GBP")){
            holder.currency.setText("UK Pound Sterling ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("CHF")){
            holder.currency.setText("Swiss Franc ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("AUD")){
            holder.currency.setText("Australian Dollar ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("CAD")){
            holder.currency.setText("Canadian Dollar ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("SGD")){
            holder.currency.setText("Singapore Dollar ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("JPY")){
            holder.currency.setText("Japanese Yen ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("CNY")){
            holder.currency.setText("Chinese Renminbi ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("SAR")){
            holder.currency.setText("Saudi Arabian Riyal ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("QAR")){
            holder.currency.setText("Qatari Riyal ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("THB")){
            holder.currency.setText("Thai Baht ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("AED")){
            holder.currency.setText("UAE Dirham ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("MYR")){
            holder.currency.setText("Malaysian Ringgit ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("KRW")){
            holder.currency.setText("Korean Won ("+forexModel.getBaseCurrency()+")");

        }

        else if (forexModel.getBaseCurrency().equalsIgnoreCase("SEK")){
            holder.currency.setText("Swedish Krone ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("DKK")){
            holder.currency.setText("Danish Krone ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("HKD")){
            holder.currency.setText("Hong Kong Dollar ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("KWD")){
            holder.currency.setText("Kuwaity Dinar ("+forexModel.getBaseCurrency()+")");

        }
        else if (forexModel.getBaseCurrency().equalsIgnoreCase("BHD")){
            holder.currency.setText("Bahrain Dinar ("+forexModel.getBaseCurrency()+")");

        }
        else {
            holder.currency.setText(forexModel.getBaseCurrency());

        }
        holder.unit.setText(""+forexModel.getBaseValue());
        holder.buying.setText(forexModel.getTargetBuy());
        holder.selling.setText(forexModel.getTargetSell());

    }

    @Override
    public int getItemCount() {
        return forexModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView currency,unit,selling,buying;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            currency = itemView.findViewById(R.id.txt_forex_currency);
            unit = itemView.findViewById(R.id.txt_forex_unit);
            selling = itemView.findViewById(R.id.txt_forex_selling);
            buying = itemView.findViewById(R.id.txt_forex_buying);
        }
    }
}
