package khabarhub.com.Nepali;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import khabarhub.com.Nepali.Views.NpBreakingNews.NepaliBreakingNewsFlipview.NepaliBreakingNewsFlipView;
import khabarhub.com.Nepali.Views.NpCategoriesNews.Fragment_of_Categories_news_list;
import khabarhub.com.Nepali.Views.NpHotNews.RecycleViewFragment;

class CustomFragmentPageAdapter extends FragmentPagerAdapter {
    private static final String TAG = CustomFragmentPageAdapter.class.getSimpleName();
    private static final int FRAGMENT_COUNT = 17;

    public CustomFragmentPageAdapter(FragmentManager fm) {
        super(fm);
    }
    @Override
    public Fragment getItem(int position) {
        Fragment_of_Categories_news_list fragment_of_categories_news_list=new Fragment_of_Categories_news_list();
        Bundle b=new Bundle();

        switch (position){
            case 0:
                return new NepaliBreakingNewsFlipView();
            case 1:
                return new RecycleViewFragment();
            case 2:

                b.putInt("cat_id",13);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;
            case 3:

                b.putInt("cat_id",20);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;

            case 4:

                b.putInt("cat_id",2);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;
            case 5:
                b.putInt("cat_id",4);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;
            case 6:
                b.putInt("cat_id",9);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;

            case 7:
                b.putInt("cat_id",150);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;
            case 8:
                b.putInt("cat_id",149);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;

            case 9:
                b.putInt("cat_id",6);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;
            case 10:
                b.putInt("cat_id",26);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;

            case 11:
                b.putInt("cat_id",41);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;

            case 12:
                b.putInt("cat_id",10);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;


            case 13:
                b.putInt("cat_id",15);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;

            case 14:
                b.putInt("cat_id",5);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;
            case 15:
                b.putInt("cat_id",49);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;



            case 16:
                b.putInt("cat_id",16);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;




            case 17:
                b.putInt("cat_id",34);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;

            case 18:
                b.putInt("cat_id",32);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;
            case 19:
                b.putInt("cat_id",12);
                fragment_of_categories_news_list.setArguments(b);
                return fragment_of_categories_news_list;




        }
        return null;
    }
    @Override
    public int getCount() {
        return FRAGMENT_COUNT;
    }
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "मुख्य";
            case 1:
                return "भर्खर";
            case 2:
                return "समाचार";
            case 3:
                return "राजनीति";
            case 4:
                return "अन्तर्राष्ट्रिय";
            case 5:
                return "आर्थिक";
            case 6:
                return "विचार";

            case 7:
                return "अन्तर्वार्ता";
            case 8:
                return "सुरक्षा";
            case 9:
                return "देश/प्रदेश";
            case 10:
                return "प्रवास";

            case 11:
                return "स्वास्थ्य";

            case 12:
                return "मनोरञ्जन";
            case 13:
                return "सूचना-प्रविधि";
            case 14:
                return "खेलकुद";
            case 15:
                return "ट्राभल";
            case 16:
                return "जीवनशैली";
            case 17:
                return "इमिग्रेसन";
             case 18:
                return "धर्म/संस्कृति";
            case 19:
                return "विचित्र संसार";

        }
        return null;
    }
}
