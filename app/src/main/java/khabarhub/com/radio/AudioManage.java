package khabarhub.com.radio;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.AudioFocusRequest;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.PowerManager;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.io.IOException;

import khabarhub.com.Nepali.Views.MainActivity;
import khabarhub.com.R;

public class AudioManage implements  MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, AudioManager.OnAudioFocusChangeListener {

    private static final String ACTION_PLAY = "com.android.music.playstatechanged";
    MediaPlayer mMediaPlayer = null;
    NotificationActionListner notificationActionListner;
    Context context;
    NotificationManagerCompat notificationManagerCompat;
    public static AudioManage instance = null;
    boolean flag = false;
    AudioManager audioManager;
    public static  AudioManage INSTANCE(Context context){

        if (instance == null){
            instance = new AudioManage(context);
        }
        return  instance;
    }
    AudioManage(Context context){

        notificationManagerCompat =  NotificationManagerCompat.from(context);
        this.context = context;

    }
    public void initMediaPlayer(){
        Log.d("mediaplayer","init");
        mMediaPlayer= new MediaPlayer();
        mMediaPlayer.setWakeMode(context, PowerManager.PARTIAL_WAKE_LOCK);
        WifiManager.WifiLock wifiLock = ((WifiManager) context.getSystemService(Context.WIFI_SERVICE))
                .createWifiLock(WifiManager.WIFI_MODE_FULL, "mylock");

        wifiLock.acquire();


        mMediaPlayer.setOnErrorListener(this);

        try {
            mMediaPlayer.setDataSource("https://hamro-radio-stream.hamropatro.com/radio-live-hd/radio-candid/icecast.audio");

            mMediaPlayer.prepareAsync();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void startAudio(){
        Log.d("mediaplayer","onprepare");
         audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        final int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
                AudioManager.AUDIOFOCUS_GAIN);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            AudioAttributes playbackAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();
            AudioFocusRequest focusRequest = new AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                    .setAudioAttributes(playbackAttributes)
                    .setAcceptsDelayedFocusGain(true)
                    .setOnAudioFocusChangeListener(this)
                    .build();

            boolean playbackDelayed = false;
            boolean playbackNowAuthorized = false;
            final Object focusLock = new Object();


            int res = audioManager.requestAudioFocus(focusRequest);
            synchronized (focusLock) {
                if (res == AudioManager.AUDIOFOCUS_REQUEST_FAILED) {
                    playbackNowAuthorized = false;
                } else if (res == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    playbackNowAuthorized = true;
                    Log.d("mediaplayer","focuslock");
                 onpreparelistner(res);
                } else if (res == AudioManager.AUDIOFOCUS_REQUEST_DELAYED) {
                    playbackDelayed = true;
                    playbackNowAuthorized = false;
                }
            }
        }


        onpreparelistner(result);
        if (flag ==true) {
            Log.d("mediaplayer", "flag"+flag);
        }
    }

    private void onpreparelistner(int result) {
        Log.d("mediaplayer", "result:"+result);
        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {

                switch (result) {
                    case AudioManager.AUDIOFOCUS_GAIN:
                        // resume playback
                        Log.d("mediaplayer", "gain");
                        if (mMediaPlayer == null) initMediaPlayer();
                        else if (!mMediaPlayer.isPlaying()){
                            mMediaPlayer.start();
                         //   startForgrounds();
                            setAudioFlag(true);
                        }
                        break;

                    case AudioManager.AUDIOFOCUS_LOSS:
                        Log.d("mediaplayer", "Loss");
                        // Lost focus for an unbounded amount of time: stop playback and release media player
                        if (mMediaPlayer.isPlaying()) mMediaPlayer.stop();
                        mMediaPlayer.release();
                        mMediaPlayer = null;
                        setAudioFlag(false);
                        break;

                    case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                        Log.d("mediaplayer", "loss transient");
                        // Lost focus for a short time, but we have to stop
                        // playback. We don't release the media player because playback
                        // is likely to resume
                        if (mMediaPlayer.isPlaying()) mMediaPlayer.pause();
                        break;

                    case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                        Log.d("mediaplayer", "duck");
                        // Lost focus for a short time, but it's ok to keep playing
                        // at an attenuated level
                        if (mMediaPlayer.isPlaying()) mMediaPlayer.setVolume(0.1f, 0.1f);
                        break;
                }
            }
        });
    }

    public void pauseAudio(){
        if (mMediaPlayer.isPlaying()){
            mMediaPlayer.stop();
            mMediaPlayer.reset();
            mMediaPlayer.release();
            mMediaPlayer = null;


        }

     //   mMediaPlayer.reset();
      //  notificationManagerCompat.cancel(0);
        setAudioFlag(false);
    }
    public void stopAudio(){
        if (mMediaPlayer.isPlaying()){
            mMediaPlayer.stop();
           // mMediaPlayer.reset();
            mMediaPlayer.release();
            mMediaPlayer = null;
          //  notificationManagerCompat.cancel(0);
            setAudioFlag(false);
        }

    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        return false;
    }

    @Override
    public void onAudioFocusChange(int i) {
        switch (i) {
            case AudioManager.AUDIOFOCUS_GAIN:
                // resume playback
                Log.d("mediaplayer","gain");
                if (mMediaPlayer == null) initMediaPlayer();
                else if (!mMediaPlayer.isPlaying()){
                    mMediaPlayer.start();
                  //  startForgrounds();
                }

                //  mMediaPlayer.setVolume(1.0f, 1.0f);

                break;

            case AudioManager.AUDIOFOCUS_LOSS:
                Log.d("mediaplayer","Loss");
                // Lost focus for an unbounded amount of time: stop playback and release media player
                if (mMediaPlayer.isPlaying()) mMediaPlayer.stop();
                mMediaPlayer.release();
                mMediaPlayer = null;
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                Log.d("mediaplayer","loss transient");
                // Lost focus for a short time, but we have to stop
                // playback. We don't release the media player because playback
                // is likely to resume
                if (mMediaPlayer.isPlaying()) mMediaPlayer.pause();
                break;

            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                Log.d("mediaplayer","duck");
                // Lost focus for a short time, but it's ok to keep playing
                // at an attenuated level
                if (mMediaPlayer.isPlaying()) mMediaPlayer.setVolume(0.1f, 0.1f);
                break;
        }
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {
        //  flag = true;
    }



    public void startForgrounds(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "channel name";
            String description = "desc";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("CHANNEL_ID", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

        }
        Intent intent_pause= new Intent(context,NotificationActionListner.class);
        PendingIntent pendingIntent_pause = PendingIntent.getBroadcast(context,0,intent_pause,0);
        RemoteViews notificationLayout = new RemoteViews(context.getPackageName(), R.layout.customnotificationonradio);
        notificationLayout.setOnClickPendingIntent(R.id.pause,pendingIntent_pause);


        Log.d("mediaplayer","startforg");
        PendingIntent pi = PendingIntent.getActivity(context, 0,
                new Intent(context, RadioActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new NotificationCompat.Builder(context,"CHANNEL_ID")
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setCustomContentView(notificationLayout)
                .setSmallIcon(R.drawable.ic_radio_black_24dp)
                .setContentTitle("RaidoCandid")
                .build();
        notification.icon = R.drawable.ic_play_circle_outline_black_24dp;
        notification.flags |= Notification.FLAG_ONGOING_EVENT;

        // context.startForeground(1, notification);

        notificationManagerCompat.notify(0,notification);
    }

    public void setAudioFlag( boolean status){
        flag = status;
        AudioSharepreference.setPlaying(context,status);
    }
    public boolean getAudioFlag(){
      //  boolean st = false;
//        if (!mMediaPlayer.isPlaying()){
//            return true;
//        }
//        else {
//            return false;
//        }
        return flag;
    }

}
