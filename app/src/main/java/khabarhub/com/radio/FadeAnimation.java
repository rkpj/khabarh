package khabarhub.com.radio;

import android.content.Context;
import android.transition.Fade;
import android.view.View;
import android.view.Window;

import khabarhub.com.R;

public class FadeAnimation {
    private static  FadeAnimation instance= null;
    Context context;
    public FadeAnimation(Context context) {
        this.context = context;
    }

    public static FadeAnimation INSTANCE(Context context){
        if (instance == null){
            instance = new FadeAnimation(context);
        }
        return instance;
    }
    public void fadeTransition(Window window){
        Fade fade = new Fade();
        View decorView = window.getDecorView();
        fade.excludeTarget(decorView.findViewById(R.id.action_bar_container),true);
        fade.excludeTarget(android.R.id.statusBarBackground,true);
        fade.excludeTarget(android.R.id.navigationBarBackground,true);
        window.setEnterTransition(fade);
        window.setExitTransition(fade);
    }
}
