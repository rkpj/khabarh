package khabarhub.com.radio;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import khabarhub.com.R;

public class RadioScheduleAllProgramFragment extends Fragment {
    String day;
    List<PodoCastScheduleModel> podoCastScheduleModelList;
    RecyclerView recyclerView;
    public RadioScheduleAllProgramFragment() {
        // Required empty public constructor
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_radio_schedule_all_program, container, false);
       recyclerView = v.findViewById(R.id.recy_all_schedule_programs);
        Bundle c= getArguments();
         day = c.getString("day");
        podoCastScheduleModelList = new ArrayList<>();
        String jsonres = readjsonprograms();
        parseRadioPrograms(jsonres);
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        Radio_Schedule_All_Adapter adapter = new Radio_Schedule_All_Adapter(podoCastScheduleModelList,getContext());
        recyclerView.setAdapter(adapter);
        return v;
    }



    private void parseRadioPrograms(String jsonres) {
        try {
            podoCastScheduleModelList.clear();
            JSONObject jsonObject = new JSONObject(jsonres);
            JSONObject data = jsonObject.getJSONObject("data");
            JSONObject timeSlots = data.getJSONObject("timeSlots");
            JSONArray jsonArray1 = timeSlots.getJSONArray("00:00");
            jsontolist(day,jsonArray1);
            JSONArray jsonArray2 = timeSlots.getJSONArray("05:00");
            jsontolist(day,jsonArray2);
            JSONArray jsonArray3 = timeSlots.getJSONArray("05:30");
            jsontolist(day,jsonArray3);
            JSONArray jsonArray4 = timeSlots.getJSONArray("06:00");
            jsontolist(day,jsonArray4);
            JSONArray jsonArray5 = timeSlots.getJSONArray("06:35");
            jsontolist(day,jsonArray5);
            JSONArray jsonArray6 = timeSlots.getJSONArray("07:05");
            jsontolist(day,jsonArray6);
            JSONArray jsonArray7 = timeSlots.getJSONArray("07:25");
            jsontolist(day,jsonArray7);
            JSONArray jsonArray8 = timeSlots.getJSONArray("08:00");
            jsontolist(day,jsonArray8);
            JSONArray jsonArray9 = timeSlots.getJSONArray("08:35");
            jsontolist(day,jsonArray9);
            JSONArray jsonArray10 = timeSlots.getJSONArray("09:00");
            jsontolist(day,jsonArray10);
            JSONArray jsonArray11 = timeSlots.getJSONArray("09:20");
            jsontolist(day,jsonArray11);
            JSONArray jsonArray12 = timeSlots.getJSONArray("10:00");
            jsontolist(day,jsonArray12);
            JSONArray jsonArray13 = timeSlots.getJSONArray("10:15");
            jsontolist(day,jsonArray13);
            JSONArray jsonArray14 = timeSlots.getJSONArray("11:00");
            jsontolist(day,jsonArray14);
            JSONArray jsonArray15 = timeSlots.getJSONArray("11:15");
            jsontolist(day,jsonArray15);
            JSONArray jsonArray16 = timeSlots.getJSONArray("12:00");
            jsontolist(day,jsonArray16);
            JSONArray jsonArray17 = timeSlots.getJSONArray("12:20");
            jsontolist(day,jsonArray17);
            JSONArray jsonArray18 = timeSlots.getJSONArray("13:00");
            jsontolist(day,jsonArray18);
            JSONArray jsonArray19 = timeSlots.getJSONArray("14:00");
            jsontolist(day,jsonArray19);
            JSONArray jsonArray20 = timeSlots.getJSONArray("14:20");
            jsontolist(day,jsonArray20);
            JSONArray jsonArray21 = timeSlots.getJSONArray("15:00");
            jsontolist(day,jsonArray21);
            JSONArray jsonArray22 = timeSlots.getJSONArray("15:15");
            jsontolist(day,jsonArray22);
            JSONArray jsonArray23 = timeSlots.getJSONArray("16:00");
            jsontolist(day,jsonArray23);
            JSONArray jsonArray24 = timeSlots.getJSONArray("18:03");
            jsontolist(day,jsonArray24);
            JSONArray jsonArray25 = timeSlots.getJSONArray("18:33");
            jsontolist(day,jsonArray25);
            JSONArray jsonArray26 = timeSlots.getJSONArray("19:00");
            jsontolist(day,jsonArray26);
            JSONArray jsonArray27 = timeSlots.getJSONArray("19:15");
            jsontolist(day,jsonArray27);
            JSONArray jsonArray28 = timeSlots.getJSONArray("20:00");
            jsontolist(day,jsonArray28);
            JSONArray jsonArray29 = timeSlots.getJSONArray("20:30");
            jsontolist(day,jsonArray29);
            JSONArray jsonArray30 = timeSlots.getJSONArray("20:45");
            jsontolist(day,jsonArray30);
            JSONArray jsonArray31 = timeSlots.getJSONArray("21:15");
            jsontolist(day,jsonArray31);
            JSONArray jsonArray32 = timeSlots.getJSONArray("21:45");
            jsontolist(day,jsonArray32);
            JSONArray jsonArray33 = timeSlots.getJSONArray("23:00");
            jsontolist(day,jsonArray33);
            JSONArray jsonArray34 = timeSlots.getJSONArray("23:30");
            jsontolist(day,jsonArray34);
            JSONArray jsonArray35 = timeSlots.getJSONArray("23:45");
            jsontolist(day,jsonArray35);


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void jsontolist(String day, JSONArray jsonArray){
        for (int i = 0; i < jsonArray.length(); i++) {

            try {
                JSONObject jobj_0 = jsonArray.getJSONObject(i);


                        if (jobj_0.getString("day").toString().equalsIgnoreCase(day)) {
                            //  Log.d("mediaplayer", "sunday" + jobj_0);
                            savePodoToList(jobj_0);
                        }



            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void savePodoToList(JSONObject jobj_0) {
        try {

            JSONObject jsonObject = jobj_0.getJSONObject("program");
            PodoCastScheduleModel podoCastScheduleModel = new PodoCastScheduleModel(jobj_0.getString("starting_time"),jobj_0.getString("ending_time"),jsonObject.getString("name"));
            podoCastScheduleModelList.add(podoCastScheduleModel);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private String readjsonprograms()  {

        File file = new File(getContext().getFilesDir(),"audiopodo.json");
        FileReader fileReader = null;
        StringBuilder stringBuilder = null;
        try {
            fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            stringBuilder = new StringBuilder();
            String line = null;
            line = bufferedReader.readLine();
            while (line != null){
                stringBuilder.append(line).append("\n");
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
            String responce = stringBuilder.toString();
            return   responce;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        catch (IOException e) {
            e.printStackTrace();
        }


// This responce will have Json Format String
        //      String responce = stringBuilder.toString();
        return   null;
    }

}
