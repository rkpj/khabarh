package khabarhub.com.radio;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.IBinder;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import java.io.IOException;

import khabarhub.com.R;

public class MyService extends Service  {
   // private static final String ACTION_PLAY = "com.android.music.playstatechanged";
    MediaPlayer mMediaPlayer = null;
    NotificationActionListner notificationActionListner;
    NotificationManagerCompat notificationManagerCompat;
    AudioManage audioManage;


    @Override
    public IBinder onBind(Intent intent) {
        Log.d("mediaplayer","onbind");
        return null;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("mediaplayer ","ocreate");

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("mediaplayer","onstartcomm");
        notificationManagerCompat =  NotificationManagerCompat.from(this);
         audioManage = AudioManage.INSTANCE(this);
         audioManage.initMediaPlayer();
         audioManage.startAudio();

        //  AudioSharepreference.setPlaying(this,true);
        startForgrounds();

        return START_NOT_STICKY;
    }


    public void startForgrounds(){

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            CharSequence name = "channel name";
//            String description = "desc";
//            int importance = NotificationManager.IMPORTANCE_DEFAULT;
//            NotificationChannel channel = new NotificationChannel("CHANNEL_ID", name, importance);
//            channel.setDescription(description);
//            // Register the channel with the system; you can't change the importance
//            // or other notification behaviors after this
//            NotificationManager notificationManager = getSystemService(NotificationManager.class);
//            notificationManager.createNotificationChannel(channel);
//
//        }
//        Intent intent_pause= new Intent(this,NotificationActionListner.class);
//        PendingIntent pendingIntent_pause = PendingIntent.getBroadcast(this,0,intent_pause,0);
//        RemoteViews notificationLayout = new RemoteViews(getPackageName(), R.layout.customnotificationonradio);
//        notificationLayout.setOnClickPendingIntent(R.id.pause,pendingIntent_pause);
//
//
//        Log.d("mediaplayer","startforg");
//        PendingIntent pi = PendingIntent.getActivity(this, 0,
//                new Intent(this, MainActivity.class),
//                PendingIntent.FLAG_UPDATE_CURRENT);
//        Notification notification = new NotificationCompat.Builder(this,"CHANNEL_ID")
//                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
//                .setCustomContentView(notificationLayout)
//                .setSmallIcon(R.drawable.ic_radio_black_24dp)
//                .setContentTitle("RaidoCandid")
//                .build();
//        notification.icon = R.drawable.ic_play_circle_outline_black_24dp;
//        notification.flags |= Notification.FLAG_ONGOING_EVENT;
//
//        startForeground(1, notification);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "channel name";
            String description = "desc";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("CHANNEL_ID", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = this.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);

        }
        Intent intent_pause= new Intent(this, khabarhub.com.radio.NotificationActionListner.class);
        PendingIntent pendingIntent_pause = PendingIntent.getBroadcast(this,0,intent_pause,0);
        RemoteViews notificationLayout = new RemoteViews(this.getPackageName(), R.layout.customnotificationonradio);
        notificationLayout.setOnClickPendingIntent(R.id.pause,pendingIntent_pause);


        Log.d("mediaplayer","startforg");
        PendingIntent pi = PendingIntent.getActivity(this, 0,
                new Intent(this, RadioActivity.class),
                PendingIntent.FLAG_UPDATE_CURRENT);
        Notification notification = new NotificationCompat.Builder(this,"CHANNEL_ID")
                .setStyle(new NotificationCompat.DecoratedCustomViewStyle())
                .setCustomContentView(notificationLayout)
                .build();
        notification.icon = R.drawable.ic_play_circle_outline_black_24dp;
        notification.flags |= Notification.FLAG_ONGOING_EVENT;

         startForeground(1, notification);

      //  notificationManagerCompat.notify(0,notification);
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("mediaplayer","service destroy ");
        //  mMediaPlayer.stop();
        audioManage.stopAudio();
        stopForeground(true);
      //  notificationManagerCompat.cancel(0);
       // AudioSharepreference.setPlaying(this,false);
        //  this.unregisterReceiver(notificationActionListner);

    }

}
