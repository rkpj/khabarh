package khabarhub.com.radio;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

public class Radioprograms_Adapter_week extends FragmentPagerAdapter {
    public Radioprograms_Adapter_week(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        RadioScheduleAllProgramFragment radioScheduleAllProgramFragment = new RadioScheduleAllProgramFragment();
        Bundle c = new Bundle();
        switch (position){
            case 0:
                c.putString("day","Sunday");
                radioScheduleAllProgramFragment.setArguments(c);
                return radioScheduleAllProgramFragment;
            case 1:
                c.putString("day","Monday");
                radioScheduleAllProgramFragment.setArguments(c);
                return radioScheduleAllProgramFragment;
            case 2:
                c.putString("day","Tuesday");
                radioScheduleAllProgramFragment.setArguments(c);
                return radioScheduleAllProgramFragment;
            case 3:
                c.putString("day","Wednesday");
                radioScheduleAllProgramFragment.setArguments(c);
                return radioScheduleAllProgramFragment;
            case 4:
                c.putString("day","Thursday");
                radioScheduleAllProgramFragment.setArguments(c);
                return radioScheduleAllProgramFragment;
            case 5:
                c.putString("day","Friday");
                radioScheduleAllProgramFragment.setArguments(c);
                return radioScheduleAllProgramFragment;
            case 6:
                c.putString("day","Saturday");
                radioScheduleAllProgramFragment.setArguments(c);
                return radioScheduleAllProgramFragment;

        }
        return null;
    }

    @Override
    public int getCount() {
        return 7;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "SUN";
            case 1:
                return "MON";
            case 2:
                return "TUE";
            case 3:
                return "WEB";
            case 4:
                return "THR";
            case 5:
                return "FRI";
            case 6:
                return "SAT";
        }
        return null;
    }
}
