package khabarhub.com.radio;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import khabarhub.com.R;

public class Radio_Schedule_All_Adapter extends RecyclerView.Adapter<Radio_Schedule_All_Adapter.MyViewHolder> {

    List<PodoCastScheduleModel> list;
    Context context;

    public Radio_Schedule_All_Adapter(List<PodoCastScheduleModel> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.all_radio_schedule_list_layout,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        PodoCastScheduleModel podoCastScheduleModel = list.get(position);
        holder.txt_program.setText(podoCastScheduleModel.getProgramename());
        holder.txt_time.setText(podoCastScheduleModel.getStarttime()+" - "+podoCastScheduleModel.getEndtime());
    }

    @Override
    public int getItemCount() {
//        if (list.size() > 8) {
//            return 8;
//        } else {
//            return list.size();
//        }
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_program,txt_time;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_program = itemView.findViewById(R.id.txt_all_program_name);
            txt_time = itemView.findViewById(R.id.txt_all_program_time);
        }
    }
}
