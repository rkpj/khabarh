package khabarhub.com.radio;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import static android.content.Context.MODE_PRIVATE;

public class AudioSharepreference {

   static SharedPreferences sharedPreferences;

    public static   void setPlaying(Context context,boolean status){
       sharedPreferences = context.getSharedPreferences("audioplay",MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("isPlay",status);
        editor.commit();
        Log.d("mediaplayer","preferencestore "+ status);
    }

    public static boolean getPlayingStatus(Context context){
        sharedPreferences = context.getSharedPreferences("audioplay",MODE_PRIVATE);
        return sharedPreferences.getBoolean("isPlay",false);
    }
}
