package khabarhub.com.radio;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.ViewCompat;
import androidx.core.widget.ImageViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.transition.Fade;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.JsonObject;

import org.joda.time.LocalDate;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import khabarhub.com.English.View.EnglishMainActivity;
import khabarhub.com.LanguageSharepreference;
import khabarhub.com.Nepali.Views.MainActivity;
import khabarhub.com.NetworkConnection;
import khabarhub.com.R;

public class RadioActivity extends AppCompatActivity  {
    ImageButton play;
    SeekBar seekBar;
    //AppCompatImageView wave_view;
    private AudioManager mgr = null;
 //   boolean isplaying = false;
    ConstraintLayout linearLayoutCompat;

    AudioManage audioManage;
    LinearLayoutCompat lay_buttomradio;
    AppCompatImageView botton_play;
    List<PodoCastScheduleModel> podoCastScheduleModelList;
    List<PodoCastScheduleModel> list_programs_short;
    RecyclerView recyclerView;
    AppCompatButton btn_more;
    AppCompatImageView logo_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.radio_activity_with_custom);
        play = findViewById(R.id.play);
        recyclerView = findViewById(R.id.recy_programs);
        logo_image = findViewById(R.id.img_radio_logo);
     //   btn_more = findViewById(R.id.btn_more);
        podoCastScheduleModelList = new ArrayList<>();
        list_programs_short = new ArrayList<>();
        play.setImageResource(R.drawable.ic_play_circle_outline_black_24dp);
        seekBar = findViewById(R.id.voice_control);
       // wave_view = findViewById(R.id.img_wave);
        linearLayoutCompat = findViewById(R.id.linear_radio);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        LayoutInflater inflator = (LayoutInflater) this .getSystemService(this.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.toolbar_custom, null);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setCustomView(v);
        FadeAnimation.INSTANCE(this).fadeTransition(getWindow());
        lay_buttomradio = findViewById(R.id.layout_linear_buttomradio);
        botton_play = findViewById(R.id.imgbtn_play_bottomradio);


        mgr=(AudioManager)getSystemService(getApplicationContext().AUDIO_SERVICE);
        audioManage = AudioManage.INSTANCE(this);

        AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
        int maxvolume = audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
        int currentvolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        seekBar.setMax(100);
        seekBar.setProgress(currentvolume);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress>0)
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,(progress/10),0);
                else
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,(progress),0);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        if (audioManage.getAudioFlag() == false) {
          //  isplaying = false;
            play.setImageResource(R.drawable.ic_play_circle_outline_black_24dp);
        //    wave_view.setImageResource(R.drawable.wavejpg);
        }
        else {
           // isplaying = true;
            lay_buttomradio.setVisibility(View.VISIBLE);

            play.setImageResource(R.drawable.ic_pause_circle_outline_black_24dp);
         //   Glide.with(RadioActivity.this).load(R.drawable.waves).into(wave_view);
        }

        String jsonres = readjsonprograms();

        parseRadioPrograms(jsonres);
        boolean st =AudioSharepreference.getPlayingStatus(this);
        programDisplayToRecycleview();
        Log.d("mediaplayer","listprogram: "+list_programs_short.get(0).getProgramename().toString());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
       if (list_programs_short.size()<7){
           for (int i=0; i<6;i++){
               if (list_programs_short.size()<7){
                   list_programs_short.add(podoCastScheduleModelList.get(i));
               }
           }
       }
        RadioProgramAdapter adapter = new RadioProgramAdapter(list_programs_short,this,logo_image);
        recyclerView.setAdapter(adapter);



        //   isplaying = st;
      //  audioManage.initMediaPlayer();
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (audioManage.getAudioFlag() == true){
                    Intent intent = new Intent(RadioActivity.this,MyService.class);
                    intent.setAction("com.android.music.playstatechanged");

                   // audioManage.stopAudio();
                    stopService(intent);
                 //   isplaying = false;
                    play.setImageResource(R.drawable.ic_play_circle_outline_black_24dp);
                  //  wave_view.setImageResource(R.drawable.wavejpg);
                }

                else {
                    Intent intent = new Intent(RadioActivity.this, MyService.class);
                    intent.setAction("com.android.music.playstatechanged");

                   // audioManage.startAudio();
                    startService(intent);
                    lay_buttomradio.setVisibility(View.VISIBLE);
                  //  isplaying = true;
                    play.setImageResource(R.drawable.ic_pause_circle_outline_black_24dp);
                    //Glide.with(RadioActivity.this).load(R.drawable.waves).into(wave_view);

                }
            }
        });
        botton_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("mediaplayer", "mediaplayerclick"+audioManage.getAudioFlag());

                if (audioManage.getAudioFlag() == true){
                    Intent intent = new Intent(RadioActivity.this,MyService.class);
                    intent.setAction("com.android.music.playstatechanged");
                    Log.d("mediaplayer", "mediaplayerclicked"+audioManage.getAudioFlag());

                  //  audioManage.stopAudio();
                    stopService(intent);

                    //isplaying = false;
                    play.setImageResource(R.drawable.ic_play_circle_outline_black_24dp);
                    botton_play.setImageResource(R.drawable.ic_play_circle_outline_white_24dp);

                  //  wave_view.setImageResource(R.drawable.wavejpg);
                }

                else {
                    Intent intent = new Intent(RadioActivity.this, MyService.class);
                    intent.setAction("com.android.music.playstatechanged");
                    Log.d("mediaplayer", "mediaplayerclicked"+audioManage.getAudioFlag());

                   // audioManage.startAudio();
                   startService(intent);
                    lay_buttomradio.setVisibility(View.VISIBLE);
                    Snackbar.make(linearLayoutCompat,"Buffering...",Snackbar.LENGTH_LONG).show();

                    //  isplaying = true;

                    play.setImageResource(R.drawable.ic_pause_circle_outline_black_24dp);
                    botton_play.setImageResource(R.drawable.ic_pause_circle_outline_white_24dp);

                 //   Glide.with(RadioActivity.this).load(R.drawable.waves).into(wave_view);

                }
            }
        });

      //  initBar(seekBar, AudioManager.STREAM_MUSIC);

//        btn_more.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startActivity(new Intent(RadioActivity.this,RadioScheduleProgramActivity.class));
//            }
//        });
    }

    private void programDisplayToRecycleview() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String str = sdf.format(new Date());
        list_programs_short.clear();
        try {
            Date currenttime = sdf.parse(str);
            for (int i = 0;i<podoCastScheduleModelList.size();i++){
                Date programTime = sdf.parse(podoCastScheduleModelList.get(i).starttime);
                Date programEndTime =sdf.parse(podoCastScheduleModelList.get(i).endtime);

                if (currenttime.after(programTime) && currenttime.before(programEndTime)){
                    Log.d("mediaplayer","times: "+currenttime+" podotime: "+programTime);
                    list_programs_short.add(podoCastScheduleModelList.get(i));
                }
                if (programTime.after(currenttime)){
                    Log.d("mediaplayer","times: "+currenttime+" podotime: "+programTime);
                    list_programs_short.add(podoCastScheduleModelList.get(i));
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    private void parseRadioPrograms(String jsonres) {
        try {
            int day =   Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
            podoCastScheduleModelList.clear();
            JSONObject jsonObject = new JSONObject(jsonres);
            JSONObject data = jsonObject.getJSONObject("data");
            JSONObject timeSlots = data.getJSONObject("timeSlots");
            JSONArray jsonArray1 = timeSlots.getJSONArray("00:00");
            jsontolist(day,jsonArray1);
            JSONArray jsonArray2 = timeSlots.getJSONArray("05:00");
            jsontolist(day,jsonArray2);
            JSONArray jsonArray3 = timeSlots.getJSONArray("05:30");
            jsontolist(day,jsonArray3);
            JSONArray jsonArray4 = timeSlots.getJSONArray("06:00");
            jsontolist(day,jsonArray4);
            JSONArray jsonArray5 = timeSlots.getJSONArray("06:35");
            jsontolist(day,jsonArray5);
            JSONArray jsonArray6 = timeSlots.getJSONArray("07:05");
            jsontolist(day,jsonArray6);
            JSONArray jsonArray7 = timeSlots.getJSONArray("07:25");
            jsontolist(day,jsonArray7);
            JSONArray jsonArray8 = timeSlots.getJSONArray("08:00");
            jsontolist(day,jsonArray8);
            JSONArray jsonArray9 = timeSlots.getJSONArray("08:35");
            jsontolist(day,jsonArray9);
            JSONArray jsonArray10 = timeSlots.getJSONArray("09:00");
            jsontolist(day,jsonArray10);
            JSONArray jsonArray11 = timeSlots.getJSONArray("09:20");
            jsontolist(day,jsonArray11);
            JSONArray jsonArray12 = timeSlots.getJSONArray("10:00");
            jsontolist(day,jsonArray12);
            JSONArray jsonArray13 = timeSlots.getJSONArray("10:15");
            jsontolist(day,jsonArray13);
            JSONArray jsonArray14 = timeSlots.getJSONArray("11:00");
            jsontolist(day,jsonArray14);
            JSONArray jsonArray15 = timeSlots.getJSONArray("11:15");
            jsontolist(day,jsonArray15);
            JSONArray jsonArray16 = timeSlots.getJSONArray("12:00");
            jsontolist(day,jsonArray16);
            JSONArray jsonArray17 = timeSlots.getJSONArray("12:20");
            jsontolist(day,jsonArray17);
            JSONArray jsonArray18 = timeSlots.getJSONArray("13:00");
            jsontolist(day,jsonArray18);
            JSONArray jsonArray19 = timeSlots.getJSONArray("14:00");
            jsontolist(day,jsonArray19);
            JSONArray jsonArray20 = timeSlots.getJSONArray("14:20");
            jsontolist(day,jsonArray20);
            JSONArray jsonArray21 = timeSlots.getJSONArray("15:00");
            jsontolist(day,jsonArray21);
            JSONArray jsonArray22 = timeSlots.getJSONArray("15:15");
            jsontolist(day,jsonArray22);
            JSONArray jsonArray23 = timeSlots.getJSONArray("16:00");
            jsontolist(day,jsonArray23);
            JSONArray jsonArray24 = timeSlots.getJSONArray("18:03");
            jsontolist(day,jsonArray24);
            JSONArray jsonArray25 = timeSlots.getJSONArray("18:33");
            jsontolist(day,jsonArray25);
            JSONArray jsonArray26 = timeSlots.getJSONArray("19:00");
            jsontolist(day,jsonArray26);
            JSONArray jsonArray27 = timeSlots.getJSONArray("19:15");
            jsontolist(day,jsonArray27);
            JSONArray jsonArray28 = timeSlots.getJSONArray("20:00");
            jsontolist(day,jsonArray28);
            JSONArray jsonArray29 = timeSlots.getJSONArray("20:30");
            jsontolist(day,jsonArray29);
            JSONArray jsonArray30 = timeSlots.getJSONArray("20:45");
            jsontolist(day,jsonArray30);
            JSONArray jsonArray31 = timeSlots.getJSONArray("21:15");
            jsontolist(day,jsonArray31);
            JSONArray jsonArray32 = timeSlots.getJSONArray("21:45");
            jsontolist(day,jsonArray32);
            JSONArray jsonArray33 = timeSlots.getJSONArray("23:00");
            jsontolist(day,jsonArray33);
            JSONArray jsonArray34 = timeSlots.getJSONArray("23:30");
            jsontolist(day,jsonArray34);
            JSONArray jsonArray35 = timeSlots.getJSONArray("23:45");
            jsontolist(day,jsonArray35);


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private void jsontolist(int day, JSONArray jsonArray){
        for (int i = 0; i < jsonArray.length(); i++) {

            try {
                JSONObject jobj_0 = jsonArray.getJSONObject(i);

                switch (day) {
                    case Calendar.SUNDAY:

                        if (jobj_0.getString("day").toString().equalsIgnoreCase("Sunday")) {
                          //  Log.d("mediaplayer", "sunday" + jobj_0);
                            savePodoToList(jobj_0);
                        }
                        break;
                    case Calendar.MONDAY:
                        if (jobj_0.getString("day").toString().equalsIgnoreCase("Monday")) {
                          //  Log.d("mediaplayer", "monday" + jobj_0);
                            savePodoToList(jobj_0);
                        }
                        break;
                    case Calendar.TUESDAY:
                        if (jobj_0.getString("day").toString().equalsIgnoreCase("Tuesday")) {
                          //  Log.d("mediaplayer", "tue" + jobj_0);
                            savePodoToList(jobj_0);
                        }
                        break;
                    case Calendar.WEDNESDAY:
                        if (jobj_0.getString("day").toString().equalsIgnoreCase("Wednesday")) {
                           // Log.d("mediaplayer", "wed" + jobj_0);
                            savePodoToList(jobj_0);
                        }
                        break;
                    case Calendar.THURSDAY:
                        if (jobj_0.getString("day").toString().equalsIgnoreCase("Thursday")) {
                          //  Log.d("mediaplayer", "thur" + jobj_0);
                            savePodoToList(jobj_0);
                        }
                        break;
                    case Calendar.FRIDAY:
                        if (jobj_0.getString("day").toString().equalsIgnoreCase("Friday")) {
                           // Log.d("mediaplayer", "fri" + jobj_0);
                            savePodoToList(jobj_0);
                        }
                        break;
                    case Calendar.SATURDAY:
                        if (jobj_0.getString("day").toString().equalsIgnoreCase("Saturday")) {
                          //  Log.d("mediaplayer", "sat" + jobj_0);
                            savePodoToList(jobj_0);
                        }
                        break;


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void savePodoToList(JSONObject jobj_0) {
        try {

            JSONObject jsonObject = jobj_0.getJSONObject("program");
            PodoCastScheduleModel podoCastScheduleModel = new PodoCastScheduleModel(jobj_0.getString("starting_time"),jobj_0.getString("ending_time"),jsonObject.getString("name"));
            podoCastScheduleModelList.add(podoCastScheduleModel);

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    private String readjsonprograms()  {

        File file = new File(getFilesDir(),"audiopodo.json");
        FileReader fileReader = null;
        StringBuilder stringBuilder = null;
        try {
            fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
             stringBuilder = new StringBuilder();
            String line = null;
            line = bufferedReader.readLine();
            while (line != null){
                stringBuilder.append(line).append("\n");
                line = bufferedReader.readLine();
            }
            bufferedReader.close();
            String responce = stringBuilder.toString();
            return   responce;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        
        catch (IOException e) {
            e.printStackTrace();
        }


// This responce will have Json Format String
  //      String responce = stringBuilder.toString();
        return   null;
    }

    private void initBar(SeekBar bar, final int stream) {
        bar.setMax(mgr.getStreamMaxVolume(stream));
        bar.setProgress(mgr.getStreamVolume(stream));
        bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar bar, int progress, boolean fromUser) {
                mgr.setStreamVolume(stream, progress, AudioManager.FLAG_PLAY_SOUND);

            }
            public void onStartTrackingTouch(SeekBar bar) {
                Log.d("mediaplayer","strack: "+bar.toString());
            }
            public void onStopTrackingTouch(SeekBar bar) {
                Log.d("mediaplayer","stoptrack: "+bar.toString());
            }
        });
    }

//    @Override
//    public boolean onKeyDown(int keyCode, KeyEvent event) {
//        try {
//
//
//            if (keyCode == KeyEvent.KEYCODE_VOLUME_UP) {
//                seekBar.setProgress(mgr.getStreamVolume(AudioManager.STREAM_MUSIC + 1));
//            } else if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
//                seekBar.setProgress(mgr.getStreamVolume(AudioManager.STREAM_MUSIC - 1));
//            }
//        }
//        catch (Exception e){
//            e.printStackTrace();
//        }
//        return super.onKeyDown(keyCode, event);
//    }

    @Override
    protected void onStart() {
        super.onStart();
        if (audioManage.getAudioFlag() == false) {
          //  isplaying = false;
            play.setImageResource(R.drawable.ic_play_circle_outline_black_24dp);
           // wave_view.setImageResource(R.drawable.wavejpg);
        }
        else {
          //  isplaying = true;
            Intent intent = new Intent(RadioActivity.this, MyService.class);
            intent.setAction("com.android.music.playstatechanged");
            lay_buttomradio.setVisibility(View.VISIBLE);

         //   startService(intent);
            play.setImageResource(R.drawable.ic_pause_circle_outline_black_24dp);
          //  Glide.with(RadioActivity.this).load(R.drawable.waves).into(wave_view);
        }
    }
    @Override
    public void onBackPressed() {
        String language = LanguageSharepreference.Instance(this).getlanguage();
        if (language!=null && language.equalsIgnoreCase("English")){
            startActivity(new Intent(this, EnglishMainActivity.class));
        }
        else {
            startActivity(new Intent(this, MainActivity.class));
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}