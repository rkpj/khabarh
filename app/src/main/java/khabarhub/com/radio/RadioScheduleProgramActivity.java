package khabarhub.com.radio;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.transition.Fade;
import android.view.LayoutInflater;
import android.view.View;

import com.google.android.material.tabs.TabLayout;

import khabarhub.com.R;

public class RadioScheduleProgramActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_schedule_program);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        LayoutInflater inflator = (LayoutInflater) this .getSystemService(this.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.toolbar_custom, null);
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setCustomView(v);

        FadeAnimation.INSTANCE(this).fadeTransition(getWindow());

        tabLayout = findViewById(R.id.programs_tabs);
        viewPager = findViewById(R.id.view_pager_schedule_program);
        viewPager.setAdapter(new Radioprograms_Adapter_week(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
