package khabarhub.com.radio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import khabarhub.com.Nepali.Views.List_of_news_adapters;
import khabarhub.com.R;

public class RadioProgramAdapter extends RecyclerView.Adapter<RadioProgramAdapter.MyViewHolder> {

    List<PodoCastScheduleModel> list;
    Context context;
    AppCompatImageView logo_image;


    public RadioProgramAdapter(List<PodoCastScheduleModel> list, Context context, AppCompatImageView logo_image) {
        this.list = list;
        this.context = context;
        this.logo_image = logo_image;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.radioprogramslayout, parent, false);

            return new MyViewHolder(view);

    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        PodoCastScheduleModel podoCastScheduleModel = list.get(position);
        holder.txt_program.setText(podoCastScheduleModel.getProgramename());
        holder.txt_time.setText(podoCastScheduleModel.getStarttime() + " - " + podoCastScheduleModel.getEndtime());

    }



    @Override
    public int getItemCount() {
//        if (list.size() > 8) {
//            return 8;
//        } else {
//            return list.size();
//        }
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txt_program,txt_time;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_program = itemView.findViewById(R.id.txt_program_name);
            txt_time = itemView.findViewById(R.id.txt_program_time);
        }
    }

//    private class MyViewHolder2 extends MyViewHolder {
//        TextView more;
//        public MyViewHolder2(@NonNull View view) {
//            super(view);
//            more = view.findViewById(R.id.btn_more_radio);
//            more.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(context,RadioScheduleProgramActivity.class);
//                    ActivityOptionsCompat optionsCompat=  ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) context,logo_image, ViewCompat.getTransitionName(logo_image));
//                    context.startActivity(intent,optionsCompat.toBundle());
//
//                }
//            });
//        }
//    }
}
