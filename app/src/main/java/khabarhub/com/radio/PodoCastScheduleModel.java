package khabarhub.com.radio;


public class PodoCastScheduleModel {
    String starttime,endtime;
    String programename;

    public PodoCastScheduleModel(String starttime, String endtime, String programename) {
        this.starttime = starttime;
        this.endtime = endtime;
        this.programename = programename;
    }

    public String getStarttime() {
        return starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public String getProgramename() {
        return programename;
    }
}
