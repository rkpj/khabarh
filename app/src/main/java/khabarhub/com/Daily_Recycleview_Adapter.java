package khabarhub.com;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class Daily_Recycleview_Adapter extends RecyclerView.Adapter<Daily_Recycleview_Adapter.MyViewHolder> {

   List webviewtext;
   Context context;

    public Daily_Recycleview_Adapter(List webviewtext, Context context) {
        this.webviewtext = webviewtext;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.daily_rasifal_widgets,parent,false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.mwebview.loadData("<style>img{display: inline;height: auto;max-width: 100%;}p{text-align: justify;}</style>"+webviewtext.get(position).toString(),"text/html","utf-8");
    }

    @Override
    public int getItemCount() {
        return webviewtext.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        WebView mwebview;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            mwebview=itemView.findViewById(R.id.webview_daily_rasifal);
        }
    }
}
